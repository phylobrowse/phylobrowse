# PhyloBrowse

A preconfigured Lubuntu virtual machine (VM) image with PhyloBrowse setup on a local browser, can by downloaded from the following link:
https://drive.google.com/file/d/1QN1VmSAPvNdzWsz6dH9LPzfTYGmOSSBd/view?usp=sharing
Please see the user guide for further instructions