var fs = require("fs");
var _ = require("lodash");
var mocha = require('mocha');
var chai = require('chai');

eval(fs.readFileSync('../lib/newick.js') + '');
var Newick = this;

eval(fs.readFileSync('../lib/newick.js') + '');
eval(fs.readFileSync('../js/parsers/GeneToGOParser.js') + '');
eval(fs.readFileSync('../js/OrderedHashmap.js') + '');
eval(fs.readFileSync('../js/staticData.js') + '');
eval(fs.readFileSync('../js/util.js') + '');


function setUpStaticData() {
    var data = fs.readFileSync("testData/newick.tre", "utf8");
    staticData.setRoot(Newick.parse(data));

    data = fs.readFileSync("testData/species.json", "utf8");
    data = JSON.parse(data);
    staticData.setSpecies(data);
    //console.log(staticData.getRoot());

    data = fs.readFileSync("testData/GoDesc.txt", "utf8");
    goTermDescriptionFileParser.parseFile(data);
    staticData.setGoTermIdToGoNameMap(goTermDescriptionFileParser.goIdToNameMap);
    staticData.setGoTermNameToGoIdMap(goTermDescriptionFileParser.goNameToIdMap);

    data = fs.readFileSync("testData/pbs.json", "utf8");
    data = JSON.parse(data);
    staticData.setPBS(data);
    //console.log(staticData.getPBS().N58);

    data = fs.readFileSync("testData/G2GO.json", "utf8");
    data = JSON.parse(data);
    staticData.processData(data);

    //console.log(bigPlantUtil.describeDataStructure(null, staticData.getGeneList()));
    //generateGeneListTest();
}

function generateGeneListTest() {
    var list = staticData.getGeneList();
    var counter = 0;
    _.each(list, function (item) {
        if (counter % 50 == 0) {
            console.log("it('" + item + "', function () {");
            console.log("chai.expect(staticData.getNodesForGene('" + item + "')).to.eql(" + JSON.stringify(staticData.getNodesForGene(item)) + ");");
            console.log("});");
        }
        counter++;
    });
}

/*function generatePValuesTest() {
 var map = staticData.getGoTermIdToNodesMap();
 for (key in map) {
 var term = map[key];
 if (term.totalPValue) {
 console.log("it('" + key + ".totalPValue', function () {");
 console.log("chai.expect(staticData.getNodesForGoTerm('" + key + "').totalPValue).to.equal(" + term.totalPValue + ");");
 console.log("});");
 console.log("it('" + key + ".numPValues', function () {");
 console.log("chai.expect(staticData.getNodesForGoTerm('" + key + "').numPValues).to.equal(" + term.numPValues + ");");
 console.log("});");
 console.log("it('" + key + ".avgPValue', function () {");
 console.log("chai.expect(staticData.getNodesForGoTerm('" + key + "').avgPValue).to.equal(" + term.avgPValue + ");");
 console.log("});");
 }
 }
 }*/

function generateGoTermIdToNodesMapTest() {
    for (key in goTermDescriptionFileParser.goIdToNameMap) {
        if (counter % 10 == 0) {
            console.log("it('" + key + "', function () {");
            console.log("chai.expect(staticData.getGoTermName('" + key + "')).to.equal('" + staticData.getGoTermName(key) + "');");
            console.log("});");
        }
        counter++;
    }
}

function generateGoTermNameToIdMapTest() {
    var counter = 0;
    for (key in goTermDescriptionFileParser.goNameToIdMap) {
        if (counter % 10 == 0) {
            console.log("it('" + key + "', function () {");
            console.log("chai.expect(staticData.getGOtermIdByName('" + key + "')).to.equal('" + staticData.getGOtermIdByName(key) + "');");
            console.log("});");
        }
        counter++;
    }
}

function testHighLows() {
    describe('testHighLows', function () {
        it('getLowestPValue', function () {
            chai.expect(staticData.getLowestPValue()).to.equal(0.0001);
        });
        it('getHighestPValue', function () {
            chai.expect(staticData.getHighestPValue()).to.equal(0.1);
        });
        it('getLowestAvgPBSValue', function () {
            chai.expect(staticData.getLowestAvgPBSValue()).to.equal(-0.24);
        });
        it('getHighestAvgPBSValue', function () {
            chai.expect(staticData.getHighestAvgPBSValue()).to.equal(10.5);
        });
        it('getLowestAbsPBSValue', function () {
            chai.expect(staticData.getLowestAbsPBSValue()).to.equal(1);
        });
        it('getHighestAbsPBSValue', function () {
            chai.expect(staticData.getHighestAbsPBSValue()).to.equal(162);
        });
    });
}

function testPValues() {
    describe('testPValues', function () {
        it('GO:0044249.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044249').totalPValue).to.equal(0.10087);
        });
        it('GO:0044249.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044249').numPValues).to.equal(4);
        });
        it('GO:0044249.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044249').avgPValue).to.equal(0.0252175);
        });
        it('GO:0008152.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0008152').totalPValue).to.equal(0.09152);
        });
        it('GO:0008152.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0008152').numPValues).to.equal(2);
        });
        it('GO:0008152.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0008152').avgPValue).to.equal(0.04576);
        });
        it('GO:0009987.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009987').totalPValue).to.equal(0.23733);
        });
        it('GO:0009987.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009987').numPValues).to.equal(5);
        });
        it('GO:0009987.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009987').avgPValue).to.equal(0.047466);
        });
        it('GO:0009058.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009058').totalPValue).to.equal(0.04186);
        });
        it('GO:0009058.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009058').numPValues).to.equal(1);
        });
        it('GO:0009058.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009058').avgPValue).to.equal(0.04186);
        });
        it('GO:0044237.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044237').totalPValue).to.equal(0.01349);
        });
        it('GO:0044237.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044237').numPValues).to.equal(1);
        });
        it('GO:0044237.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044237').avgPValue).to.equal(0.01349);
        });
        it('GO:0044272.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044272').totalPValue).to.equal(0.0811);
        });
        it('GO:0044272.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044272').numPValues).to.equal(1);
        });
        it('GO:0044272.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044272').avgPValue).to.equal(0.0811);
        });
        it('GO:0019438.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0019438').totalPValue).to.equal(0.01333);
        });
        it('GO:0019438.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0019438').numPValues).to.equal(1);
        });
        it('GO:0019438.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0019438').avgPValue).to.equal(0.01333);
        });
        it('GO:0044238.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044238').totalPValue).to.equal(0.02442);
        });
        it('GO:0044238.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044238').numPValues).to.equal(1);
        });
        it('GO:0044238.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044238').avgPValue).to.equal(0.02442);
        });
        it('GO:0044260.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044260').totalPValue).to.equal(0.01363);
        });
        it('GO:0044260.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044260').numPValues).to.equal(2);
        });
        it('GO:0044260.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044260').avgPValue).to.equal(0.006815);
        });
        it('GO:0019538.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0019538').totalPValue).to.equal(0.11832999999999999);
        });
        it('GO:0019538.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0019538').numPValues).to.equal(3);
        });
        it('GO:0019538.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0019538').avgPValue).to.equal(0.03944333333333333);
        });
        it('GO:0009309.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009309').totalPValue).to.equal(0.06989);
        });
        it('GO:0009309.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009309').numPValues).to.equal(2);
        });
        it('GO:0009309.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009309').avgPValue).to.equal(0.034945);
        });
        it('GO:0044267.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044267').totalPValue).to.equal(0.08876999999999999);
        });
        it('GO:0044267.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044267').numPValues).to.equal(3);
        });
        it('GO:0044267.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044267').avgPValue).to.equal(0.029589999999999995);
        });
        it('GO:0006412.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006412').totalPValue).to.equal(0.04951);
        });
        it('GO:0006412.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006412').numPValues).to.equal(2);
        });
        it('GO:0006412.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006412').avgPValue).to.equal(0.024755);
        });
        it('GO:0045036.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0045036').totalPValue).to.equal(0.00231);
        });
        it('GO:0045036.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0045036').numPValues).to.equal(1);
        });
        it('GO:0045036.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0045036').avgPValue).to.equal(0.00231);
        });
        it('GO:0046394.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0046394').totalPValue).to.equal(0.02871);
        });
        it('GO:0046394.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0046394').numPValues).to.equal(1);
        });
        it('GO:0046394.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0046394').avgPValue).to.equal(0.02871);
        });
        it('GO:0016053.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0016053').totalPValue).to.equal(0.02871);
        });
        it('GO:0016053.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0016053').numPValues).to.equal(1);
        });
        it('GO:0016053.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0016053').avgPValue).to.equal(0.02871);
        });
        it('GO:0009753.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009753').totalPValue).to.equal(0.09128);
        });
        it('GO:0009753.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009753').numPValues).to.equal(1);
        });
        it('GO:0009753.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009753').avgPValue).to.equal(0.09128);
        });
        it('GO:0006633.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006633').totalPValue).to.equal(0.04655);
        });
        it('GO:0006633.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006633').numPValues).to.equal(1);
        });
        it('GO:0006633.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006633').avgPValue).to.equal(0.04655);
        });
        it('GO:0006631.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006631').totalPValue).to.equal(0.04173);
        });
        it('GO:0006631.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006631').numPValues).to.equal(1);
        });
        it('GO:0006631.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006631').avgPValue).to.equal(0.04173);
        });
        it('GO:0009404.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009404').totalPValue).to.equal(0.02899);
        });
        it('GO:0009404.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009404').numPValues).to.equal(1);
        });
        it('GO:0009404.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009404').avgPValue).to.equal(0.02899);
        });
        it('GO:0009407.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009407').totalPValue).to.equal(0.02899);
        });
        it('GO:0009407.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009407').numPValues).to.equal(1);
        });
        it('GO:0009407.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009407').avgPValue).to.equal(0.02899);
        });
        it('GO:0006979.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006979').totalPValue).to.equal(0.0031);
        });
        it('GO:0006979.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006979').numPValues).to.equal(1);
        });
        it('GO:0006979.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006979').avgPValue).to.equal(0.0031);
        });
        it('GO:0009269.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009269').totalPValue).to.equal(0.04325);
        });
        it('GO:0009269.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009269').numPValues).to.equal(1);
        });
        it('GO:0009269.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009269').avgPValue).to.equal(0.04325);
        });
        it('GO:0046417.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0046417').totalPValue).to.equal(0.01371);
        });
        it('GO:0046417.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0046417').numPValues).to.equal(1);
        });
        it('GO:0046417.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0046417').avgPValue).to.equal(0.01371);
        });
        it('GO:0009073.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009073').totalPValue).to.equal(0.01371);
        });
        it('GO:0009073.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009073').numPValues).to.equal(1);
        });
        it('GO:0009073.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009073').avgPValue).to.equal(0.01371);
        });
        it('GO:0009072.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009072').totalPValue).to.equal(0.01767);
        });
        it('GO:0009072.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009072').numPValues).to.equal(1);
        });
        it('GO:0009072.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009072').avgPValue).to.equal(0.01767);
        });
        it('GO:0043648.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0043648').totalPValue).to.equal(0.04197);
        });
        it('GO:0043648.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0043648').numPValues).to.equal(1);
        });
        it('GO:0043648.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0043648').avgPValue).to.equal(0.04197);
        });
        it('GO:0017038.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0017038').totalPValue).to.equal(0.03701);
        });
        it('GO:0017038.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0017038').numPValues).to.equal(1);
        });
        it('GO:0017038.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0017038').avgPValue).to.equal(0.03701);
        });
        it('GO:0006869.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006869').totalPValue).to.equal(0.04877);
        });
        it('GO:0006869.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006869').numPValues).to.equal(1);
        });
        it('GO:0006869.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006869').avgPValue).to.equal(0.04877);
        });
        it('GO:0006972.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006972').totalPValue).to.equal(0.05401);
        });
        it('GO:0006972.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006972').numPValues).to.equal(1);
        });
        it('GO:0006972.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006972').avgPValue).to.equal(0.05401);
        });
        it('GO:0009739.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009739').totalPValue).to.equal(0.01924);
        });
        it('GO:0009739.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009739').numPValues).to.equal(1);
        });
        it('GO:0009739.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009739').avgPValue).to.equal(0.01924);
        });
        it('GO:0045037.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0045037').totalPValue).to.equal(0.00042);
        });
        it('GO:0045037.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0045037').numPValues).to.equal(1);
        });
        it('GO:0045037.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0045037').avgPValue).to.equal(0.00042);
        });
        it('GO:0042538.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0042538').totalPValue).to.equal(0.05401);
        });
        it('GO:0042538.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0042538').numPValues).to.equal(1);
        });
        it('GO:0042538.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0042538').avgPValue).to.equal(0.05401);
        });
        it('GO:0009850.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009850').totalPValue).to.equal(0.02516);
        });
        it('GO:0009850.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009850').numPValues).to.equal(1);
        });
        it('GO:0009850.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009850').avgPValue).to.equal(0.02516);
        });
        it('GO:0044273.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044273').totalPValue).to.equal(0.00428);
        });
        it('GO:0044273.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044273').numPValues).to.equal(2);
        });
        it('GO:0044273.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0044273').avgPValue).to.equal(0.00214);
        });
        it('GO:0010114.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0010114').totalPValue).to.equal(0.05513);
        });
        it('GO:0010114.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0010114').numPValues).to.equal(1);
        });
        it('GO:0010114.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0010114').avgPValue).to.equal(0.05513);
        });
        it('GO:0006510.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006510').totalPValue).to.equal(0.15466999999999997);
        });
        it('GO:0006510.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006510').numPValues).to.equal(3);
        });
        it('GO:0006510.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0006510').avgPValue).to.equal(0.05155666666666666);
        });
        it('GO:0009875.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009875').totalPValue).to.equal(0.02641);
        });
        it('GO:0009875.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009875').numPValues).to.equal(2);
        });
        it('GO:0009875.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0009875').avgPValue).to.equal(0.013205);
        });
        it('GO:0010218.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0010218').totalPValue).to.equal(0.01994);
        });
        it('GO:0010218.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0010218').numPValues).to.equal(1);
        });
        it('GO:0010218.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0010218').avgPValue).to.equal(0.01994);
        });
        it('GO:0032011.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0032011').totalPValue).to.equal(0.08433);
        });
        it('GO:0032011.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0032011').numPValues).to.equal(1);
        });
        it('GO:0032011.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0032011').avgPValue).to.equal(0.08433);
        });
        it('GO:0045165.totalPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0045165').totalPValue).to.equal(0.11773);
        });
        it('GO:0045165.numPValues', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0045165').numPValues).to.equal(2);
        });
        it('GO:0045165.avgPValue', function () {
            chai.expect(staticData.getNodesForGoTerm('GO:0045165').avgPValue).to.equal(0.058865);
        });
    });
}

function testGoIdToNameMap() {
    describe('GoTermIdToNodesMap', function () {
        it('GO:0044249', function () {
            chai.expect(staticData.getGoTermName('GO:0044249')).to.equal('cellular biosynthetic process');
        });
        it('GO:0006767', function () {
            chai.expect(staticData.getGoTermName('GO:0006767')).to.equal('water-soluble vitamin metabolic process');
        });
        it('GO:0044237', function () {
            chai.expect(staticData.getGoTermName('GO:0044237')).to.equal('cellular metabolic process');
        });
        it('GO:0006807', function () {
            chai.expect(staticData.getGoTermName('GO:0006807')).to.equal('nitrogen compound metabolic process');
        });
        it('GO:0044444', function () {
            chai.expect(staticData.getGoTermName('GO:0044444')).to.equal('cytoplasmic part');
        });
        it('GO:0050486', function () {
            chai.expect(staticData.getGoTermName('GO:0050486')).to.equal('intramolecular transferase activity, transferring hydroxy groups');
        });
        it('GO:0044238', function () {
            chai.expect(staticData.getGoTermName('GO:0044238')).to.equal('primary metabolic process');
        });
        it('GO:0008047', function () {
            chai.expect(staticData.getGoTermName('GO:0008047')).to.equal('enzyme activator activity');
        });
        it('GO:0051188', function () {
            chai.expect(staticData.getGoTermName('GO:0051188')).to.equal('cofactor biosynthetic process');
        });
        it('GO:0071841', function () {
            chai.expect(staticData.getGoTermName('GO:0071841')).to.equal('cellular component organization or biogenesis at cellular level');
        });
        it('GO:0048522', function () {
            chai.expect(staticData.getGoTermName('GO:0048522')).to.equal('positive regulation of cellular process');
        });
        it('GO:0090304', function () {
            chai.expect(staticData.getGoTermName('GO:0090304')).to.equal('nucleic acid metabolic process');
        });
        it('GO:0045449', function () {
            chai.expect(staticData.getGoTermName('GO:0045449')).to.equal('regulation of transcription');
        });
        it('GO:0044451', function () {
            chai.expect(staticData.getGoTermName('GO:0044451')).to.equal('nucleoplasm part');
        });
        it('GO:0016071', function () {
            chai.expect(staticData.getGoTermName('GO:0016071')).to.equal('mRNA metabolic process');
        });
        it('GO:0051603', function () {
            chai.expect(staticData.getGoTermName('GO:0051603')).to.equal('proteolysis involved in cellular protein catabolic process');
        });
        it('GO:0019941', function () {
            chai.expect(staticData.getGoTermName('GO:0019941')).to.equal('modification-dependent protein catabolic process');
        });
        it('GO:0016740', function () {
            chai.expect(staticData.getGoTermName('GO:0016740')).to.equal('transferase activity');
        });
        it('GO:0016772', function () {
            chai.expect(staticData.getGoTermName('GO:0016772')).to.equal('transferase activity, transferring phosphorus-containing groups');
        });
        it('GO:0016779', function () {
            chai.expect(staticData.getGoTermName('GO:0016779')).to.equal('nucleotidyltransferase activity');
        });
        it('GO:0043169', function () {
            chai.expect(staticData.getGoTermName('GO:0043169')).to.equal('cation binding');
        });
        it('GO:0046930', function () {
            chai.expect(staticData.getGoTermName('GO:0046930')).to.equal('pore complex');
        });
        it('GO:0005515', function () {
            chai.expect(staticData.getGoTermName('GO:0005515')).to.equal('protein binding');
        });
        it('GO:0043622', function () {
            chai.expect(staticData.getGoTermName('GO:0043622')).to.equal('cortical microtubule organization');
        });
        it('GO:0005524', function () {
            chai.expect(staticData.getGoTermName('GO:0005524')).to.equal('ATP binding');
        });
        it('GO:0008233', function () {
            chai.expect(staticData.getGoTermName('GO:0008233')).to.equal('peptidase activity');
        });
        it('GO:0019217', function () {
            chai.expect(staticData.getGoTermName('GO:0019217')).to.equal('regulation of fatty acid metabolic process');
        });
        it('GO:0010307', function () {
            chai.expect(staticData.getGoTermName('GO:0010307')).to.equal('acetylglutamate kinase regulator activity');
        });
        it('GO:0000287', function () {
            chai.expect(staticData.getGoTermName('GO:0000287')).to.equal('magnesium ion binding');
        });
        it('GO:0006338', function () {
            chai.expect(staticData.getGoTermName('GO:0006338')).to.equal('chromatin remodeling');
        });
        it('GO:0016818', function () {
            chai.expect(staticData.getGoTermName('GO:0016818')).to.equal('hydrolase activity, acting on acid anhydrides, in phosphorus-containing anhydrides');
        });
        it('GO:0065010', function () {
            chai.expect(staticData.getGoTermName('GO:0065010')).to.equal('extracellular membrane-bounded organelle');
        });
        it('GO:0009605', function () {
            chai.expect(staticData.getGoTermName('GO:0009605')).to.equal('response to external stimulus');
        });
        it('GO:0015137', function () {
            chai.expect(staticData.getGoTermName('GO:0015137')).to.equal('citrate transmembrane transporter activity');
        });
        it('GO:0016021', function () {
            chai.expect(staticData.getGoTermName('GO:0016021')).to.equal('integral to membrane');
        });
        it('GO:0034243', function () {
            chai.expect(staticData.getGoTermName('GO:0034243')).to.equal('regulation of transcription elongation from RNA polymerase II promoter');
        });
        it('GO:0044267', function () {
            chai.expect(staticData.getGoTermName('GO:0044267')).to.equal('cellular protein metabolic process');
        });
        it('GO:0006970', function () {
            chai.expect(staticData.getGoTermName('GO:0006970')).to.equal('response to osmotic stress');
        });
        it('GO:0070417', function () {
            chai.expect(staticData.getGoTermName('GO:0070417')).to.equal('cellular response to cold');
        });
        it('GO:0005783', function () {
            chai.expect(staticData.getGoTermName('GO:0005783')).to.equal('endoplasmic reticulum');
        });
        it('GO:0009526', function () {
            chai.expect(staticData.getGoTermName('GO:0009526')).to.equal('plastid envelope');
        });
        it('GO:0019867', function () {
            chai.expect(staticData.getGoTermName('GO:0019867')).to.equal('outer membrane');
        });
        it('GO:0019725', function () {
            chai.expect(staticData.getGoTermName('GO:0019725')).to.equal('cellular homeostasis');
        });
        it('GO:0009081', function () {
            chai.expect(staticData.getGoTermName('GO:0009081')).to.equal('branched chain family amino acid metabolic process');
        });
        it('GO:0006549', function () {
            chai.expect(staticData.getGoTermName('GO:0006549')).to.equal('isoleucine metabolic process');
        });
        it('GO:0048869', function () {
            chai.expect(staticData.getGoTermName('GO:0048869')).to.equal('cellular developmental process');
        });
        it('GO:0065001', function () {
            chai.expect(staticData.getGoTermName('GO:0065001')).to.equal('specification of axis polarity');
        });
        it('GO:0004129', function () {
            chai.expect(staticData.getGoTermName('GO:0004129')).to.equal('cytochrome-c oxidase activity');
        });
        it('GO:0008194', function () {
            chai.expect(staticData.getGoTermName('GO:0008194')).to.equal('UDP-glycosyltransferase activity');
        });
        it('GO:0009753', function () {
            chai.expect(staticData.getGoTermName('GO:0009753')).to.equal('response to jasmonic acid stimulus');
        });
        it('GO:0044042', function () {
            chai.expect(staticData.getGoTermName('GO:0044042')).to.equal('glucan metabolic process');
        });
        it('GO:0016741', function () {
            chai.expect(staticData.getGoTermName('GO:0016741')).to.equal('transferase activity, transferring one-carbon groups');
        });
        it('GO:0016788', function () {
            chai.expect(staticData.getGoTermName('GO:0016788')).to.equal('hydrolase activity, acting on ester bonds');
        });
        it('GO:0030570', function () {
            chai.expect(staticData.getGoTermName('GO:0030570')).to.equal('pectate lyase activity');
        });
        it('GO:0017171', function () {
            chai.expect(staticData.getGoTermName('GO:0017171')).to.equal('serine hydrolase activity');
        });
        it('GO:0070887', function () {
            chai.expect(staticData.getGoTermName('GO:0070887')).to.equal('cellular response to chemical stimulus');
        });
        it('GO:0080008', function () {
            chai.expect(staticData.getGoTermName('GO:0080008')).to.equal('CUL4 RING ubiquitin ligase complex');
        });
        it('GO:0006464', function () {
            chai.expect(staticData.getGoTermName('GO:0006464')).to.equal('protein modification process');
        });
        it('GO:0035601', function () {
            chai.expect(staticData.getGoTermName('GO:0035601')).to.equal('protein deacylation');
        });
        it('GO:0071368', function () {
            chai.expect(staticData.getGoTermName('GO:0071368')).to.equal('cellular response to cytokinin stimulus');
        });
        it('GO:0061025', function () {
            chai.expect(staticData.getGoTermName('GO:0061025')).to.equal('membrane fusion');
        });
        it('GO:0006259', function () {
            chai.expect(staticData.getGoTermName('GO:0006259')).to.equal('DNA metabolic process');
        });
        it('GO:0016616', function () {
            chai.expect(staticData.getGoTermName('GO:0016616')).to.equal('oxidoreductase activity, acting on the CH-OH group of donors, NAD or NADP as acceptor');
        });
        it('GO:0022607', function () {
            chai.expect(staticData.getGoTermName('GO:0022607')).to.equal('cellular component assembly');
        });
        it('GO:0043450', function () {
            chai.expect(staticData.getGoTermName('GO:0043450')).to.equal('alkene biosynthetic process');
        });
        it('GO:0030674', function () {
            chai.expect(staticData.getGoTermName('GO:0030674')).to.equal('protein binding, bridging');
        });
        it('GO:0009620', function () {
            chai.expect(staticData.getGoTermName('GO:0009620')).to.equal('response to fungus');
        });
        it('GO:0051253', function () {
            chai.expect(staticData.getGoTermName('GO:0051253')).to.equal('negative regulation of RNA metabolic process');
        });
        it('GO:0048523', function () {
            chai.expect(staticData.getGoTermName('GO:0048523')).to.equal('negative regulation of cellular process');
        });
        it('GO:0032583', function () {
            chai.expect(staticData.getGoTermName('GO:0032583')).to.equal('regulation of gene-specific transcription');
        });
        it('GO:0006633', function () {
            chai.expect(staticData.getGoTermName('GO:0006633')).to.equal('fatty acid biosynthetic process');
        });
        it('GO:0005098', function () {
            chai.expect(staticData.getGoTermName('GO:0005098')).to.equal('Ran GTPase activator activity');
        });
        it('GO:0009934', function () {
            chai.expect(staticData.getGoTermName('GO:0009934')).to.equal('regulation of meristem structural organization');
        });
        it('GO:0048583', function () {
            chai.expect(staticData.getGoTermName('GO:0048583')).to.equal('regulation of response to stimulus');
        });
        it('GO:0001510', function () {
            chai.expect(staticData.getGoTermName('GO:0001510')).to.equal('RNA methylation');
        });
        it('GO:0019201', function () {
            chai.expect(staticData.getGoTermName('GO:0019201')).to.equal('nucleotide kinase activity');
        });
        it('GO:0006020', function () {
            chai.expect(staticData.getGoTermName('GO:0006020')).to.equal('inositol metabolic process');
        });
        it('GO:0046527', function () {
            chai.expect(staticData.getGoTermName('GO:0046527')).to.equal('glucosyltransferase activity');
        });
        it('GO:0008287', function () {
            chai.expect(staticData.getGoTermName('GO:0008287')).to.equal('protein serine/threonine phosphatase complex');
        });
        it('GO:0003954', function () {
            chai.expect(staticData.getGoTermName('GO:0003954')).to.equal('NADH dehydrogenase activity');
        });
        it('GO:0043094', function () {
            chai.expect(staticData.getGoTermName('GO:0043094')).to.equal('cellular metabolic compound salvage');
        });
        it('GO:0016887', function () {
            chai.expect(staticData.getGoTermName('GO:0016887')).to.equal('ATPase activity');
        });
        it('GO:0009909', function () {
            chai.expect(staticData.getGoTermName('GO:0009909')).to.equal('regulation of flower development');
        });
        it('GO:0016126', function () {
            chai.expect(staticData.getGoTermName('GO:0016126')).to.equal('sterol biosynthetic process');
        });
        it('GO:0006892', function () {
            chai.expect(staticData.getGoTermName('GO:0006892')).to.equal('post-Golgi vesicle-mediated transport');
        });
        it('GO:0005774', function () {
            chai.expect(staticData.getGoTermName('GO:0005774')).to.equal('vacuolar membrane');
        });
        it('GO:0015036', function () {
            chai.expect(staticData.getGoTermName('GO:0015036')).to.equal('disulfide oxidoreductase activity');
        });
        it('GO:0009767', function () {
            chai.expect(staticData.getGoTermName('GO:0009767')).to.equal('photosynthetic electron transport chain');
        });
        it('GO:0033500', function () {
            chai.expect(staticData.getGoTermName('GO:0033500')).to.equal('carbohydrate homeostasis');
        });
        it('GO:0001678', function () {
            chai.expect(staticData.getGoTermName('GO:0001678')).to.equal('cellular glucose homeostasis');
        });
        it('GO:0030029', function () {
            chai.expect(staticData.getGoTermName('GO:0030029')).to.equal('actin filament-based process');
        });
        it('GO:0006072', function () {
            chai.expect(staticData.getGoTermName('GO:0006072')).to.equal('glycerol-3-phosphate metabolic process');
        });
        it('GO:0003702', function () {
            chai.expect(staticData.getGoTermName('GO:0003702')).to.equal('RNA polymerase II transcription factor activity');
        });
        it('GO:0044275', function () {
            chai.expect(staticData.getGoTermName('GO:0044275')).to.equal('cellular carbohydrate catabolic process');
        });
        it('GO:0046456', function () {
            chai.expect(staticData.getGoTermName('GO:0046456')).to.equal('icosanoid biosynthetic process');
        });
        it('GO:0016731', function () {
            chai.expect(staticData.getGoTermName('GO:0016731')).to.equal('oxidoreductase activity, acting on iron-sulfur proteins as donors, NAD or NADP as acceptor');
        });
        it('GO:0048500', function () {
            chai.expect(staticData.getGoTermName('GO:0048500')).to.equal('signal recognition particle');
        });
        it('GO:0006614', function () {
            chai.expect(staticData.getGoTermName('GO:0006614')).to.equal('SRP-dependent cotranslational protein targeting to membrane');
        });
        it('GO:0008219', function () {
            chai.expect(staticData.getGoTermName('GO:0008219')).to.equal('cell death');
        });
        it('GO:0009862', function () {
            chai.expect(staticData.getGoTermName('GO:0009862')).to.equal('systemic acquired resistance, salicylic acid mediated signaling pathway');
        });
        it('GO:0006323', function () {
            chai.expect(staticData.getGoTermName('GO:0006323')).to.equal('DNA packaging');
        });
        it('GO:0016833', function () {
            chai.expect(staticData.getGoTermName('GO:0016833')).to.equal('oxo-acid-lyase activity');
        });
        it('GO:0006568', function () {
            chai.expect(staticData.getGoTermName('GO:0006568')).to.equal('tryptophan metabolic process');
        });
        it('GO:0051604', function () {
            chai.expect(staticData.getGoTermName('GO:0051604')).to.equal('protein maturation');
        });
        it('GO:0006897', function () {
            chai.expect(staticData.getGoTermName('GO:0006897')).to.equal('endocytosis');
        });
        it('GO:0016668', function () {
            chai.expect(staticData.getGoTermName('GO:0016668')).to.equal('oxidoreductase activity, acting on a sulfur group of donors, NAD or NADP as acceptor');
        });
        it('GO:0035300', function () {
            chai.expect(staticData.getGoTermName('GO:0035300')).to.equal('inositol-1,3,4-trisphosphate 5/6-kinase activity');
        });
        it('GO:0009088', function () {
            chai.expect(staticData.getGoTermName('GO:0009088')).to.equal('threonine biosynthetic process');
        });
        it('GO:0022804', function () {
            chai.expect(staticData.getGoTermName('GO:0022804')).to.equal('active transmembrane transporter activity');
        });
        it('GO:0015399', function () {
            chai.expect(staticData.getGoTermName('GO:0015399')).to.equal('primary active transmembrane transporter activity');
        });
        it('GO:0016407', function () {
            chai.expect(staticData.getGoTermName('GO:0016407')).to.equal('acetyltransferase activity');
        });
        it('GO:0048589', function () {
            chai.expect(staticData.getGoTermName('GO:0048589')).to.equal('developmental growth');
        });
        it('GO:0070971', function () {
            chai.expect(staticData.getGoTermName('GO:0070971')).to.equal('endoplasmic reticulum exit site');
        });
        it('GO:0043413', function () {
            chai.expect(staticData.getGoTermName('GO:0043413')).to.equal('macromolecule glycosylation');
        });
        it('GO:0009062', function () {
            chai.expect(staticData.getGoTermName('GO:0009062')).to.equal('fatty acid catabolic process');
        });
        it('GO:0010029', function () {
            chai.expect(staticData.getGoTermName('GO:0010029')).to.equal('regulation of seed germination');
        });
        it('GO:0019842', function () {
            chai.expect(staticData.getGoTermName('GO:0019842')).to.equal('vitamin binding');
        });
        it('GO:0050790', function () {
            chai.expect(staticData.getGoTermName('GO:0050790')).to.equal('regulation of catalytic activity');
        });
        it('GO:0016036', function () {
            chai.expect(staticData.getGoTermName('GO:0016036')).to.equal('cellular response to phosphate starvation');
        });
        it('GO:0006643', function () {
            chai.expect(staticData.getGoTermName('GO:0006643')).to.equal('membrane lipid metabolic process');
        });
        it('GO:0005543', function () {
            chai.expect(staticData.getGoTermName('GO:0005543')).to.equal('phospholipid binding');
        });
        it('GO:0010347', function () {
            chai.expect(staticData.getGoTermName('GO:0010347')).to.equal('L-galactose-1-phosphate phosphatase activity');
        });
        it('GO:0019318', function () {
            chai.expect(staticData.getGoTermName('GO:0019318')).to.equal('hexose metabolic process');
        });
        it('GO:0042362', function () {
            chai.expect(staticData.getGoTermName('GO:0042362')).to.equal('fat-soluble vitamin biosynthetic process');
        });
        it('GO:0005750', function () {
            chai.expect(staticData.getGoTermName('GO:0005750')).to.equal('mitochondrial respiratory chain complex III');
        });
        it('GO:0043155', function () {
            chai.expect(staticData.getGoTermName('GO:0043155')).to.equal('negative regulation of photosynthesis, light reaction');
        });
        it('GO:0016709', function () {
            chai.expect(staticData.getGoTermName('GO:0016709')).to.equal('oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen, NADH or NADPH as one donor, and incorporation of one atom of oxygen');
        });
        it('GO:0031537', function () {
            chai.expect(staticData.getGoTermName('GO:0031537')).to.equal('regulation of anthocyanin metabolic process');
        });
        it('GO:0032549', function () {
            chai.expect(staticData.getGoTermName('GO:0032549')).to.equal('ribonucleoside binding');
        });
        it('GO:0042325', function () {
            chai.expect(staticData.getGoTermName('GO:0042325')).to.equal('regulation of phosphorylation');
        });
        it('GO:0004725', function () {
            chai.expect(staticData.getGoTermName('GO:0004725')).to.equal('protein tyrosine phosphatase activity');
        });
        it('GO:0023056', function () {
            chai.expect(staticData.getGoTermName('GO:0023056')).to.equal('positive regulation of signaling');
        });
        it('GO:0046451', function () {
            chai.expect(staticData.getGoTermName('GO:0046451')).to.equal('diaminopimelate metabolic process');
        });
        it('GO:0033015', function () {
            chai.expect(staticData.getGoTermName('GO:0033015')).to.equal('tetrapyrrole catabolic process');
        });
        it('GO:0004784', function () {
            chai.expect(staticData.getGoTermName('GO:0004784')).to.equal('superoxide dismutase activity');
        });
        it('GO:0010639', function () {
            chai.expect(staticData.getGoTermName('GO:0010639')).to.equal('negative regulation of organelle organization');
        });
        it('GO:0071156', function () {
            chai.expect(staticData.getGoTermName('GO:0071156')).to.equal('regulation of cell cycle arrest');
        });
        it('GO:0051784', function () {
            chai.expect(staticData.getGoTermName('GO:0051784')).to.equal('negative regulation of nuclear division');
        });
        it('GO:0046520', function () {
            chai.expect(staticData.getGoTermName('GO:0046520')).to.equal('sphingoid biosynthetic process');
        });
        it('GO:0004746', function () {
            chai.expect(staticData.getGoTermName('GO:0004746')).to.equal('riboflavin synthase activity');
        });
        it('GO:0006505', function () {
            chai.expect(staticData.getGoTermName('GO:0006505')).to.equal('GPI anchor metabolic process');
        });
        it('GO:0016790', function () {
            chai.expect(staticData.getGoTermName('GO:0016790')).to.equal('thiolester hydrolase activity');
        });
        it('GO:0000255', function () {
            chai.expect(staticData.getGoTermName('GO:0000255')).to.equal('allantoin metabolic process');
        });
        it('GO:0031234', function () {
            chai.expect(staticData.getGoTermName('GO:0031234')).to.equal('extrinsic to internal side of plasma membrane');
        });
        it('GO:0042981', function () {
            chai.expect(staticData.getGoTermName('GO:0042981')).to.equal('regulation of apoptosis');
        });
        it('GO:0009965', function () {
            chai.expect(staticData.getGoTermName('GO:0009965')).to.equal('leaf morphogenesis');
        });
        it('GO:0022613', function () {
            chai.expect(staticData.getGoTermName('GO:0022613')).to.equal('ribonucleoprotein complex biogenesis');
        });
        it('GO:0016857', function () {
            chai.expect(staticData.getGoTermName('GO:0016857')).to.equal('racemase and epimerase activity, acting on carbohydrates and derivatives');
        });
        it('GO:0010025', function () {
            chai.expect(staticData.getGoTermName('GO:0010025')).to.equal('wax biosynthetic process');
        });
        it('GO:0009310', function () {
            chai.expect(staticData.getGoTermName('GO:0009310')).to.equal('amine catabolic process');
        });
        it('GO:0016859', function () {
            chai.expect(staticData.getGoTermName('GO:0016859')).to.equal('cis-trans isomerase activity');
        });
        it('GO:0006672', function () {
            chai.expect(staticData.getGoTermName('GO:0006672')).to.equal('ceramide metabolic process');
        });
        it('GO:0016986', function () {
            chai.expect(staticData.getGoTermName('GO:0016986')).to.equal('transcription initiation factor activity');
        });
        it('GO:0006972', function () {
            chai.expect(staticData.getGoTermName('GO:0006972')).to.equal('hyperosmotic response');
        });
        it('GO:0005528', function () {
            chai.expect(staticData.getGoTermName('GO:0005528')).to.equal('FK506 binding');
        });
        it('GO:0004527', function () {
            chai.expect(staticData.getGoTermName('GO:0004527')).to.equal('exonuclease activity');
        });
        it('GO:0071824', function () {
            chai.expect(staticData.getGoTermName('GO:0071824')).to.equal('protein-DNA complex subunit organization');
        });
        it('GO:0032506', function () {
            chai.expect(staticData.getGoTermName('GO:0032506')).to.equal('cytokinetic process');
        });
        it('GO:0046505', function () {
            chai.expect(staticData.getGoTermName('GO:0046505')).to.equal('sulfolipid metabolic process');
        });
        it('GO:0004345', function () {
            chai.expect(staticData.getGoTermName('GO:0004345')).to.equal('glucose-6-phosphate dehydrogenase activity');
        });
        it('GO:0071265', function () {
            chai.expect(staticData.getGoTermName('GO:0071265')).to.equal('L-methionine biosynthetic process');
        });
        it('GO:0080061', function () {
            chai.expect(staticData.getGoTermName('GO:0080061')).to.equal('indole-3-acetonitrile nitrilase activity');
        });
        it('GO:0006825', function () {
            chai.expect(staticData.getGoTermName('GO:0006825')).to.equal('copper ion transport');
        });
        it('GO:0042214', function () {
            chai.expect(staticData.getGoTermName('GO:0042214')).to.equal('terpene metabolic process');
        });
        it('GO:0070298', function () {
            chai.expect(staticData.getGoTermName('GO:0070298')).to.equal('negative regulation of two-component signal transduction system (phosphorelay)');
        });
        it('GO:0007005', function () {
            chai.expect(staticData.getGoTermName('GO:0007005')).to.equal('mitochondrion organization');
        });
        it('GO:0006753', function () {
            chai.expect(staticData.getGoTermName('GO:0006753')).to.equal('nucleoside phosphate metabolic process');
        });
        it('GO:0006621', function () {
            chai.expect(staticData.getGoTermName('GO:0006621')).to.equal('protein retention in ER lumen');
        });
        it('GO:0005216', function () {
            chai.expect(staticData.getGoTermName('GO:0005216')).to.equal('ion channel activity');
        });
        it('GO:0015267', function () {
            chai.expect(staticData.getGoTermName('GO:0015267')).to.equal('channel activity');
        });
        it('GO:0003988', function () {
            chai.expect(staticData.getGoTermName('GO:0003988')).to.equal('acetyl-CoA C-acyltransferase activity');
        });
        it('GO:0004747', function () {
            chai.expect(staticData.getGoTermName('GO:0004747')).to.equal('ribokinase activity');
        });
        it('GO:0000293', function () {
            chai.expect(staticData.getGoTermName('GO:0000293')).to.equal('ferric-chelate reductase activity');
        });
        it('GO:0006075', function () {
            chai.expect(staticData.getGoTermName('GO:0006075')).to.equal('1,3-beta-D-glucan biosynthetic process');
        });
        it('GO:0006754', function () {
            chai.expect(staticData.getGoTermName('GO:0006754')).to.equal('ATP biosynthetic process');
        });
        it('GO:0009141', function () {
            chai.expect(staticData.getGoTermName('GO:0009141')).to.equal('nucleoside triphosphate metabolic process');
        });
        it('GO:0034220', function () {
            chai.expect(staticData.getGoTermName('GO:0034220')).to.equal('ion transmembrane transport');
        });
        it('GO:0004047', function () {
            chai.expect(staticData.getGoTermName('GO:0004047')).to.equal('aminomethyltransferase activity');
        });
        it('GO:0070403', function () {
            chai.expect(staticData.getGoTermName('GO:0070403')).to.equal('NAD binding');
        });
        it('GO:0080134', function () {
            chai.expect(staticData.getGoTermName('GO:0080134')).to.equal('regulation of response to stress');
        });
        it('GO:0009908', function () {
            chai.expect(staticData.getGoTermName('GO:0009908')).to.equal('flower development');
        });
        it('GO:0010258', function () {
            chai.expect(staticData.getGoTermName('GO:0010258')).to.equal('NADH dehydrogenase complex (plastoquinone) assembly');
        });
        it('GO:0000578', function () {
            chai.expect(staticData.getGoTermName('GO:0000578')).to.equal('embryonic axis specification');
        });
        it('GO:0042631', function () {
            chai.expect(staticData.getGoTermName('GO:0042631')).to.equal('cellular response to water deprivation');
        });
        it('GO:0003678', function () {
            chai.expect(staticData.getGoTermName('GO:0003678')).to.equal('DNA helicase activity');
        });
        it('GO:0033124', function () {
            chai.expect(staticData.getGoTermName('GO:0033124')).to.equal('regulation of GTP catabolic process');
        });
        it('GO:0004652', function () {
            chai.expect(staticData.getGoTermName('GO:0004652')).to.equal('polynucleotide adenylyltransferase activity');
        });
        it('GO:0004686', function () {
            chai.expect(staticData.getGoTermName('GO:0004686')).to.equal('elongation factor-2 kinase activity');
        });
        it('GO:0016670', function () {
            chai.expect(staticData.getGoTermName('GO:0016670')).to.equal('oxidoreductase activity, acting on a sulfur group of donors, oxygen as acceptor');
        });
        it('GO:0071845', function () {
            chai.expect(staticData.getGoTermName('GO:0071845')).to.equal('cellular component disassembly at cellular level');
        });
        it('GO:0004555', function () {
            chai.expect(staticData.getGoTermName('GO:0004555')).to.equal('alpha,alpha-trehalase activity');
        });
        it('GO:0080041', function () {
            chai.expect(staticData.getGoTermName('GO:0080041')).to.equal('ADP-ribose pyrophosphohydrolase activity');
        });
        it('GO:0070283', function () {
            chai.expect(staticData.getGoTermName('GO:0070283')).to.equal('radical SAM enzyme activity');
        });
        it('GO:0070589', function () {
            chai.expect(staticData.getGoTermName('GO:0070589')).to.equal('cellular component macromolecule biosynthetic process');
        });
        it('GO:0051179', function () {
            chai.expect(staticData.getGoTermName('GO:0051179')).to.equal('localization');
        });
        it('GO:0051641', function () {
            chai.expect(staticData.getGoTermName('GO:0051641')).to.equal('cellular localization');
        });
        it('GO:0009700', function () {
            chai.expect(staticData.getGoTermName('GO:0009700')).to.equal('indole phytoalexin biosynthetic process');
        });
        it('GO:0005828', function () {
            chai.expect(staticData.getGoTermName('GO:0005828')).to.equal('kinetochore microtubule');
        });
        it('GO:0004156', function () {
            chai.expect(staticData.getGoTermName('GO:0004156')).to.equal('dihydropteroate synthase activity');
        });
        it('GO:0050778', function () {
            chai.expect(staticData.getGoTermName('GO:0050778')).to.equal('positive regulation of immune response');
        });
        it('GO:0005487', function () {
            chai.expect(staticData.getGoTermName('GO:0005487')).to.equal('nucleocytoplasmic transporter activity');
        });
        it('GO:0004468', function () {
            chai.expect(staticData.getGoTermName('GO:0004468')).to.equal('lysine N-acetyltransferase activity');
        });
        it('GO:0016423', function () {
            chai.expect(staticData.getGoTermName('GO:0016423')).to.equal('tRNA (guanine) methyltransferase activity');
        });
        it('GO:0030170', function () {
            chai.expect(staticData.getGoTermName('GO:0030170')).to.equal('pyridoxal phosphate binding');
        });
        it('GO:0018826', function () {
            chai.expect(staticData.getGoTermName('GO:0018826')).to.equal('methionine gamma-lyase activity');
        });
        it('GO:0005792', function () {
            chai.expect(staticData.getGoTermName('GO:0005792')).to.equal('microsome');
        });
        it('GO:0060151', function () {
            chai.expect(staticData.getGoTermName('GO:0060151')).to.equal('peroxisome localization');
        });
        it('GO:0042939', function () {
            chai.expect(staticData.getGoTermName('GO:0042939')).to.equal('tripeptide transport');
        });
        it('GO:0016122', function () {
            chai.expect(staticData.getGoTermName('GO:0016122')).to.equal('xanthophyll metabolic process');
        });
        it('GO:0006714', function () {
            chai.expect(staticData.getGoTermName('GO:0006714')).to.equal('sesquiterpenoid metabolic process');
        });
        it('GO:0004722', function () {
            chai.expect(staticData.getGoTermName('GO:0004722')).to.equal('protein serine/threonine phosphatase activity');
        });
        it('GO:0048532', function () {
            chai.expect(staticData.getGoTermName('GO:0048532')).to.equal('anatomical structure arrangement');
        });
        it('GO:0006221', function () {
            chai.expect(staticData.getGoTermName('GO:0006221')).to.equal('pyrimidine nucleotide biosynthetic process');
        });
        it('GO:0005088', function () {
            chai.expect(staticData.getGoTermName('GO:0005088')).to.equal('Ras guanyl-nucleotide exchange factor activity');
        });
        it('GO:0004439', function () {
            chai.expect(staticData.getGoTermName('GO:0004439')).to.equal('phosphatidylinositol-4,5-bisphosphate 5-phosphatase activity');
        });
        it('GO:0009168', function () {
            chai.expect(staticData.getGoTermName('GO:0009168')).to.equal('purine ribonucleoside monophosphate biosynthetic process');
        });
        it('GO:0004594', function () {
            chai.expect(staticData.getGoTermName('GO:0004594')).to.equal('pantothenate kinase activity');
        });
        it('GO:0005302', function () {
            chai.expect(staticData.getGoTermName('GO:0005302')).to.equal('L-tyrosine transmembrane transporter activity');
        });
        it('GO:0015669', function () {
            chai.expect(staticData.getGoTermName('GO:0015669')).to.equal('gas transport');
        });
        it('GO:0042644', function () {
            chai.expect(staticData.getGoTermName('GO:0042644')).to.equal('chloroplast nucleoid');
        });
        it('GO:0046113', function () {
            chai.expect(staticData.getGoTermName('GO:0046113')).to.equal('nucleobase catabolic process');
        });
        it('GO:0080161', function () {
            chai.expect(staticData.getGoTermName('GO:0080161')).to.equal('auxin transmembrane transporter activity');
        });
        it('GO:0043022', function () {
            chai.expect(staticData.getGoTermName('GO:0043022')).to.equal('ribosome binding');
        });
        it('GO:0008615', function () {
            chai.expect(staticData.getGoTermName('GO:0008615')).to.equal('pyridoxine biosynthetic process');
        });
        it('GO:0046174', function () {
            chai.expect(staticData.getGoTermName('GO:0046174')).to.equal('polyol catabolic process');
        });
        it('GO:0008506', function () {
            chai.expect(staticData.getGoTermName('GO:0008506')).to.equal('sucrose:hydrogen symporter activity');
        });
        it('GO:0016725', function () {
            chai.expect(staticData.getGoTermName('GO:0016725')).to.equal('oxidoreductase activity, acting on CH or CH2 groups');
        });
        it('GO:0006528', function () {
            chai.expect(staticData.getGoTermName('GO:0006528')).to.equal('asparagine metabolic process');
        });
        it('GO:0010227', function () {
            chai.expect(staticData.getGoTermName('GO:0010227')).to.equal('floral organ abscission');
        });
        it('GO:0008170', function () {
            chai.expect(staticData.getGoTermName('GO:0008170')).to.equal('N-methyltransferase activity');
        });
        it('GO:0006144', function () {
            chai.expect(staticData.getGoTermName('GO:0006144')).to.equal('purine base metabolic process');
        });
        it('GO:0009881', function () {
            chai.expect(staticData.getGoTermName('GO:0009881')).to.equal('photoreceptor activity');
        });
        it('GO:0010118', function () {
            chai.expect(staticData.getGoTermName('GO:0010118')).to.equal('stomatal movement');
        });
        it('GO:0007349', function () {
            chai.expect(staticData.getGoTermName('GO:0007349')).to.equal('cellularization');
        });
        it('GO:0016411', function () {
            chai.expect(staticData.getGoTermName('GO:0016411')).to.equal('acylglycerol O-acyltransferase activity');
        });
        it('GO:0019320', function () {
            chai.expect(staticData.getGoTermName('GO:0019320')).to.equal('hexose catabolic process');
        });
        it('GO:0051003', function () {
            chai.expect(staticData.getGoTermName('GO:0051003')).to.equal('ligase activity, forming nitrogen-metal bonds, forming coordination complexes');
        });
        it('GO:0010090', function () {
            chai.expect(staticData.getGoTermName('GO:0010090')).to.equal('trichome morphogenesis');
        });
        it('GO:0006305', function () {
            chai.expect(staticData.getGoTermName('GO:0006305')).to.equal('DNA alkylation');
        });
        it('GO:0006917', function () {
            chai.expect(staticData.getGoTermName('GO:0006917')).to.equal('induction of apoptosis');
        });
        it('GO:0006366', function () {
            chai.expect(staticData.getGoTermName('GO:0006366')).to.equal('transcription from RNA polymerase II promoter');
        });
        it('GO:0006261', function () {
            chai.expect(staticData.getGoTermName('GO:0006261')).to.equal('DNA-dependent DNA replication');
        });
        it('GO:0008239', function () {
            chai.expect(staticData.getGoTermName('GO:0008239')).to.equal('dipeptidyl-peptidase activity');
        });
        it('GO:0055081', function () {
            chai.expect(staticData.getGoTermName('GO:0055081')).to.equal('anion homeostasis');
        });
        it('GO:0009513', function () {
            chai.expect(staticData.getGoTermName('GO:0009513')).to.equal('etioplast');
        });
        it('GO:0010244', function () {
            chai.expect(staticData.getGoTermName('GO:0010244')).to.equal('response to low fluence blue light stimulus by blue low-fluence system');
        });
        it('GO:0010494', function () {
            chai.expect(staticData.getGoTermName('GO:0010494')).to.equal('stress granule');
        });
        it('GO:0045491', function () {
            chai.expect(staticData.getGoTermName('GO:0045491')).to.equal('xylan metabolic process');
        });
        it('GO:0005885', function () {
            chai.expect(staticData.getGoTermName('GO:0005885')).to.equal('Arp2/3 protein complex');
        });
        it('GO:0043407', function () {
            chai.expect(staticData.getGoTermName('GO:0043407')).to.equal('negative regulation of MAP kinase activity');
        });
        it('GO:0004396', function () {
            chai.expect(staticData.getGoTermName('GO:0004396')).to.equal('hexokinase activity');
        });
        it('GO:0009927', function () {
            chai.expect(staticData.getGoTermName('GO:0009927')).to.equal('histidine phosphotransfer kinase activity');
        });
        it('GO:0015204', function () {
            chai.expect(staticData.getGoTermName('GO:0015204')).to.equal('urea transmembrane transporter activity');
        });
        it('GO:0006470', function () {
            chai.expect(staticData.getGoTermName('GO:0006470')).to.equal('protein dephosphorylation');
        });
        it('GO:0006547', function () {
            chai.expect(staticData.getGoTermName('GO:0006547')).to.equal('histidine metabolic process');
        });
        it('GO:0009973', function () {
            chai.expect(staticData.getGoTermName('GO:0009973')).to.equal('adenylyl-sulfate reductase activity');
        });
        it('GO:0006516', function () {
            chai.expect(staticData.getGoTermName('GO:0006516')).to.equal('glycoprotein catabolic process');
        });
        it('GO:0010209', function () {
            chai.expect(staticData.getGoTermName('GO:0010209')).to.equal('vacuolar sorting signal binding');
        });
        it('GO:0051017', function () {
            chai.expect(staticData.getGoTermName('GO:0051017')).to.equal('actin filament bundle assembly');
        });
        it('GO:0008794', function () {
            chai.expect(staticData.getGoTermName('GO:0008794')).to.equal('arsenate reductase (glutaredoxin) activity');
        });
        it('GO:0046488', function () {
            chai.expect(staticData.getGoTermName('GO:0046488')).to.equal('phosphatidylinositol metabolic process');
        });
        it('GO:0015116', function () {
            chai.expect(staticData.getGoTermName('GO:0015116')).to.equal('sulfate transmembrane transporter activity');
        });
        it('GO:0042175', function () {
            chai.expect(staticData.getGoTermName('GO:0042175')).to.equal('nuclear membrane-endoplasmic reticulum network');
        });
        it('GO:0006479', function () {
            chai.expect(staticData.getGoTermName('GO:0006479')).to.equal('protein methylation');
        });
        it('GO:0004829', function () {
            chai.expect(staticData.getGoTermName('GO:0004829')).to.equal('threonine-tRNA ligase activity');
        });
        it('GO:0005217', function () {
            chai.expect(staticData.getGoTermName('GO:0005217')).to.equal('intracellular ligand-gated ion channel activity');
        });
        it('GO:0015114', function () {
            chai.expect(staticData.getGoTermName('GO:0015114')).to.equal('phosphate transmembrane transporter activity');
        });
        it('GO:0009052', function () {
            chai.expect(staticData.getGoTermName('GO:0009052')).to.equal('pentose-phosphate shunt, non-oxidative branch');
        });
        it('GO:0004671', function () {
            chai.expect(staticData.getGoTermName('GO:0004671')).to.equal('protein C-terminal S-isoprenylcysteine carboxyl O-methyltransferase activity');
        });
        it('GO:0006168', function () {
            chai.expect(staticData.getGoTermName('GO:0006168')).to.equal('adenine salvage');
        });
        it('GO:0009750', function () {
            chai.expect(staticData.getGoTermName('GO:0009750')).to.equal('response to fructose stimulus');
        });
        it('GO:0010332', function () {
            chai.expect(staticData.getGoTermName('GO:0010332')).to.equal('response to gamma radiation');
        });
        it('GO:0015085', function () {
            chai.expect(staticData.getGoTermName('GO:0015085')).to.equal('calcium ion transmembrane transporter activity');
        });
        it('GO:0072507', function () {
            chai.expect(staticData.getGoTermName('GO:0072507')).to.equal('divalent inorganic cation homeostasis');
        });
        it('GO:0090066', function () {
            chai.expect(staticData.getGoTermName('GO:0090066')).to.equal('regulation of anatomical structure size');
        });
        it('GO:0046209', function () {
            chai.expect(staticData.getGoTermName('GO:0046209')).to.equal('nitric oxide metabolic process');
        });
        it('GO:0070192', function () {
            chai.expect(staticData.getGoTermName('GO:0070192')).to.equal('chromosome organization involved in meiosis');
        });
        it('GO:0007131', function () {
            chai.expect(staticData.getGoTermName('GO:0007131')).to.equal('reciprocal meiotic recombination');
        });
        it('GO:0009070', function () {
            chai.expect(staticData.getGoTermName('GO:0009070')).to.equal('serine family amino acid biosynthetic process');
        });
        it('GO:0005731', function () {
            chai.expect(staticData.getGoTermName('GO:0005731')).to.equal('nucleolus organizer region');
        });
        it('GO:0034968', function () {
            chai.expect(staticData.getGoTermName('GO:0034968')).to.equal('histone lysine methylation');
        });
        it('GO:0010833', function () {
            chai.expect(staticData.getGoTermName('GO:0010833')).to.equal('telomere maintenance via telomere lengthening');
        });
        it('GO:0006278', function () {
            chai.expect(staticData.getGoTermName('GO:0006278')).to.equal('RNA-dependent DNA replication');
        });
        it('GO:0009691', function () {
            chai.expect(staticData.getGoTermName('GO:0009691')).to.equal('cytokinin biosynthetic process');
        });
        it('GO:0007267', function () {
            chai.expect(staticData.getGoTermName('GO:0007267')).to.equal('cell-cell signaling');
        });
        it('GO:0023052', function () {
            chai.expect(staticData.getGoTermName('GO:0023052')).to.equal('signaling');
        });
        it('GO:0009130', function () {
            chai.expect(staticData.getGoTermName('GO:0009130')).to.equal('pyrimidine nucleoside monophosphate biosynthetic process');
        });
        it('GO:0004799', function () {
            chai.expect(staticData.getGoTermName('GO:0004799')).to.equal('thymidylate synthase activity');
        });
        it('GO:0016768', function () {
            chai.expect(staticData.getGoTermName('GO:0016768')).to.equal('spermine synthase activity');
        });
        it('GO:0042586', function () {
            chai.expect(staticData.getGoTermName('GO:0042586')).to.equal('peptide deformylase activity');
        });
        it('GO:0045740', function () {
            chai.expect(staticData.getGoTermName('GO:0045740')).to.equal('positive regulation of DNA replication');
        });
        it('GO:0034196', function () {
            chai.expect(staticData.getGoTermName('GO:0034196')).to.equal('acylglycerol transport');
        });
        it('GO:0009432', function () {
            chai.expect(staticData.getGoTermName('GO:0009432')).to.equal('SOS response');
        });
        it('GO:0009870', function () {
            chai.expect(staticData.getGoTermName('GO:0009870')).to.equal('defense response signaling pathway, resistance gene-dependent');
        });
        it('GO:0009884', function () {
            chai.expect(staticData.getGoTermName('GO:0009884')).to.equal('cytokinin receptor activity');
        });
        it('GO:0042823', function () {
            chai.expect(staticData.getGoTermName('GO:0042823')).to.equal('pyridoxal phosphate biosynthetic process');
        });
        it('GO:0006874', function () {
            chai.expect(staticData.getGoTermName('GO:0006874')).to.equal('cellular calcium ion homeostasis');
        });
        it('GO:0033865', function () {
            chai.expect(staticData.getGoTermName('GO:0033865')).to.equal('nucleoside bisphosphate metabolic process');
        });
        it('GO:0045005', function () {
            chai.expect(staticData.getGoTermName('GO:0045005')).to.equal('maintenance of fidelity involved in DNA-dependent DNA replication');
        });
        it('GO:0051654', function () {
            chai.expect(staticData.getGoTermName('GO:0051654')).to.equal('establishment of mitochondrion localization');
        });
        it('GO:0005483', function () {
            chai.expect(staticData.getGoTermName('GO:0005483')).to.equal('soluble NSF attachment protein activity');
        });
        it('GO:0080040', function () {
            chai.expect(staticData.getGoTermName('GO:0080040')).to.equal('positive regulation of cellular response to phosphate starvation');
        });
        it('GO:0004607', function () {
            chai.expect(staticData.getGoTermName('GO:0004607')).to.equal('phosphatidylcholine-sterol O-acyltransferase activity');
        });
        it('GO:0035383', function () {
            chai.expect(staticData.getGoTermName('GO:0035383')).to.equal('thioester metabolic process');
        });
        it('GO:0000488', function () {
            chai.expect(staticData.getGoTermName('GO:0000488')).to.equal('maturation of LSU-rRNA from tetracistronic rRNA transcript (SSU-rRNA, LSU-rRNA, 4.5S-rRNA, 5S-rRNA)');
        });
        it('GO:0016278', function () {
            chai.expect(staticData.getGoTermName('GO:0016278')).to.equal('lysine N-methyltransferase activity');
        });
        it('GO:0055047', function () {
            chai.expect(staticData.getGoTermName('GO:0055047')).to.equal('generative cell mitosis');
        });
        it('GO:0033206', function () {
            chai.expect(staticData.getGoTermName('GO:0033206')).to.equal('cytokinesis after meiosis');
        });
        it('GO:0004549', function () {
            chai.expect(staticData.getGoTermName('GO:0004549')).to.equal('tRNA-specific ribonuclease activity');
        });
        it('GO:0015140', function () {
            chai.expect(staticData.getGoTermName('GO:0015140')).to.equal('malate transmembrane transporter activity');
        });
        it('GO:0006546', function () {
            chai.expect(staticData.getGoTermName('GO:0006546')).to.equal('glycine catabolic process');
        });
        it('GO:0009118', function () {
            chai.expect(staticData.getGoTermName('GO:0009118')).to.equal('regulation of nucleoside metabolic process');
        });
        it('GO:0019209', function () {
            chai.expect(staticData.getGoTermName('GO:0019209')).to.equal('kinase activator activity');
        });
        it('GO:0006828', function () {
            chai.expect(staticData.getGoTermName('GO:0006828')).to.equal('manganese ion transport');
        });
        it('GO:0008660', function () {
            chai.expect(staticData.getGoTermName('GO:0008660')).to.equal('1-aminocyclopropane-1-carboxylate deaminase activity');
        });
        it('GO:0005319', function () {
            chai.expect(staticData.getGoTermName('GO:0005319')).to.equal('lipid transporter activity');
        });
        it('GO:0010962', function () {
            chai.expect(staticData.getGoTermName('GO:0010962')).to.equal('regulation of glucan biosynthetic process');
        });
        it('GO:0018401', function () {
            chai.expect(staticData.getGoTermName('GO:0018401')).to.equal('peptidyl-proline hydroxylation to 4-hydroxy-L-proline');
        });
        it('GO:0010183', function () {
            chai.expect(staticData.getGoTermName('GO:0010183')).to.equal('pollen tube guidance');
        });
        it('GO:0032260', function () {
            chai.expect(staticData.getGoTermName('GO:0032260')).to.equal('response to jasmonic acid stimulus involved in jasmonic acid and ethylene-dependent systemic resistance');
        });
        it('GO:0010213', function () {
            chai.expect(staticData.getGoTermName('GO:0010213')).to.equal('non-photoreactive DNA repair');
        });
        it('GO:0048554', function () {
            chai.expect(staticData.getGoTermName('GO:0048554')).to.equal('positive regulation of metalloenzyme activity');
        });
        it('GO:0030665', function () {
            chai.expect(staticData.getGoTermName('GO:0030665')).to.equal('clathrin coated vesicle membrane');
        });
        it('GO:0019585', function () {
            chai.expect(staticData.getGoTermName('GO:0019585')).to.equal('glucuronate metabolic process');
        });
        it('GO:0009647', function () {
            chai.expect(staticData.getGoTermName('GO:0009647')).to.equal('skotomorphogenesis');
        });
        it('GO:0046473', function () {
            chai.expect(staticData.getGoTermName('GO:0046473')).to.equal('phosphatidic acid metabolic process');
        });
        it('GO:0000990', function () {
            chai.expect(staticData.getGoTermName('GO:0000990')).to.equal('core RNA polymerase binding transcription factor activity');
        });
        it('GO:0010136', function () {
            chai.expect(staticData.getGoTermName('GO:0010136')).to.equal('ureide catabolic process');
        });
        it('GO:0016814', function () {
            chai.expect(staticData.getGoTermName('GO:0016814')).to.equal('hydrolase activity, acting on carbon-nitrogen (but not peptide) bonds, in cyclic amidines');
        });
        it('GO:0048441', function () {
            chai.expect(staticData.getGoTermName('GO:0048441')).to.equal('petal development');
        });
        it('GO:0000210', function () {
            chai.expect(staticData.getGoTermName('GO:0000210')).to.equal('NAD+ diphosphatase activity');
        });
        it('GO:0042773', function () {
            chai.expect(staticData.getGoTermName('GO:0042773')).to.equal('ATP synthesis coupled electron transport');
        });
        it('GO:0006085', function () {
            chai.expect(staticData.getGoTermName('GO:0006085')).to.equal('acetyl-CoA biosynthetic process');
        });
        it('GO:0043686', function () {
            chai.expect(staticData.getGoTermName('GO:0043686')).to.equal('co-translational protein modification');
        });
        it('GO:0030151', function () {
            chai.expect(staticData.getGoTermName('GO:0030151')).to.equal('molybdenum ion binding');
        });
        it('GO:0034613', function () {
            chai.expect(staticData.getGoTermName('GO:0034613')).to.equal('cellular protein localization');
        });
        it('GO:0016926', function () {
            chai.expect(staticData.getGoTermName('GO:0016926')).to.equal('protein desumoylation');
        });
        it('GO:0047661', function () {
            chai.expect(staticData.getGoTermName('GO:0047661')).to.equal('amino-acid racemase activity');
        });
        it('GO:0051031', function () {
            chai.expect(staticData.getGoTermName('GO:0051031')).to.equal('tRNA transport');
        });
        it('GO:0000922', function () {
            chai.expect(staticData.getGoTermName('GO:0000922')).to.equal('spindle pole');
        });
        it('GO:0050777', function () {
            chai.expect(staticData.getGoTermName('GO:0050777')).to.equal('negative regulation of immune response');
        });
        it('GO:0032271', function () {
            chai.expect(staticData.getGoTermName('GO:0032271')).to.equal('regulation of protein polymerization');
        });
        it('GO:0032273', function () {
            chai.expect(staticData.getGoTermName('GO:0032273')).to.equal('positive regulation of protein polymerization');
        });
        it('GO:0016832', function () {
            chai.expect(staticData.getGoTermName('GO:0016832')).to.equal('aldehyde-lyase activity');
        });
        it('GO:0080162', function () {
            chai.expect(staticData.getGoTermName('GO:0080162')).to.equal('intracellular auxin transport');
        });
        it('GO:0004430', function () {
            chai.expect(staticData.getGoTermName('GO:0004430')).to.equal('1-phosphatidylinositol 4-kinase activity');
        });
        it('GO:0046839', function () {
            chai.expect(staticData.getGoTermName('GO:0046839')).to.equal('phospholipid dephosphorylation');
        });
        it('GO:0070071', function () {
            chai.expect(staticData.getGoTermName('GO:0070071')).to.equal('proton-transporting two-sector ATPase complex assembly');
        });
        it('GO:0001907', function () {
            chai.expect(staticData.getGoTermName('GO:0001907')).to.equal('killing by symbiont of host cells');
        });
        it('GO:0051715', function () {
            chai.expect(staticData.getGoTermName('GO:0051715')).to.equal('cytolysis of cells of another organism');
        });
        it('GO:0048825', function () {
            chai.expect(staticData.getGoTermName('GO:0048825')).to.equal('cotyledon development');
        });
        it('GO:0006379', function () {
            chai.expect(staticData.getGoTermName('GO:0006379')).to.equal('mRNA cleavage');
        });
        it('GO:0080116', function () {
            chai.expect(staticData.getGoTermName('GO:0080116')).to.equal('glucuronoxylan glucuronosyltransferase activity');
        });
        it('GO:0016337', function () {
            chai.expect(staticData.getGoTermName('GO:0016337')).to.equal('cell-cell adhesion');
        });
        it('GO:0050879', function () {
            chai.expect(staticData.getGoTermName('GO:0050879')).to.equal('multicellular organismal movement');
        });
        it('GO:0042372', function () {
            chai.expect(staticData.getGoTermName('GO:0042372')).to.equal('phylloquinone biosynthetic process');
        });
        it('GO:0000798', function () {
            chai.expect(staticData.getGoTermName('GO:0000798')).to.equal('nuclear cohesin complex');
        });
        it('GO:0015099', function () {
            chai.expect(staticData.getGoTermName('GO:0015099')).to.equal('nickel ion transmembrane transporter activity');
        });
    });
}

function testGoTermIdToNodesMap() {

    describe('GoTermIdToNodesMap', function () {
        /*   it('length', function () {
         chai.expect(Object.keys(map).length).to.equal(3592);
         });*/
        it('GO:0010440 nodes', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0010440")).to.eql(['N26', 'N28', 'N42', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0051318', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0051318")).to.eql(['N26', 'N28', 'N42', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0000080', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0000080")).to.eql(['N26', 'N28', 'N42', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0000247', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0000247")).to.eql(['N26', 'N28', 'N29', 'N42', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0000741', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0000741")).to.eql(['N26', 'N28', 'N42', 'N94', 'N93', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0010197', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0010197")).to.eql(['N26', 'N28', 'N42', 'N94', 'N93', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0048284', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0048284")).to.eql(['N26', 'N28', 'N42', 'N94', 'N93', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0005319', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0005319")).to.eql(['N26',
                'N28',
                'N42',
                'N94',
                'N95',
                'N97',
                'N93',
                'N40',
                'N78',
                'N23',
                'N24',
                'N43']);
        });
        it('GO:0006900', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0006900")).to.eql(['N26', 'N28', 'N94', 'N95', 'N97', 'N93', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0048194', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0048194")).to.eql(['N26', 'N28', 'N94', 'N95', 'N97', 'N93', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0005548', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0005548")).to.eql(['N26', 'N28', 'N94', 'N95', 'N97', 'N93', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0008665', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0008665")).to.eql(['N26', 'N28', 'N94', 'N93', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0000215', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0000215")).to.eql(['N26', 'N28', 'N94', 'N93', 'N40', 'N23', 'N24', 'N43']);
        });
        it('GO:0071452', function () {
            chai.expect(staticData.getNodesForGoTerm("GO:0071452")).to.eql(['N26', 'N28', 'N42', 'N40', 'N23', 'N24', 'N43']);
        });
    });
}

function testGoTermIdToNodesMap() {

    describe('GoTermIdToNodesMap', function () {
            it('cellular biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('cellular biosynthetic process')).to.equal('GO:0044249');
            });
            it('water-soluble vitamin metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('water-soluble vitamin metabolic process')).to.equal('GO:0006767');
            });
            it('cellular metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('cellular metabolic process')).to.equal('GO:0044237');
            });
            it('nitrogen compound metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('nitrogen compound metabolic process')).to.equal('GO:0006807');
            });
            it('cytoplasmic part', function () {
                chai.expect(staticData.getGOtermIdByName('cytoplasmic part')).to.equal('GO:0044444');
            });
            it('intramolecular transferase activity, transferring hydroxy groups', function () {
                chai.expect(staticData.getGOtermIdByName('intramolecular transferase activity, transferring hydroxy groups')).to.equal('GO:0050486');
            });
            it('primary metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('primary metabolic process')).to.equal('GO:0044238');
            });
            it('enzyme activator activity', function () {
                chai.expect(staticData.getGOtermIdByName('enzyme activator activity')).to.equal('GO:0008047');
            });
            it('cofactor biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('cofactor biosynthetic process')).to.equal('GO:0051188');
            });
            it('cellular component organization or biogenesis at cellular level', function () {
                chai.expect(staticData.getGOtermIdByName('cellular component organization or biogenesis at cellular level')).to.equal('GO:0071841');
            });
            it('positive regulation of cellular process', function () {
                chai.expect(staticData.getGOtermIdByName('positive regulation of cellular process')).to.equal('GO:0048522');
            });
            it('nucleic acid metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('nucleic acid metabolic process')).to.equal('GO:0090304');
            });
            it('regulation of transcription', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of transcription')).to.equal('GO:0045449');
            });
            it('nucleoplasm part', function () {
                chai.expect(staticData.getGOtermIdByName('nucleoplasm part')).to.equal('GO:0044451');
            });
            it('mRNA metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('mRNA metabolic process')).to.equal('GO:0016071');
            });
            it('proteolysis involved in cellular protein catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('proteolysis involved in cellular protein catabolic process')).to.equal('GO:0051603');
            });
            it('modification-dependent protein catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('modification-dependent protein catabolic process')).to.equal('GO:0019941');
            });
            it('transferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('transferase activity')).to.equal('GO:0016740');
            });
            it('transferase activity, transferring phosphorus-containing groups', function () {
                chai.expect(staticData.getGOtermIdByName('transferase activity, transferring phosphorus-containing groups')).to.equal('GO:0016772');
            });
            it('nucleotidyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('nucleotidyltransferase activity')).to.equal('GO:0016779');
            });
            it('cation binding', function () {
                chai.expect(staticData.getGOtermIdByName('cation binding')).to.equal('GO:0043169');
            });
            it('pore complex', function () {
                chai.expect(staticData.getGOtermIdByName('pore complex')).to.equal('GO:0046930');
            });
            it('protein binding', function () {
                chai.expect(staticData.getGOtermIdByName('protein binding')).to.equal('GO:0005515');
            });
            it('cortical microtubule organization', function () {
                chai.expect(staticData.getGOtermIdByName('cortical microtubule organization')).to.equal('GO:0043622');
            });
            it('ATP binding', function () {
                chai.expect(staticData.getGOtermIdByName('ATP binding')).to.equal('GO:0005524');
            });
            it('peptidase activity', function () {
                chai.expect(staticData.getGOtermIdByName('peptidase activity')).to.equal('GO:0008233');
            });
            it('regulation of fatty acid metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of fatty acid metabolic process')).to.equal('GO:0019217');
            });
            it('acetylglutamate kinase regulator activity', function () {
                chai.expect(staticData.getGOtermIdByName('acetylglutamate kinase regulator activity')).to.equal('GO:0010307');
            });
            it('magnesium ion binding', function () {
                chai.expect(staticData.getGOtermIdByName('magnesium ion binding')).to.equal('GO:0000287');
            });
            it('chromatin remodeling', function () {
                chai.expect(staticData.getGOtermIdByName('chromatin remodeling')).to.equal('GO:0006338');
            });
            it('hydrolase activity, acting on acid anhydrides, in phosphorus-containing anhydrides', function () {
                chai.expect(staticData.getGOtermIdByName('hydrolase activity, acting on acid anhydrides, in phosphorus-containing anhydrides')).to.equal('GO:0016818');
            });
            it('extracellular membrane-bounded organelle', function () {
                chai.expect(staticData.getGOtermIdByName('extracellular membrane-bounded organelle')).to.equal('GO:0065010');
            });
            it('response to external stimulus', function () {
                chai.expect(staticData.getGOtermIdByName('response to external stimulus')).to.equal('GO:0009605');
            });
            it('citrate transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('citrate transmembrane transporter activity')).to.equal('GO:0015137');
            });
            it('integral to membrane', function () {
                chai.expect(staticData.getGOtermIdByName('integral to membrane')).to.equal('GO:0016021');
            });
            it('regulation of transcription elongation from RNA polymerase II promoter', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of transcription elongation from RNA polymerase II promoter')).to.equal('GO:0034243');
            });
            it('cellular protein metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('cellular protein metabolic process')).to.equal('GO:0044267');
            });
            it('response to osmotic stress', function () {
                chai.expect(staticData.getGOtermIdByName('response to osmotic stress')).to.equal('GO:0006970');
            });
            it('cellular response to cold', function () {
                chai.expect(staticData.getGOtermIdByName('cellular response to cold')).to.equal('GO:0070417');
            });
            it('endoplasmic reticulum', function () {
                chai.expect(staticData.getGOtermIdByName('endoplasmic reticulum')).to.equal('GO:0005783');
            });
            it('plastid envelope', function () {
                chai.expect(staticData.getGOtermIdByName('plastid envelope')).to.equal('GO:0009526');
            });
            it('outer membrane', function () {
                chai.expect(staticData.getGOtermIdByName('outer membrane')).to.equal('GO:0019867');
            });
            it('cellular homeostasis', function () {
                chai.expect(staticData.getGOtermIdByName('cellular homeostasis')).to.equal('GO:0019725');
            });
            it('branched chain family amino acid metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('branched chain family amino acid metabolic process')).to.equal('GO:0009081');
            });
            it('isoleucine metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('isoleucine metabolic process')).to.equal('GO:0006549');
            });
            it('cellular developmental process', function () {
                chai.expect(staticData.getGOtermIdByName('cellular developmental process')).to.equal('GO:0048869');
            });
            it('specification of axis polarity', function () {
                chai.expect(staticData.getGOtermIdByName('specification of axis polarity')).to.equal('GO:0065001');
            });
            it('cytochrome-c oxidase activity', function () {
                chai.expect(staticData.getGOtermIdByName('cytochrome-c oxidase activity')).to.equal('GO:0004129');
            });
            it('UDP-glycosyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('UDP-glycosyltransferase activity')).to.equal('GO:0008194');
            });
            it('response to jasmonic acid stimulus', function () {
                chai.expect(staticData.getGOtermIdByName('response to jasmonic acid stimulus')).to.equal('GO:0009753');
            });
            it('glucan metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('glucan metabolic process')).to.equal('GO:0044042');
            });
            it('transferase activity, transferring one-carbon groups', function () {
                chai.expect(staticData.getGOtermIdByName('transferase activity, transferring one-carbon groups')).to.equal('GO:0016741');
            });
            it('hydrolase activity, acting on ester bonds', function () {
                chai.expect(staticData.getGOtermIdByName('hydrolase activity, acting on ester bonds')).to.equal('GO:0016788');
            });
            it('pectate lyase activity', function () {
                chai.expect(staticData.getGOtermIdByName('pectate lyase activity')).to.equal('GO:0030570');
            });
            it('serine hydrolase activity', function () {
                chai.expect(staticData.getGOtermIdByName('serine hydrolase activity')).to.equal('GO:0017171');
            });
            it('cellular response to chemical stimulus', function () {
                chai.expect(staticData.getGOtermIdByName('cellular response to chemical stimulus')).to.equal('GO:0070887');
            });
            it('CUL4 RING ubiquitin ligase complex', function () {
                chai.expect(staticData.getGOtermIdByName('CUL4 RING ubiquitin ligase complex')).to.equal('GO:0080008');
            });
            it('protein modification process', function () {
                chai.expect(staticData.getGOtermIdByName('protein modification process')).to.equal('GO:0006464');
            });
            it('protein deacylation', function () {
                chai.expect(staticData.getGOtermIdByName('protein deacylation')).to.equal('GO:0035601');
            });
            it('cellular response to cytokinin stimulus', function () {
                chai.expect(staticData.getGOtermIdByName('cellular response to cytokinin stimulus')).to.equal('GO:0071368');
            });
            it('membrane fusion', function () {
                chai.expect(staticData.getGOtermIdByName('membrane fusion')).to.equal('GO:0061025');
            });
            it('DNA metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('DNA metabolic process')).to.equal('GO:0006259');
            });
            it('oxidoreductase activity, acting on the CH-OH group of donors, NAD or NADP as acceptor', function () {
                chai.expect(staticData.getGOtermIdByName('oxidoreductase activity, acting on the CH-OH group of donors, NAD or NADP as acceptor')).to.equal('GO:0016616');
            });
            it('cellular component assembly', function () {
                chai.expect(staticData.getGOtermIdByName('cellular component assembly')).to.equal('GO:0022607');
            });
            it('alkene biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('alkene biosynthetic process')).to.equal('GO:0043450');
            });
            it('protein binding, bridging', function () {
                chai.expect(staticData.getGOtermIdByName('protein binding, bridging')).to.equal('GO:0030674');
            });
            it('response to fungus', function () {
                chai.expect(staticData.getGOtermIdByName('response to fungus')).to.equal('GO:0009620');
            });
            it('negative regulation of RNA metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('negative regulation of RNA metabolic process')).to.equal('GO:0051253');
            });
            it('negative regulation of cellular process', function () {
                chai.expect(staticData.getGOtermIdByName('negative regulation of cellular process')).to.equal('GO:0048523');
            });
            it('regulation of gene-specific transcription', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of gene-specific transcription')).to.equal('GO:0032583');
            });
            it('fatty acid biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('fatty acid biosynthetic process')).to.equal('GO:0006633');
            });
            it('Ran GTPase activator activity', function () {
                chai.expect(staticData.getGOtermIdByName('Ran GTPase activator activity')).to.equal('GO:0005098');
            });
            it('regulation of meristem structural organization', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of meristem structural organization')).to.equal('GO:0009934');
            });
            it('regulation of response to stimulus', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of response to stimulus')).to.equal('GO:0048583');
            });
            it('RNA methylation', function () {
                chai.expect(staticData.getGOtermIdByName('RNA methylation')).to.equal('GO:0001510');
            });
            it('nucleotide kinase activity', function () {
                chai.expect(staticData.getGOtermIdByName('nucleotide kinase activity')).to.equal('GO:0019201');
            });
            it('inositol metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('inositol metabolic process')).to.equal('GO:0006020');
            });
            it('glucosyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('glucosyltransferase activity')).to.equal('GO:0046527');
            });
            it('protein serine/threonine phosphatase complex', function () {
                chai.expect(staticData.getGOtermIdByName('protein serine/threonine phosphatase complex')).to.equal('GO:0008287');
            });
            it('NADH dehydrogenase activity', function () {
                chai.expect(staticData.getGOtermIdByName('NADH dehydrogenase activity')).to.equal('GO:0003954');
            });
            it('cellular metabolic compound salvage', function () {
                chai.expect(staticData.getGOtermIdByName('cellular metabolic compound salvage')).to.equal('GO:0043094');
            });
            it('ATPase activity', function () {
                chai.expect(staticData.getGOtermIdByName('ATPase activity')).to.equal('GO:0016887');
            });
            it('regulation of flower development', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of flower development')).to.equal('GO:0009909');
            });
            it('sterol biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('sterol biosynthetic process')).to.equal('GO:0016126');
            });
            it('post-Golgi vesicle-mediated transport', function () {
                chai.expect(staticData.getGOtermIdByName('post-Golgi vesicle-mediated transport')).to.equal('GO:0006892');
            });
            it('vacuolar membrane', function () {
                chai.expect(staticData.getGOtermIdByName('vacuolar membrane')).to.equal('GO:0005774');
            });
            it('disulfide oxidoreductase activity', function () {
                chai.expect(staticData.getGOtermIdByName('disulfide oxidoreductase activity')).to.equal('GO:0015036');
            });
            it('photosynthetic electron transport chain', function () {
                chai.expect(staticData.getGOtermIdByName('photosynthetic electron transport chain')).to.equal('GO:0009767');
            });
            it('carbohydrate homeostasis', function () {
                chai.expect(staticData.getGOtermIdByName('carbohydrate homeostasis')).to.equal('GO:0033500');
            });
            it('cellular glucose homeostasis', function () {
                chai.expect(staticData.getGOtermIdByName('cellular glucose homeostasis')).to.equal('GO:0001678');
            });
            it('actin filament-based process', function () {
                chai.expect(staticData.getGOtermIdByName('actin filament-based process')).to.equal('GO:0030029');
            });
            it('glycerol-3-phosphate metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('glycerol-3-phosphate metabolic process')).to.equal('GO:0006072');
            });
            it('RNA polymerase II transcription factor activity', function () {
                chai.expect(staticData.getGOtermIdByName('RNA polymerase II transcription factor activity')).to.equal('GO:0003702');
            });
            it('cellular carbohydrate catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('cellular carbohydrate catabolic process')).to.equal('GO:0044275');
            });
            it('icosanoid biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('icosanoid biosynthetic process')).to.equal('GO:0046456');
            });
            it('oxidoreductase activity, acting on iron-sulfur proteins as donors, NAD or NADP as acceptor', function () {
                chai.expect(staticData.getGOtermIdByName('oxidoreductase activity, acting on iron-sulfur proteins as donors, NAD or NADP as acceptor')).to.equal('GO:0016731');
            });
            it('signal recognition particle', function () {
                chai.expect(staticData.getGOtermIdByName('signal recognition particle')).to.equal('GO:0048500');
            });
            it('SRP-dependent cotranslational protein targeting to membrane', function () {
                chai.expect(staticData.getGOtermIdByName('SRP-dependent cotranslational protein targeting to membrane')).to.equal('GO:0006614');
            });
            it('cell death', function () {
                chai.expect(staticData.getGOtermIdByName('cell death')).to.equal('GO:0008219');
            });
            it('systemic acquired resistance, salicylic acid mediated signaling pathway', function () {
                chai.expect(staticData.getGOtermIdByName('systemic acquired resistance, salicylic acid mediated signaling pathway')).to.equal('GO:0009862');
            });
            it('DNA packaging', function () {
                chai.expect(staticData.getGOtermIdByName('DNA packaging')).to.equal('GO:0006323');
            });
            it('oxo-acid-lyase activity', function () {
                chai.expect(staticData.getGOtermIdByName('oxo-acid-lyase activity')).to.equal('GO:0016833');
            });
            it('tryptophan metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('tryptophan metabolic process')).to.equal('GO:0006568');
            });
            it('protein maturation', function () {
                chai.expect(staticData.getGOtermIdByName('protein maturation')).to.equal('GO:0051604');
            });
            it('endocytosis', function () {
                chai.expect(staticData.getGOtermIdByName('endocytosis')).to.equal('GO:0006897');
            });
            it('oxidoreductase activity, acting on a sulfur group of donors, NAD or NADP as acceptor', function () {
                chai.expect(staticData.getGOtermIdByName('oxidoreductase activity, acting on a sulfur group of donors, NAD or NADP as acceptor')).to.equal('GO:0016668');
            });
            it('inositol-1,3,4-trisphosphate 5/6-kinase activity', function () {
                chai.expect(staticData.getGOtermIdByName('inositol-1,3,4-trisphosphate 5/6-kinase activity')).to.equal('GO:0035300');
            });
            it('threonine biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('threonine biosynthetic process')).to.equal('GO:0009088');
            });
            it('active transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('active transmembrane transporter activity')).to.equal('GO:0022804');
            });
            it('primary active transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('primary active transmembrane transporter activity')).to.equal('GO:0015399');
            });
            it('acetyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('acetyltransferase activity')).to.equal('GO:0016407');
            });
            it('developmental growth', function () {
                chai.expect(staticData.getGOtermIdByName('developmental growth')).to.equal('GO:0048589');
            });
            it('endoplasmic reticulum exit site', function () {
                chai.expect(staticData.getGOtermIdByName('endoplasmic reticulum exit site')).to.equal('GO:0070971');
            });
            it('macromolecule glycosylation', function () {
                chai.expect(staticData.getGOtermIdByName('macromolecule glycosylation')).to.equal('GO:0043413');
            });
            it('fatty acid catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('fatty acid catabolic process')).to.equal('GO:0009062');
            });
            it('regulation of seed germination', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of seed germination')).to.equal('GO:0010029');
            });
            it('vitamin binding', function () {
                chai.expect(staticData.getGOtermIdByName('vitamin binding')).to.equal('GO:0019842');
            });
            it('regulation of catalytic activity', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of catalytic activity')).to.equal('GO:0050790');
            });
            it('cellular response to phosphate starvation', function () {
                chai.expect(staticData.getGOtermIdByName('cellular response to phosphate starvation')).to.equal('GO:0016036');
            });
            it('membrane lipid metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('membrane lipid metabolic process')).to.equal('GO:0006643');
            });
            it('phospholipid binding', function () {
                chai.expect(staticData.getGOtermIdByName('phospholipid binding')).to.equal('GO:0005543');
            });
            it('L-galactose-1-phosphate phosphatase activity', function () {
                chai.expect(staticData.getGOtermIdByName('L-galactose-1-phosphate phosphatase activity')).to.equal('GO:0010347');
            });
            it('hexose metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('hexose metabolic process')).to.equal('GO:0019318');
            });
            it('fat-soluble vitamin biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('fat-soluble vitamin biosynthetic process')).to.equal('GO:0042362');
            });
            it('mitochondrial respiratory chain complex III', function () {
                chai.expect(staticData.getGOtermIdByName('mitochondrial respiratory chain complex III')).to.equal('GO:0005750');
            });
            it('negative regulation of photosynthesis, light reaction', function () {
                chai.expect(staticData.getGOtermIdByName('negative regulation of photosynthesis, light reaction')).to.equal('GO:0043155');
            });
            it('oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen, NADH or NADPH as one donor, and incorporation of one atom of oxygen', function () {
                chai.expect(staticData.getGOtermIdByName('oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen, NADH or NADPH as one donor, and incorporation of one atom of oxygen')).to.equal('GO:0016709');
            });
            it('regulation of anthocyanin metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of anthocyanin metabolic process')).to.equal('GO:0031537');
            });
            it('ribonucleoside binding', function () {
                chai.expect(staticData.getGOtermIdByName('ribonucleoside binding')).to.equal('GO:0032549');
            });
            it('regulation of phosphorylation', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of phosphorylation')).to.equal('GO:0042325');
            });
            it('protein tyrosine phosphatase activity', function () {
                chai.expect(staticData.getGOtermIdByName('protein tyrosine phosphatase activity')).to.equal('GO:0004725');
            });
            it('positive regulation of signaling', function () {
                chai.expect(staticData.getGOtermIdByName('positive regulation of signaling')).to.equal('GO:0023056');
            });
            it('diaminopimelate metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('diaminopimelate metabolic process')).to.equal('GO:0046451');
            });
            it('tetrapyrrole catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('tetrapyrrole catabolic process')).to.equal('GO:0033015');
            });
            it('superoxide dismutase activity', function () {
                chai.expect(staticData.getGOtermIdByName('superoxide dismutase activity')).to.equal('GO:0004784');
            });
            it('negative regulation of organelle organization', function () {
                chai.expect(staticData.getGOtermIdByName('negative regulation of organelle organization')).to.equal('GO:0010639');
            });
            it('regulation of cell cycle arrest', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of cell cycle arrest')).to.equal('GO:0071156');
            });
            it('negative regulation of nuclear division', function () {
                chai.expect(staticData.getGOtermIdByName('negative regulation of nuclear division')).to.equal('GO:0051784');
            });
            it('sphingoid biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('sphingoid biosynthetic process')).to.equal('GO:0046520');
            });
            it('riboflavin synthase activity', function () {
                chai.expect(staticData.getGOtermIdByName('riboflavin synthase activity')).to.equal('GO:0004746');
            });
            it('GPI anchor metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('GPI anchor metabolic process')).to.equal('GO:0006505');
            });
            it('thiolester hydrolase activity', function () {
                chai.expect(staticData.getGOtermIdByName('thiolester hydrolase activity')).to.equal('GO:0016790');
            });
            it('allantoin metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('allantoin metabolic process')).to.equal('GO:0000255');
            });
            it('extrinsic to internal side of plasma membrane', function () {
                chai.expect(staticData.getGOtermIdByName('extrinsic to internal side of plasma membrane')).to.equal('GO:0031234');
            });
            it('regulation of apoptosis', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of apoptosis')).to.equal('GO:0042981');
            });
            it('leaf morphogenesis', function () {
                chai.expect(staticData.getGOtermIdByName('leaf morphogenesis')).to.equal('GO:0009965');
            });
            it('ribonucleoprotein complex biogenesis', function () {
                chai.expect(staticData.getGOtermIdByName('ribonucleoprotein complex biogenesis')).to.equal('GO:0022613');
            });
            it('racemase and epimerase activity, acting on carbohydrates and derivatives', function () {
                chai.expect(staticData.getGOtermIdByName('racemase and epimerase activity, acting on carbohydrates and derivatives')).to.equal('GO:0016857');
            });
            it('wax biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('wax biosynthetic process')).to.equal('GO:0010025');
            });
            it('amine catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('amine catabolic process')).to.equal('GO:0009310');
            });
            it('cis-trans isomerase activity', function () {
                chai.expect(staticData.getGOtermIdByName('cis-trans isomerase activity')).to.equal('GO:0016859');
            });
            it('ceramide metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('ceramide metabolic process')).to.equal('GO:0006672');
            });
            it('transcription initiation factor activity', function () {
                chai.expect(staticData.getGOtermIdByName('transcription initiation factor activity')).to.equal('GO:0016986');
            });
            it('hyperosmotic response', function () {
                chai.expect(staticData.getGOtermIdByName('hyperosmotic response')).to.equal('GO:0006972');
            });
            it('FK506 binding', function () {
                chai.expect(staticData.getGOtermIdByName('FK506 binding')).to.equal('GO:0005528');
            });
            it('exonuclease activity', function () {
                chai.expect(staticData.getGOtermIdByName('exonuclease activity')).to.equal('GO:0004527');
            });
            it('protein-DNA complex subunit organization', function () {
                chai.expect(staticData.getGOtermIdByName('protein-DNA complex subunit organization')).to.equal('GO:0071824');
            });
            it('cytokinetic process', function () {
                chai.expect(staticData.getGOtermIdByName('cytokinetic process')).to.equal('GO:0032506');
            });
            it('sulfolipid metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('sulfolipid metabolic process')).to.equal('GO:0046505');
            });
            it('glucose-6-phosphate dehydrogenase activity', function () {
                chai.expect(staticData.getGOtermIdByName('glucose-6-phosphate dehydrogenase activity')).to.equal('GO:0004345');
            });
            it('L-methionine biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('L-methionine biosynthetic process')).to.equal('GO:0071265');
            });
            it('indole-3-acetonitrile nitrilase activity', function () {
                chai.expect(staticData.getGOtermIdByName('indole-3-acetonitrile nitrilase activity')).to.equal('GO:0080061');
            });
            it('copper ion transport', function () {
                chai.expect(staticData.getGOtermIdByName('copper ion transport')).to.equal('GO:0006825');
            });
            it('terpene metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('terpene metabolic process')).to.equal('GO:0042214');
            });
            it('negative regulation of two-component signal transduction system (phosphorelay)', function () {
                chai.expect(staticData.getGOtermIdByName('negative regulation of two-component signal transduction system (phosphorelay)')).to.equal('GO:0070298');
            });
            it('mitochondrion organization', function () {
                chai.expect(staticData.getGOtermIdByName('mitochondrion organization')).to.equal('GO:0007005');
            });
            it('nucleoside phosphate metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('nucleoside phosphate metabolic process')).to.equal('GO:0006753');
            });
            it('protein retention in ER lumen', function () {
                chai.expect(staticData.getGOtermIdByName('protein retention in ER lumen')).to.equal('GO:0006621');
            });
            it('ion channel activity', function () {
                chai.expect(staticData.getGOtermIdByName('ion channel activity')).to.equal('GO:0005216');
            });
            it('channel activity', function () {
                chai.expect(staticData.getGOtermIdByName('channel activity')).to.equal('GO:0015267');
            });
            it('acetyl-CoA C-acyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('acetyl-CoA C-acyltransferase activity')).to.equal('GO:0003988');
            });
            it('ribokinase activity', function () {
                chai.expect(staticData.getGOtermIdByName('ribokinase activity')).to.equal('GO:0004747');
            });
            it('ferric-chelate reductase activity', function () {
                chai.expect(staticData.getGOtermIdByName('ferric-chelate reductase activity')).to.equal('GO:0000293');
            });
            it('1,3-beta-D-glucan biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('1,3-beta-D-glucan biosynthetic process')).to.equal('GO:0006075');
            });
            it('ATP biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('ATP biosynthetic process')).to.equal('GO:0006754');
            });
            it('nucleoside triphosphate metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('nucleoside triphosphate metabolic process')).to.equal('GO:0009141');
            });
            it('ion transmembrane transport', function () {
                chai.expect(staticData.getGOtermIdByName('ion transmembrane transport')).to.equal('GO:0034220');
            });
            it('aminomethyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('aminomethyltransferase activity')).to.equal('GO:0004047');
            });
            it('NAD binding', function () {
                chai.expect(staticData.getGOtermIdByName('NAD binding')).to.equal('GO:0070403');
            });
            it('regulation of response to stress', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of response to stress')).to.equal('GO:0080134');
            });
            it('flower development', function () {
                chai.expect(staticData.getGOtermIdByName('flower development')).to.equal('GO:0009908');
            });
            it('NADH dehydrogenase complex (plastoquinone) assembly', function () {
                chai.expect(staticData.getGOtermIdByName('NADH dehydrogenase complex (plastoquinone) assembly')).to.equal('GO:0010258');
            });
            it('embryonic axis specification', function () {
                chai.expect(staticData.getGOtermIdByName('embryonic axis specification')).to.equal('GO:0000578');
            });
            it('cellular response to water deprivation', function () {
                chai.expect(staticData.getGOtermIdByName('cellular response to water deprivation')).to.equal('GO:0042631');
            });
            it('DNA helicase activity', function () {
                chai.expect(staticData.getGOtermIdByName('DNA helicase activity')).to.equal('GO:0003678');
            });
            it('regulation of GTP catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of GTP catabolic process')).to.equal('GO:0033124');
            });
            it('polynucleotide adenylyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('polynucleotide adenylyltransferase activity')).to.equal('GO:0004652');
            });
            it('elongation factor-2 kinase activity', function () {
                chai.expect(staticData.getGOtermIdByName('elongation factor-2 kinase activity')).to.equal('GO:0004686');
            });
            it('oxidoreductase activity, acting on a sulfur group of donors, oxygen as acceptor', function () {
                chai.expect(staticData.getGOtermIdByName('oxidoreductase activity, acting on a sulfur group of donors, oxygen as acceptor')).to.equal('GO:0016670');
            });
            it('cellular component disassembly at cellular level', function () {
                chai.expect(staticData.getGOtermIdByName('cellular component disassembly at cellular level')).to.equal('GO:0071845');
            });
            it('alpha,alpha-trehalase activity', function () {
                chai.expect(staticData.getGOtermIdByName('alpha,alpha-trehalase activity')).to.equal('GO:0004555');
            });
            it('ADP-ribose pyrophosphohydrolase activity', function () {
                chai.expect(staticData.getGOtermIdByName('ADP-ribose pyrophosphohydrolase activity')).to.equal('GO:0080041');
            });
            it('radical SAM enzyme activity', function () {
                chai.expect(staticData.getGOtermIdByName('radical SAM enzyme activity')).to.equal('GO:0070283');
            });
            it('cellular component macromolecule biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('cellular component macromolecule biosynthetic process')).to.equal('GO:0070589');
            });
            it('localization', function () {
                chai.expect(staticData.getGOtermIdByName('localization')).to.equal('GO:0051179');
            });
            it('cellular localization', function () {
                chai.expect(staticData.getGOtermIdByName('cellular localization')).to.equal('GO:0051641');
            });
            it('indole phytoalexin biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('indole phytoalexin biosynthetic process')).to.equal('GO:0009700');
            });
            it('kinetochore microtubule', function () {
                chai.expect(staticData.getGOtermIdByName('kinetochore microtubule')).to.equal('GO:0005828');
            });
            it('dihydropteroate synthase activity', function () {
                chai.expect(staticData.getGOtermIdByName('dihydropteroate synthase activity')).to.equal('GO:0004156');
            });
            it('positive regulation of immune response', function () {
                chai.expect(staticData.getGOtermIdByName('positive regulation of immune response')).to.equal('GO:0050778');
            });
            it('nucleocytoplasmic transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('nucleocytoplasmic transporter activity')).to.equal('GO:0005487');
            });
            it('lysine N-acetyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('lysine N-acetyltransferase activity')).to.equal('GO:0004468');
            });
            it('tRNA (guanine) methyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('tRNA (guanine) methyltransferase activity')).to.equal('GO:0016423');
            });
            it('pyridoxal phosphate binding', function () {
                chai.expect(staticData.getGOtermIdByName('pyridoxal phosphate binding')).to.equal('GO:0030170');
            });
            it('methionine gamma-lyase activity', function () {
                chai.expect(staticData.getGOtermIdByName('methionine gamma-lyase activity')).to.equal('GO:0018826');
            });
            it('microsome', function () {
                chai.expect(staticData.getGOtermIdByName('microsome')).to.equal('GO:0005792');
            });
            it('peroxisome localization', function () {
                chai.expect(staticData.getGOtermIdByName('peroxisome localization')).to.equal('GO:0060151');
            });
            it('tripeptide transport', function () {
                chai.expect(staticData.getGOtermIdByName('tripeptide transport')).to.equal('GO:0042939');
            });
            it('xanthophyll metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('xanthophyll metabolic process')).to.equal('GO:0016122');
            });
            it('sesquiterpenoid metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('sesquiterpenoid metabolic process')).to.equal('GO:0006714');
            });
            it('protein serine/threonine phosphatase activity', function () {
                chai.expect(staticData.getGOtermIdByName('protein serine/threonine phosphatase activity')).to.equal('GO:0004722');
            });
            it('anatomical structure arrangement', function () {
                chai.expect(staticData.getGOtermIdByName('anatomical structure arrangement')).to.equal('GO:0048532');
            });
            it('pyrimidine nucleotide biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('pyrimidine nucleotide biosynthetic process')).to.equal('GO:0006221');
            });
            it('Ras guanyl-nucleotide exchange factor activity', function () {
                chai.expect(staticData.getGOtermIdByName('Ras guanyl-nucleotide exchange factor activity')).to.equal('GO:0005088');
            });
            it('phosphatidylinositol-4,5-bisphosphate 5-phosphatase activity', function () {
                chai.expect(staticData.getGOtermIdByName('phosphatidylinositol-4,5-bisphosphate 5-phosphatase activity')).to.equal('GO:0004439');
            });
            it('purine ribonucleoside monophosphate biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('purine ribonucleoside monophosphate biosynthetic process')).to.equal('GO:0009168');
            });
            it('pantothenate kinase activity', function () {
                chai.expect(staticData.getGOtermIdByName('pantothenate kinase activity')).to.equal('GO:0004594');
            });
            it('L-tyrosine transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('L-tyrosine transmembrane transporter activity')).to.equal('GO:0005302');
            });
            it('gas transport', function () {
                chai.expect(staticData.getGOtermIdByName('gas transport')).to.equal('GO:0015669');
            });
            it('chloroplast nucleoid', function () {
                chai.expect(staticData.getGOtermIdByName('chloroplast nucleoid')).to.equal('GO:0042644');
            });
            it('nucleobase catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('nucleobase catabolic process')).to.equal('GO:0046113');
            });
            it('auxin transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('auxin transmembrane transporter activity')).to.equal('GO:0080161');
            });
            it('ribosome binding', function () {
                chai.expect(staticData.getGOtermIdByName('ribosome binding')).to.equal('GO:0043022');
            });
            it('pyridoxine biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('pyridoxine biosynthetic process')).to.equal('GO:0008615');
            });
            it('polyol catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('polyol catabolic process')).to.equal('GO:0046174');
            });
            it('sucrose:hydrogen symporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('sucrose:hydrogen symporter activity')).to.equal('GO:0008506');
            });
            it('oxidoreductase activity, acting on CH or CH2 groups', function () {
                chai.expect(staticData.getGOtermIdByName('oxidoreductase activity, acting on CH or CH2 groups')).to.equal('GO:0016725');
            });
            it('asparagine metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('asparagine metabolic process')).to.equal('GO:0006528');
            });
            it('floral organ abscission', function () {
                chai.expect(staticData.getGOtermIdByName('floral organ abscission')).to.equal('GO:0010227');
            });
            it('N-methyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('N-methyltransferase activity')).to.equal('GO:0008170');
            });
            it('purine base metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('purine base metabolic process')).to.equal('GO:0006144');
            });
            it('photoreceptor activity', function () {
                chai.expect(staticData.getGOtermIdByName('photoreceptor activity')).to.equal('GO:0009881');
            });
            it('stomatal movement', function () {
                chai.expect(staticData.getGOtermIdByName('stomatal movement')).to.equal('GO:0010118');
            });
            it('cellularization', function () {
                chai.expect(staticData.getGOtermIdByName('cellularization')).to.equal('GO:0007349');
            });
            it('acylglycerol O-acyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('acylglycerol O-acyltransferase activity')).to.equal('GO:0016411');
            });
            it('hexose catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('hexose catabolic process')).to.equal('GO:0019320');
            });
            it('ligase activity, forming nitrogen-metal bonds, forming coordination complexes', function () {
                chai.expect(staticData.getGOtermIdByName('ligase activity, forming nitrogen-metal bonds, forming coordination complexes')).to.equal('GO:0051003');
            });
            it('trichome morphogenesis', function () {
                chai.expect(staticData.getGOtermIdByName('trichome morphogenesis')).to.equal('GO:0010090');
            });
            it('DNA alkylation', function () {
                chai.expect(staticData.getGOtermIdByName('DNA alkylation')).to.equal('GO:0006305');
            });
            it('induction of apoptosis', function () {
                chai.expect(staticData.getGOtermIdByName('induction of apoptosis')).to.equal('GO:0006917');
            });
            it('transcription from RNA polymerase II promoter', function () {
                chai.expect(staticData.getGOtermIdByName('transcription from RNA polymerase II promoter')).to.equal('GO:0006366');
            });
            it('DNA-dependent DNA replication', function () {
                chai.expect(staticData.getGOtermIdByName('DNA-dependent DNA replication')).to.equal('GO:0006261');
            });
            it('dipeptidyl-peptidase activity', function () {
                chai.expect(staticData.getGOtermIdByName('dipeptidyl-peptidase activity')).to.equal('GO:0008239');
            });
            it('anion homeostasis', function () {
                chai.expect(staticData.getGOtermIdByName('anion homeostasis')).to.equal('GO:0055081');
            });
            it('etioplast', function () {
                chai.expect(staticData.getGOtermIdByName('etioplast')).to.equal('GO:0009513');
            });
            it('response to low fluence blue light stimulus by blue low-fluence system', function () {
                chai.expect(staticData.getGOtermIdByName('response to low fluence blue light stimulus by blue low-fluence system')).to.equal('GO:0010244');
            });
            it('stress granule', function () {
                chai.expect(staticData.getGOtermIdByName('stress granule')).to.equal('GO:0010494');
            });
            it('xylan metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('xylan metabolic process')).to.equal('GO:0045491');
            });
            it('Arp2/3 protein complex', function () {
                chai.expect(staticData.getGOtermIdByName('Arp2/3 protein complex')).to.equal('GO:0005885');
            });
            it('negative regulation of MAP kinase activity', function () {
                chai.expect(staticData.getGOtermIdByName('negative regulation of MAP kinase activity')).to.equal('GO:0043407');
            });
            it('hexokinase activity', function () {
                chai.expect(staticData.getGOtermIdByName('hexokinase activity')).to.equal('GO:0004396');
            });
            it('histidine phosphotransfer kinase activity', function () {
                chai.expect(staticData.getGOtermIdByName('histidine phosphotransfer kinase activity')).to.equal('GO:0009927');
            });
            it('urea transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('urea transmembrane transporter activity')).to.equal('GO:0015204');
            });
            it('protein dephosphorylation', function () {
                chai.expect(staticData.getGOtermIdByName('protein dephosphorylation')).to.equal('GO:0006470');
            });
            it('histidine metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('histidine metabolic process')).to.equal('GO:0006547');
            });
            it('adenylyl-sulfate reductase activity', function () {
                chai.expect(staticData.getGOtermIdByName('adenylyl-sulfate reductase activity')).to.equal('GO:0009973');
            });
            it('glycoprotein catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('glycoprotein catabolic process')).to.equal('GO:0006516');
            });
            it('vacuolar sorting signal binding', function () {
                chai.expect(staticData.getGOtermIdByName('vacuolar sorting signal binding')).to.equal('GO:0010209');
            });
            it('actin filament bundle assembly', function () {
                chai.expect(staticData.getGOtermIdByName('actin filament bundle assembly')).to.equal('GO:0051017');
            });
            it('arsenate reductase (glutaredoxin) activity', function () {
                chai.expect(staticData.getGOtermIdByName('arsenate reductase (glutaredoxin) activity')).to.equal('GO:0008794');
            });
            it('phosphatidylinositol metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('phosphatidylinositol metabolic process')).to.equal('GO:0046488');
            });
            it('sulfate transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('sulfate transmembrane transporter activity')).to.equal('GO:0015116');
            });
            it('nuclear membrane-endoplasmic reticulum network', function () {
                chai.expect(staticData.getGOtermIdByName('nuclear membrane-endoplasmic reticulum network')).to.equal('GO:0042175');
            });
            it('protein methylation', function () {
                chai.expect(staticData.getGOtermIdByName('protein methylation')).to.equal('GO:0006479');
            });
            it('threonine-tRNA ligase activity', function () {
                chai.expect(staticData.getGOtermIdByName('threonine-tRNA ligase activity')).to.equal('GO:0004829');
            });
            it('intracellular ligand-gated ion channel activity', function () {
                chai.expect(staticData.getGOtermIdByName('intracellular ligand-gated ion channel activity')).to.equal('GO:0005217');
            });
            it('phosphate transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('phosphate transmembrane transporter activity')).to.equal('GO:0015114');
            });
            it('pentose-phosphate shunt, non-oxidative branch', function () {
                chai.expect(staticData.getGOtermIdByName('pentose-phosphate shunt, non-oxidative branch')).to.equal('GO:0009052');
            });
            it('protein C-terminal S-isoprenylcysteine carboxyl O-methyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('protein C-terminal S-isoprenylcysteine carboxyl O-methyltransferase activity')).to.equal('GO:0004671');
            });
            it('adenine salvage', function () {
                chai.expect(staticData.getGOtermIdByName('adenine salvage')).to.equal('GO:0006168');
            });
            it('response to fructose stimulus', function () {
                chai.expect(staticData.getGOtermIdByName('response to fructose stimulus')).to.equal('GO:0009750');
            });
            it('response to gamma radiation', function () {
                chai.expect(staticData.getGOtermIdByName('response to gamma radiation')).to.equal('GO:0010332');
            });
            it('calcium ion transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('calcium ion transmembrane transporter activity')).to.equal('GO:0015085');
            });
            it('divalent inorganic cation homeostasis', function () {
                chai.expect(staticData.getGOtermIdByName('divalent inorganic cation homeostasis')).to.equal('GO:0072507');
            });
            it('regulation of anatomical structure size', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of anatomical structure size')).to.equal('GO:0090066');
            });
            it('nitric oxide metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('nitric oxide metabolic process')).to.equal('GO:0046209');
            });
            it('chromosome organization involved in meiosis', function () {
                chai.expect(staticData.getGOtermIdByName('chromosome organization involved in meiosis')).to.equal('GO:0070192');
            });
            it('reciprocal meiotic recombination', function () {
                chai.expect(staticData.getGOtermIdByName('reciprocal meiotic recombination')).to.equal('GO:0007131');
            });
            it('serine family amino acid biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('serine family amino acid biosynthetic process')).to.equal('GO:0009070');
            });
            it('nucleolus organizer region', function () {
                chai.expect(staticData.getGOtermIdByName('nucleolus organizer region')).to.equal('GO:0005731');
            });
            it('histone lysine methylation', function () {
                chai.expect(staticData.getGOtermIdByName('histone lysine methylation')).to.equal('GO:0034968');
            });
            it('telomere maintenance via telomere lengthening', function () {
                chai.expect(staticData.getGOtermIdByName('telomere maintenance via telomere lengthening')).to.equal('GO:0010833');
            });
            it('RNA-dependent DNA replication', function () {
                chai.expect(staticData.getGOtermIdByName('RNA-dependent DNA replication')).to.equal('GO:0006278');
            });
            it('cytokinin biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('cytokinin biosynthetic process')).to.equal('GO:0009691');
            });
            it('cell-cell signaling', function () {
                chai.expect(staticData.getGOtermIdByName('cell-cell signaling')).to.equal('GO:0007267');
            });
            it('signaling', function () {
                chai.expect(staticData.getGOtermIdByName('signaling')).to.equal('GO:0023052');
            });
            it('pyrimidine nucleoside monophosphate biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('pyrimidine nucleoside monophosphate biosynthetic process')).to.equal('GO:0009130');
            });
            it('thymidylate synthase activity', function () {
                chai.expect(staticData.getGOtermIdByName('thymidylate synthase activity')).to.equal('GO:0004799');
            });
            it('spermine synthase activity', function () {
                chai.expect(staticData.getGOtermIdByName('spermine synthase activity')).to.equal('GO:0016768');
            });
            it('peptide deformylase activity', function () {
                chai.expect(staticData.getGOtermIdByName('peptide deformylase activity')).to.equal('GO:0042586');
            });
            it('positive regulation of DNA replication', function () {
                chai.expect(staticData.getGOtermIdByName('positive regulation of DNA replication')).to.equal('GO:0045740');
            });
            it('acylglycerol transport', function () {
                chai.expect(staticData.getGOtermIdByName('acylglycerol transport')).to.equal('GO:0034196');
            });
            it('SOS response', function () {
                chai.expect(staticData.getGOtermIdByName('SOS response')).to.equal('GO:0009432');
            });
            it('defense response signaling pathway, resistance gene-dependent', function () {
                chai.expect(staticData.getGOtermIdByName('defense response signaling pathway, resistance gene-dependent')).to.equal('GO:0009870');
            });
            it('cytokinin receptor activity', function () {
                chai.expect(staticData.getGOtermIdByName('cytokinin receptor activity')).to.equal('GO:0009884');
            });
            it('pyridoxal phosphate biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('pyridoxal phosphate biosynthetic process')).to.equal('GO:0042823');
            });
            it('cellular calcium ion homeostasis', function () {
                chai.expect(staticData.getGOtermIdByName('cellular calcium ion homeostasis')).to.equal('GO:0006874');
            });
            it('nucleoside bisphosphate metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('nucleoside bisphosphate metabolic process')).to.equal('GO:0033865');
            });
            it('maintenance of fidelity involved in DNA-dependent DNA replication', function () {
                chai.expect(staticData.getGOtermIdByName('maintenance of fidelity involved in DNA-dependent DNA replication')).to.equal('GO:0045005');
            });
            it('establishment of mitochondrion localization', function () {
                chai.expect(staticData.getGOtermIdByName('establishment of mitochondrion localization')).to.equal('GO:0051654');
            });
            it('soluble NSF attachment protein activity', function () {
                chai.expect(staticData.getGOtermIdByName('soluble NSF attachment protein activity')).to.equal('GO:0005483');
            });
            it('positive regulation of cellular response to phosphate starvation', function () {
                chai.expect(staticData.getGOtermIdByName('positive regulation of cellular response to phosphate starvation')).to.equal('GO:0080040');
            });
            it('phosphatidylcholine-sterol O-acyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('phosphatidylcholine-sterol O-acyltransferase activity')).to.equal('GO:0004607');
            });
            it('thioester metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('thioester metabolic process')).to.equal('GO:0035383');
            });
            it('maturation of LSU-rRNA from tetracistronic rRNA transcript (SSU-rRNA, LSU-rRNA, 4.5S-rRNA, 5S-rRNA)', function () {
                chai.expect(staticData.getGOtermIdByName('maturation of LSU-rRNA from tetracistronic rRNA transcript (SSU-rRNA, LSU-rRNA, 4.5S-rRNA, 5S-rRNA)')).to.equal('GO:0000488');
            });
            it('lysine N-methyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('lysine N-methyltransferase activity')).to.equal('GO:0016278');
            });
            it('generative cell mitosis', function () {
                chai.expect(staticData.getGOtermIdByName('generative cell mitosis')).to.equal('GO:0055047');
            });

            it('cytokinesis after meiosis', function () {
                chai.expect(staticData.getGOtermIdByName('cytokinesis after meiosis')).to.equal('GO:0033206');
            });
            it('tRNA-specific ribonuclease activity', function () {
                chai.expect(staticData.getGOtermIdByName('tRNA-specific ribonuclease activity')).to.equal('GO:0004549');
            });
            it('malate transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('malate transmembrane transporter activity')).to.equal('GO:0015140');
            });
            it('glycine catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('glycine catabolic process')).to.equal('GO:0006546');
            });
            it('regulation of nucleoside metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of nucleoside metabolic process')).to.equal('GO:0009118');
            });
            it('kinase activator activity', function () {
                chai.expect(staticData.getGOtermIdByName('kinase activator activity')).to.equal('GO:0019209');
            });
            it('manganese ion transport', function () {
                chai.expect(staticData.getGOtermIdByName('manganese ion transport')).to.equal('GO:0006828');
            });
            it('1-aminocyclopropane-1-carboxylate deaminase activity', function () {
                chai.expect(staticData.getGOtermIdByName('1-aminocyclopropane-1-carboxylate deaminase activity')).to.equal('GO:0008660');
            });
            it('lipid transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('lipid transporter activity')).to.equal('GO:0005319');
            });
            it('regulation of glucan biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of glucan biosynthetic process')).to.equal('GO:0010962');
            });
            it('peptidyl-proline hydroxylation to 4-hydroxy-L-proline', function () {
                chai.expect(staticData.getGOtermIdByName('peptidyl-proline hydroxylation to 4-hydroxy-L-proline')).to.equal('GO:0018401');
            });
            it('pollen tube guidance', function () {
                chai.expect(staticData.getGOtermIdByName('pollen tube guidance')).to.equal('GO:0010183');
            });
            it('response to jasmonic acid stimulus involved in jasmonic acid and ethylene-dependent systemic resistance', function () {
                chai.expect(staticData.getGOtermIdByName('response to jasmonic acid stimulus involved in jasmonic acid and ethylene-dependent systemic resistance')).to.equal('GO:0032260');
            });
            it('non-photoreactive DNA repair', function () {
                chai.expect(staticData.getGOtermIdByName('non-photoreactive DNA repair')).to.equal('GO:0010213');
            });
            it('positive regulation of metalloenzyme activity', function () {
                chai.expect(staticData.getGOtermIdByName('positive regulation of metalloenzyme activity')).to.equal('GO:0048554');
            });
            it('clathrin coated vesicle membrane', function () {
                chai.expect(staticData.getGOtermIdByName('clathrin coated vesicle membrane')).to.equal('GO:0030665');
            });
            it('glucuronate metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('glucuronate metabolic process')).to.equal('GO:0019585');
            });
            it('skotomorphogenesis', function () {
                chai.expect(staticData.getGOtermIdByName('skotomorphogenesis')).to.equal('GO:0009647');
            });
            it('phosphatidic acid metabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('phosphatidic acid metabolic process')).to.equal('GO:0046473');
            });
            it('core RNA polymerase binding transcription factor activity', function () {
                chai.expect(staticData.getGOtermIdByName('core RNA polymerase binding transcription factor activity')).to.equal('GO:0000990');
            });
            it('ureide catabolic process', function () {
                chai.expect(staticData.getGOtermIdByName('ureide catabolic process')).to.equal('GO:0010136');
            });
            it('hydrolase activity, acting on carbon-nitrogen (but not peptide) bonds, in cyclic amidines', function () {
                chai.expect(staticData.getGOtermIdByName('hydrolase activity, acting on carbon-nitrogen (but not peptide) bonds, in cyclic amidines')).to.equal('GO:0016814');
            });
            it('petal development', function () {
                chai.expect(staticData.getGOtermIdByName('petal development')).to.equal('GO:0048441');
            });
            it('NAD+ diphosphatase activity', function () {
                chai.expect(staticData.getGOtermIdByName('NAD+ diphosphatase activity')).to.equal('GO:0000210');
            });
            it('ATP synthesis coupled electron transport', function () {
                chai.expect(staticData.getGOtermIdByName('ATP synthesis coupled electron transport')).to.equal('GO:0042773');
            });
            it('acetyl-CoA biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('acetyl-CoA biosynthetic process')).to.equal('GO:0006085');
            });
            it('co-translational protein modification', function () {
                chai.expect(staticData.getGOtermIdByName('co-translational protein modification')).to.equal('GO:0043686');
            });
            it('molybdenum ion binding', function () {
                chai.expect(staticData.getGOtermIdByName('molybdenum ion binding')).to.equal('GO:0030151');
            });
            it('cellular protein localization', function () {
                chai.expect(staticData.getGOtermIdByName('cellular protein localization')).to.equal('GO:0034613');
            });
            it('protein desumoylation', function () {
                chai.expect(staticData.getGOtermIdByName('protein desumoylation')).to.equal('GO:0016926');
            });
            it('amino-acid racemase activity', function () {
                chai.expect(staticData.getGOtermIdByName('amino-acid racemase activity')).to.equal('GO:0047661');
            });
            it('tRNA transport', function () {
                chai.expect(staticData.getGOtermIdByName('tRNA transport')).to.equal('GO:0051031');
            });
            it('spindle pole', function () {
                chai.expect(staticData.getGOtermIdByName('spindle pole')).to.equal('GO:0000922');
            });
            it('negative regulation of immune response', function () {
                chai.expect(staticData.getGOtermIdByName('negative regulation of immune response')).to.equal('GO:0050777');
            });
            it('regulation of protein polymerization', function () {
                chai.expect(staticData.getGOtermIdByName('regulation of protein polymerization')).to.equal('GO:0032271');
            });
            it('positive regulation of protein polymerization', function () {
                chai.expect(staticData.getGOtermIdByName('positive regulation of protein polymerization')).to.equal('GO:0032273');
            });
            it('aldehyde-lyase activity', function () {
                chai.expect(staticData.getGOtermIdByName('aldehyde-lyase activity')).to.equal('GO:0016832');
            });
            it('intracellular auxin transport', function () {
                chai.expect(staticData.getGOtermIdByName('intracellular auxin transport')).to.equal('GO:0080162');
            });
            it('1-phosphatidylinositol 4-kinase activity', function () {
                chai.expect(staticData.getGOtermIdByName('1-phosphatidylinositol 4-kinase activity')).to.equal('GO:0004430');
            });
            it('phospholipid dephosphorylation', function () {
                chai.expect(staticData.getGOtermIdByName('phospholipid dephosphorylation')).to.equal('GO:0046839');
            });
            it('proton-transporting two-sector ATPase complex assembly', function () {
                chai.expect(staticData.getGOtermIdByName('proton-transporting two-sector ATPase complex assembly')).to.equal('GO:0070071');
            });
            it('killing by symbiont of host cells', function () {
                chai.expect(staticData.getGOtermIdByName('killing by symbiont of host cells')).to.equal('GO:0001907');
            });
            it('cytolysis of cells of another organism', function () {
                chai.expect(staticData.getGOtermIdByName('cytolysis of cells of another organism')).to.equal('GO:0051715');
            });
            it('cotyledon development', function () {
                chai.expect(staticData.getGOtermIdByName('cotyledon development')).to.equal('GO:0048825');
            });
            it('mRNA cleavage', function () {
                chai.expect(staticData.getGOtermIdByName('mRNA cleavage')).to.equal('GO:0006379');
            });
            it('glucuronoxylan glucuronosyltransferase activity', function () {
                chai.expect(staticData.getGOtermIdByName('glucuronoxylan glucuronosyltransferase activity')).to.equal('GO:0080116');
            });
            it('cell-cell adhesion', function () {
                chai.expect(staticData.getGOtermIdByName('cell-cell adhesion')).to.equal('GO:0016337');
            });
            it('multicellular organismal movement', function () {
                chai.expect(staticData.getGOtermIdByName('multicellular organismal movement')).to.equal('GO:0050879');
            });
            it('phylloquinone biosynthetic process', function () {
                chai.expect(staticData.getGOtermIdByName('phylloquinone biosynthetic process')).to.equal('GO:0042372');
            });
            it('nuclear cohesin complex', function () {
                chai.expect(staticData.getGOtermIdByName('nuclear cohesin complex')).to.equal('GO:0000798');
            });
            it('nickel ion transmembrane transporter activity', function () {
                chai.expect(staticData.getGOtermIdByName('nickel ion transmembrane transporter activity')).to.equal('GO:0015099');
            });
        }
    );
}

function testGeneList() {
    describe('testGeneList', function () {
        it('At1g01040', function () {
            chai.expect(staticData.getGeneList()[0]).to.eql('At1g01040');
        });
        it('At1g01230', function () {
            chai.expect(staticData.getGeneList()[1]).to.eql('At1g01230');
        });
        it('At1g01260', function () {
            chai.expect(staticData.getGeneList()[2]).to.eql('At1g01260');
        });
        it('At1g01040', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g01040');
        });
        it('At1g03330', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g03330');
        });
        it('At1g05805', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g05805');
        });
        it('At1g08320', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g08320');
        });
        it('At1g10155', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g10155');
        });
        it('At1g13020', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g13020');
        });
        it('At1g15440', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g15440');
        });
        it('At1g17980', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g17980');
        });
        it('At1g20460', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g20460');
        });
        it('At1g23400', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g23400');
        });
        it('At1g27060', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g27060');
        });
        it('At1g30130', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g30130');
        });
        it('At1g33250', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g33250');
        });
        it('At1g44790', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g44790');
        });
        it('At1g49975', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g49975');
        });
        it('At1g52600', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g52600');
        });
        it('At1g55670', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g55670');
        });
        it('At1g60970', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g60970');
        });
        it('At1g64050', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g64050');
        });
        it('At1g66590', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g66590');
        });
        it('At1g69400', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g69400');
        });
        it('At1g72170', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g72170');
        });
        it('At1g75100', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g75100');
        });
        it('At1g77540', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g77540');
        });
        it('At1g80510', function () {
            chai.expect(staticData.getGeneList()).to.contain('At1g80510');
        });
        it('At2g03820', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g03820');
        });
        it('At2g13820', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g13820');
        });
        it('At2g18196', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g18196');
        });
        it('At2g20810', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g20810');
        });
        it('At2g24590', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g24590');
        });
        it('At2g26990', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g26990');
        });
        it('At2g30300', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g30300');
        });
        it('At2g32970', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g32970');
        });
        it('At2g35500', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g35500');
        });
        it('At2g38130', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g38130');
        });
        it('At2g40130', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g40130');
        });
        it('At2g42330', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g42330');
        });
        it('At2g44530', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g44530');
        });
        it('At2g46930', function () {
            chai.expect(staticData.getGeneList()).to.contain('At2g46930');
        });
        it('At3g01910', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g01910');
        });
        it('At3g03890', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g03890');
        });
        it('At3g06540', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g06540');
        });
        it('At3g08710', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g08710');
        });
        it('At3g10970', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g10970');
        });
        it('At3g12950', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g12950');
        });
        it('At3g15351', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g15351');
        });
        it('At3g17300', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g17300');
        });
        it('At3g19680', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g19680');
        });
        it('At3g21740', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g21740');
        });
        it('At3g24440', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g24440');
        });
        it('At3g26740', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g26740');
        });
        it('At3g42050', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g42050');
        });
        it('At3g47590', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g47590');
        });
        it('At3g50560', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g50560');
        });
        it('At3g52210', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g52210');
        });
        it('At3g54860', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g54860');
        });
        it('At3g57290', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g57290');
        });
        it('At3g60210', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g60210');
        });
        it('At3g63030', function () {
            chai.expect(staticData.getGeneList()).to.contain('At3g63030');
        });
        it('At4g01995', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g01995');
        });
        it('At4g04940', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g04940');
        });
        it('At4g10200', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g10200');
        });
        it('At4g13700', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g13700');
        });
        it('At4g16510', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g16510');
        });
        it('At4g19650', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g19650');
        });
        it('At4g22330', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g22330');
        });
        it('At4g25000', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g25000');
        });
        it('At4g27600', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g27600');
        });
        it('At4g30150', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g30150');
        });
        it('At4g32690', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g32690');
        });
        it('At4g34540', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g34540');
        });
        it('At4g37190', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g37190');
        });
        it('At4g39980', function () {
            chai.expect(staticData.getGeneList()).to.contain('At4g39980');
        });
        it('At5g03555', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g03555');
        });
        it('At5g05660', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g05660');
        });
        it('At5g07960', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g07960');
        });
        it('At5g10110', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g10110');
        });
        it('At5g12040', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g12040');
        });
        it('At5g14120', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g14120');
        });
        it('At5g16320', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g16320');
        });
        it('At5g18930', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g18930');
        });
        it('At5g20660', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g20660');
        });
        it('At5g23220', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g23220');
        });
        it('At5g25752', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g25752');
        });
        it('At5g35220', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g35220');
        });
        it('At5g39785', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g39785');
        });
        it('At5g42620', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g42620');
        });
        it('At5g44800', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g44800');
        });
        it('At5g47830', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g47830');
        });
        it('At5g50010', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g50010');
        });
        it('At5g52560', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g52560');
        });
        it('At5g55140', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g55140');
        });
        it('At5g57980', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g57980');
        });
        it('At5g60490', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g60490');
        });
        it('At5g63135', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g63135');
        });
        it('At5g65140', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g65140');
        });
        it('At5g67590', function () {
            chai.expect(staticData.getGeneList()).to.contain('At5g67590');
        });
    });
}

function testGeneMap() {
    it('At1g01040', function () {
        chai.expect(staticData.getNodesForGene('At1g01040')).to.eql(["N42", "N78", "N18", "N17"]);
    });
    it('At1g03330', function () {
        chai.expect(staticData.getNodesForGene('At1g03330')).to.eql(["N63", "N9", "N88", "N89", "N5", "N26", "N28", "N54", "N73", "N94", "N52", "N53", "N48", "N93", "N40", "N23", "N24", "N11"]);
    });
    it('At1g05805', function () {
        chai.expect(staticData.getNodesForGene('At1g05805')).to.eql(["N26", "N28", "N42", "N40", "N23", "N24", "N43"]);
    });
    it('At1g08320', function () {
        chai.expect(staticData.getNodesForGene('At1g08320')).to.eql(["N88", "N89", "N26", "N28", "N36", "N34", "N73", "N52", "N53", "N38", "N40", "N23", "N24", "N37", "N43"]);
    });
    it('At1g10155', function () {
        chai.expect(staticData.getNodesForGene('At1g10155')).to.eql(["N30", "N31", "N29", "N39", "N42", "N53", "N32", "N40"]);
    });
    it('At1g13020', function () {
        chai.expect(staticData.getNodesForGene('At1g13020')).to.eql(["N49", "N42", "N78", "N68"]);
    });
    it('At1g15440', function () {
        chai.expect(staticData.getNodesForGene('At1g15440')).to.eql(["N55", "N26", "N28", "N54", "N42", "N40", "N23", "N24", "N43"]);
    });
    it('At1g17980', function () {
        chai.expect(staticData.getNodesForGene('At1g17980')).to.eql(["N21", "N42", "N78"]);
    });
    it('At1g20460', function () {
        chai.expect(staticData.getNodesForGene('At1g20460')).to.eql(["N60", "N54", "N52", "N53"]);
    });
    it('At1g23400', function () {
        chai.expect(staticData.getNodesForGene('At1g23400')).to.eql(["N98", "N99", "N73", "N42", "N78", "N24"]);
    });
    it('At1g27060', function () {
        chai.expect(staticData.getNodesForGene('At1g27060')).to.eql(["N63", "N55", "N82", "N26", "N21", "N28", "N60", "N91", "N54", "N42", "N94", "N86", "N87", "N48", "N93", "N40", "N78", "N1", "N23", "N24", "N64", "N62", "N43"]);
    });
    it('At1g30130', function () {
        chai.expect(staticData.getNodesForGene('At1g30130')).to.eql(["N26", "N28", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At1g33250', function () {
        chai.expect(staticData.getNodesForGene('At1g33250')).to.eql(["N26", "N28", "N60", "N73", "N42", "N48", "N40", "N78", "N68", "N23", "N24", "N62", "N43"]);
    });
    it('At1g44790', function () {
        chai.expect(staticData.getNodesForGene('At1g44790')).to.eql(["N88", "N89", "N82", "N26", "N28", "N60", "N6", "N54", "N73", "N42", "N53", "N87", "N48", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At1g49975', function () {
        chai.expect(staticData.getNodesForGene('At1g49975')).to.eql(["N19", "N26", "N28", "N42", "N40", "N18", "N23", "N24", "N43"]);
    });
    it('At1g52600', function () {
        chai.expect(staticData.getNodesForGene('At1g52600')).to.eql(["N9", "N19", "N88", "N89", "N5", "N82", "N26", "N28", "N36", "N45", "N6", "N91", "N42", "N94", "N59", "N38", "N87", "N48", "N40", "N78", "N18", "N23", "N24", "N37", "N66", "N43"]);
    });
    it('At1g55670', function () {
        chai.expect(staticData.getNodesForGene('At1g55670')).to.eql(["N14", "N26", "N46", "N10", "N28", "N49", "N6", "N15", "N7", "N54", "N8", "N42", "N52", "N53", "N27", "N48", "N40", "N78", "N18", "N65", "N68", "N1", "N23", "N24", "N67", "N66", "N43"]);
    });
    it('At1g60970', function () {
        chai.expect(staticData.getNodesForGene('At1g60970')).to.eql(["N63", "N55", "N30", "N26", "N28", "N60", "N54", "N29", "N42", "N59", "N52", "N53", "N32", "N23", "N24", "N64", "N62", "N43"]);
    });
    it('At1g64050', function () {
        chai.expect(staticData.getNodesForGene('At1g64050')).to.eql(["N42"]);
    });
    it('At1g66590', function () {
        chai.expect(staticData.getNodesForGene('At1g66590')).to.eql(["N42", "N78"]);
    });
    it('At1g69400', function () {
        chai.expect(staticData.getNodesForGene('At1g69400')).to.eql(["N88", "N3", "N89", "N30", "N82", "N10", "N31", "N49", "N90", "N72", "N39", "N42", "N94", "N59", "N52", "N53", "N87", "N32", "N93", "N40", "N78", "N68", "N51", "N67", "N66"]);
    });
    it('At1g72170', function () {
        chai.expect(staticData.getNodesForGene('At1g72170')).to.eql(["N26", "N28", "N60", "N91", "N39", "N94", "N52", "N53", "N93", "N40", "N23", "N24", "N67", "N43"]);
    });
    it('At1g75100', function () {
        chai.expect(staticData.getNodesForGene('At1g75100')).to.eql(["N88", "N89", "N82", "N73", "N42", "N87", "N48", "N78"]);
    });
    it('At1g77540', function () {
        chai.expect(staticData.getNodesForGene('At1g77540')).to.eql(["N26", "N28", "N49", "N42", "N48", "N40", "N23", "N24", "N43"]);
    });
    it('At1g80510', function () {
        chai.expect(staticData.getNodesForGene('At1g80510')).to.eql(["N26", "N28", "N42", "N48", "N40", "N23", "N24", "N43"]);
    });
    it('At2g03820', function () {
        chai.expect(staticData.getNodesForGene('At2g03820')).to.eql(["N26", "N28", "N42", "N94", "N93", "N40", "N68", "N23", "N24", "N62", "N43"]);
    });
    it('At2g13820', function () {
        chai.expect(staticData.getNodesForGene('At2g13820')).to.eql(["N26", "N49", "N39", "N52", "N53", "N48", "N24"]);
    });
    it('At2g18196', function () {
        chai.expect(staticData.getNodesForGene('At2g18196')).to.eql(["N42"]);
    });
    it('At2g20810', function () {
        chai.expect(staticData.getNodesForGene('At2g20810')).to.eql(["N88", "N89", "N26", "N28", "N42", "N40", "N78", "N68", "N23", "N24", "N43"]);
    });
    it('At2g24590', function () {
        chai.expect(staticData.getNodesForGene('At2g24590')).to.eql(["N26", "N28", "N39", "N52", "N53", "N48", "N40", "N23", "N24", "N67", "N66", "N43"]);
    });
    it('At2g26990', function () {
        chai.expect(staticData.getNodesForGene('At2g26990')).to.eql(["N88", "N89", "N21", "N90", "N72", "N94", "N93", "N24", "N74"]);
    });
    it('At2g30300', function () {
        chai.expect(staticData.getNodesForGene('At2g30300')).to.eql(["N42", "N40"]);
    });
    it('At2g32970', function () {
        chai.expect(staticData.getNodesForGene('At2g32970')).to.eql(["N42", "N78"]);
    });
    it('At2g35500', function () {
        chai.expect(staticData.getNodesForGene('At2g35500')).to.eql(["N26", "N28", "N60", "N6", "N73", "N42", "N40", "N23", "N24", "N43"]);
    });
    it('At2g38130', function () {
        chai.expect(staticData.getNodesForGene('At2g38130')).to.eql(["N63", "N26", "N28", "N6", "N42", "N52", "N53", "N95", "N40", "N78", "N68", "N23", "N24", "N64", "N43"]);
    });
    it('At2g40130', function () {
        chai.expect(staticData.getNodesForGene('At2g40130')).to.eql(["N21"]);
    });
    it('At2g42330', function () {
        chai.expect(staticData.getNodesForGene('At2g42330')).to.eql(["N42", "N97", "N78"]);
    });
    it('At2g44530', function () {
        chai.expect(staticData.getNodesForGene('At2g44530')).to.eql(["N42"]);
    });
    it('At2g46930', function () {
        chai.expect(staticData.getNodesForGene('At2g46930')).to.eql(["N54", "N42", "N59"]);
    });
    it('At3g01910', function () {
        chai.expect(staticData.getNodesForGene('At3g01910')).to.eql(["N49", "N54", "N53", "N48", "N23", "N24"]);
    });
    it('At3g03890', function () {
        chai.expect(staticData.getNodesForGene('At3g03890')).to.eql(["N26", "N28", "N49", "N60", "N53", "N40", "N23", "N24", "N67", "N66", "N43"]);
    });
    it('At3g06540', function () {
        chai.expect(staticData.getNodesForGene('At3g06540')).to.eql(["N42"]);
    });
    it('At3g08710', function () {
        chai.expect(staticData.getNodesForGene('At3g08710')).to.eql(["N55", "N49", "N60", "N6", "N90", "N72", "N54", "N73", "N42", "N86", "N59", "N52", "N53", "N48", "N78"]);
    });
    it('At3g10970', function () {
        chai.expect(staticData.getNodesForGene('At3g10970')).to.eql(["N63", "N88", "N89", "N55", "N82", "N22", "N60", "N91", "N54", "N42", "N94", "N86", "N59", "N87", "N48", "N78", "N24", "N64", "N43"]);
    });
    it('At3g12950', function () {
        chai.expect(staticData.getNodesForGene('At3g12950')).to.eql(["N42", "N53"]);
    });
    it('At3g15351', function () {
        chai.expect(staticData.getNodesForGene('At3g15351')).to.eql(["N9", "N26", "N10", "N28", "N27", "N40", "N68", "N23", "N24", "N11", "N43"]);
    });
    it('At3g17300', function () {
        chai.expect(staticData.getNodesForGene('At3g17300')).to.eql(["N26", "N28", "N73", "N94", "N86", "N53", "N93", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At3g19680', function () {
        chai.expect(staticData.getNodesForGene('At3g19680')).to.eql(["N26", "N28", "N60", "N54", "N42", "N94", "N93", "N40", "N23", "N24", "N43"]);
    });
    it('At3g21740', function () {
        chai.expect(staticData.getNodesForGene('At3g21740')).to.eql(["N26", "N46", "N28", "N60", "N73", "N42", "N48", "N40", "N23", "N24", "N43"]);
    });
    it('At3g24440', function () {
        chai.expect(staticData.getNodesForGene('At3g24440')).to.eql(["N49", "N60", "N73", "N42", "N95", "N56", "N67", "N66"]);
    });
    it('At3g26740', function () {
        chai.expect(staticData.getNodesForGene('At3g26740')).to.eql(["N19", "N55", "N26", "N33", "N28", "N54", "N39", "N42", "N53", "N27", "N40", "N18", "N68", "N23", "N24", "N43"]);
    });
    it('At3g42050', function () {
        chai.expect(staticData.getNodesForGene('At3g42050')).to.eql(["N26", "N28", "N39", "N40", "N23", "N24", "N67", "N66", "N43"]);
    });
    it('At3g47590', function () {
        chai.expect(staticData.getNodesForGene('At3g47590')).to.eql(["N19", "N88", "N89", "N26", "N60", "N90", "N91", "N72", "N73", "N42", "N52", "N53", "N95", "N27", "N48", "N97", "N78", "N24"]);
    });
    it('At3g50560', function () {
        chai.expect(staticData.getNodesForGene('At3g50560')).to.eql(["N2", "N98", "N26", "N99", "N28", "N40", "N23", "N24", "N43"]);
    });
    it('At3g52210', function () {
        chai.expect(staticData.getNodesForGene('At3g52210')).to.eql(["N26", "N46", "N49", "N73", "N48", "N78", "N24"]);
    });
    it('At3g54860', function () {
        chai.expect(staticData.getNodesForGene('At3g54860')).to.eql(["N88", "N89", "N91", "N42", "N94", "N52", "N53", "N95", "N97", "N93", "N78"]);
    });
    it('At3g57290', function () {
        chai.expect(staticData.getNodesForGene('At3g57290')).to.eql(["N54"]);
    });
    it('At3g60210', function () {
        chai.expect(staticData.getNodesForGene('At3g60210')).to.eql(["N63", "N9", "N19", "N3", "N98", "N5", "N14", "N26", "N46", "N10", "N28", "N49", "N6", "N90", "N15", "N7", "N72", "N54", "N73", "N8", "N42", "N94", "N59", "N53", "N48", "N93", "N40", "N65", "N23", "N24", "N64", "N11", "N43"]);
    });
    it('At3g63030', function () {
        chai.expect(staticData.getNodesForGene('At3g63030')).to.eql(["N88", "N89", "N46", "N45", "N91", "N42", "N94", "N48", "N93", "N78"]);
    });
    it('At4g01995', function () {
        chai.expect(staticData.getNodesForGene('At4g01995')).to.eql(["N26", "N28", "N42", "N52", "N53", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At4g04940', function () {
        chai.expect(staticData.getNodesForGene('At4g04940')).to.eql(["N26", "N28", "N7", "N8", "N42", "N94", "N93", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At4g10200', function () {
        chai.expect(staticData.getNodesForGene('At4g10200')).to.eql(["N63", "N88", "N89", "N82", "N87", "N48", "N78", "N64", "N62", "N43"]);
    });
    it('At4g13700', function () {
        chai.expect(staticData.getNodesForGene('At4g13700')).to.eql(["N21", "N42", "N78"]);
    });
    it('At4g16510', function () {
        chai.expect(staticData.getNodesForGene('At4g16510')).to.eql(["N88", "N89", "N49", "N39", "N42", "N78"]);
    });
    it('At4g19650', function () {
        chai.expect(staticData.getNodesForGene('At4g19650')).to.eql(["N19", "N26", "N28", "N42", "N40", "N78", "N18", "N23", "N24", "N43"]);
    });
    it('At4g22330', function () {
        chai.expect(staticData.getNodesForGene('At4g22330')).to.eql(["N26", "N28", "N48", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At4g25000', function () {
        chai.expect(staticData.getNodesForGene('At4g25000')).to.eql(["N19", "N88", "N89", "N26", "N28", "N40", "N78", "N18", "N23", "N24", "N43"]);
    });
    it('At4g27600', function () {
        chai.expect(staticData.getNodesForGene('At4g27600')).to.eql(["N88", "N89", "N82", "N26", "N28", "N42", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At4g30150', function () {
        chai.expect(staticData.getNodesForGene('At4g30150')).to.eql(["N63", "N26", "N28", "N40", "N23", "N24", "N64", "N62", "N43"]);
    });
    it('At4g32690', function () {
        chai.expect(staticData.getNodesForGene('At4g32690')).to.eql(["N88", "N89", "N26", "N28", "N60", "N42", "N48", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At4g34540', function () {
        chai.expect(staticData.getNodesForGene('At4g34540')).to.eql(["N19", "N26", "N28", "N73", "N42", "N94", "N93", "N40", "N18", "N23", "N24", "N43"]);
    });
    it('At4g37190', function () {
        chai.expect(staticData.getNodesForGene('At4g37190')).to.eql(["N60", "N54", "N42", "N94", "N78", "N68"]);
    });
    it('At4g39980', function () {
        chai.expect(staticData.getNodesForGene('At4g39980')).to.eql(["N21", "N29", "N42", "N18", "N24"]);
    });
    it('At5g03555', function () {
        chai.expect(staticData.getNodesForGene('At5g03555')).to.eql(["N26", "N28", "N42", "N40", "N23", "N24", "N43"]);
    });
    it('At5g05660', function () {
        chai.expect(staticData.getNodesForGene('At5g05660')).to.eql(["N42"]);
    });
    it('At5g07960', function () {
        chai.expect(staticData.getNodesForGene('At5g07960')).to.eql(["N63", "N19", "N55", "N26", "N31", "N28", "N60", "N7", "N54", "N8", "N42", "N86", "N32", "N40", "N78", "N18", "N23", "N24", "N64", "N62", "N43"]);
    });
    it('At5g10110', function () {
        chai.expect(staticData.getNodesForGene('At5g10110')).to.eql(["N73", "N42", "N78"]);
    });
    it('At5g12040', function () {
        chai.expect(staticData.getNodesForGene('At5g12040')).to.eql(["N19", "N26", "N46", "N28", "N42", "N52", "N53", "N48", "N40", "N18", "N23", "N24", "N43"]);
    });
    it('At5g14120', function () {
        chai.expect(staticData.getNodesForGene('At5g14120')).to.eql(["N24"]);
    });
    it('At5g16320', function () {
        chai.expect(staticData.getNodesForGene('At5g16320')).to.eql(["N91", "N94", "N93", "N68", "N67", "N66"]);
    });
    it('At5g18930', function () {
        chai.expect(staticData.getNodesForGene('At5g18930')).to.eql(["N42"]);
    });
    it('At5g20660', function () {
        chai.expect(staticData.getNodesForGene('At5g20660')).to.eql(["N88", "N89", "N26", "N28", "N60", "N54", "N42", "N52", "N53", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At5g23220', function () {
        chai.expect(staticData.getNodesForGene('At5g23220')).to.eql(["N55", "N26", "N28", "N60", "N6", "N7", "N54", "N39", "N8", "N42", "N52", "N53", "N40", "N23", "N24", "N43"]);
    });
    it('At5g25752', function () {
        chai.expect(staticData.getNodesForGene('At5g25752')).to.eql(["N6", "N42", "N78"]);
    });
    it('At5g35220', function () {
        chai.expect(staticData.getNodesForGene('At5g35220')).to.eql(["N88", "N89", "N26", "N28", "N54", "N39", "N42", "N53", "N40", "N78", "N23", "N24", "N67", "N66", "N43"]);
    });
    it('At5g39785', function () {
        chai.expect(staticData.getNodesForGene('At5g39785')).to.eql(["N88", "N89", "N26", "N28", "N49", "N91", "N94", "N52", "N53", "N95", "N97", "N93", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At5g42620', function () {
        chai.expect(staticData.getNodesForGene('At5g42620')).to.eql(["N2", "N98", "N26", "N28", "N42", "N40", "N68", "N23", "N24", "N43"]);
    });
    it('At5g44800', function () {
        chai.expect(staticData.getNodesForGene('At5g44800')).to.eql(["N26", "N28", "N40", "N23", "N24", "N43"]);
    });
    it('At5g47830', function () {
        chai.expect(staticData.getNodesForGene('At5g47830')).to.eql(["N26", "N28", "N42", "N52", "N53", "N48", "N40", "N23", "N24", "N43"]);
    });
    it('At5g50010', function () {
        chai.expect(staticData.getNodesForGene('At5g50010')).to.eql(["N55", "N26", "N60", "N54", "N24", "N67"]);
    });
    it('At5g52560', function () {
        chai.expect(staticData.getNodesForGene('At5g52560')).to.eql(["N21", "N10", "N49", "N42", "N78", "N23", "N67", "N66"]);
    });
    it('At5g55140', function () {
        chai.expect(staticData.getNodesForGene('At5g55140')).to.eql(["N26", "N28", "N49", "N42", "N52", "N53", "N95", "N97", "N40", "N78", "N23", "N24", "N43"]);
    });
    it('At5g57980', function () {
        chai.expect(staticData.getNodesForGene('At5g57980')).to.eql(["N55", "N10", "N49", "N60", "N54", "N39", "N42", "N94", "N52", "N53", "N48", "N93", "N78"]);
    });
    it('At5g60490', function () {
        chai.expect(staticData.getNodesForGene('At5g60490')).to.eql(["N26", "N28", "N49", "N42", "N52", "N53", "N40", "N23", "N24", "N43"]);
    });
    it('At5g63135', function () {
        chai.expect(staticData.getNodesForGene('At5g63135')).to.eql(["N42"]);
    });
    it('At5g65140', function () {
        chai.expect(staticData.getNodesForGene('At5g65140')).to.eql(["N26", "N28", "N36", "N60", "N72", "N76", "N77", "N42", "N86", "N38", "N40", "N78", "N23", "N24", "N67", "N74", "N66", "N43"]);
    });
    it('At5g67590', function () {
        chai.expect(staticData.getNodesForGene('At5g67590')).to.eql(["N88", "N89", "N55", "N82", "N26", "N28", "N60", "N90", "N72", "N54", "N73", "N42", "N94", "N86", "N59", "N52", "N53", "N95", "N87", "N27", "N48", "N97", "N93", "N40", "N78", "N23", "N24", "N43"]);
    });
}

setUpStaticData();
testGoTermIdToNodesMap();
testGoIdToNameMap();
testGoTermIdToNodesMap();
testPValues();
testHighLows();
testGeneList();
testGeneMap();
// testGotermlist?