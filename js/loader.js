var loader = (function () {

    var config;

    addEventListener('fileUploaded', function (e) {
        processUpload(e);
    }, false);

    addEventListener('configLoadCompleteInternal', function (e) {
        config = e.data;
        var evt = document.createEvent("Event");
        evt.initEvent("configLoadComplete", true, true);
        document.dispatchEvent(evt);
    }, false);

    {
        var upLoadMode = ""; // either GENES or GO_TERMS or SAVED_STATE

        function uploadGenes() {
            upLoadMode = "GENES";
            fileUpLoader.startRead("uploadGenes");
        }

        function uploadGoTerms() {
            upLoadMode = "GO_TERMS";
            fileUpLoader.startRead("uploadGoTerms");
        }
    }

    /** If "upload genes" button has been pressed, this will process an uploaded text file and filter the gene list.
     If "upload GO terms" has been pressed, this will process a text file and filter the gene list.
     */
    function processUpload(e) {
        if (upLoadMode == "GENES") {
            genericLineParser.parseResults(e.data);
            treeComponents.setSelectedGenes(genericLineParser.getLines());
        }
        else if (upLoadMode == "GO_TERMS") {
            genericLineParser.parseResults(e.data);
            treeComponents.setSelectedGoTerms(genericLineParser.getLines());
        }
        else if (upLoadMode == "SAVED_STATE") {
            var lines = e.data.split("\n");
            var mode = "";
            var loHiArray = [];
            var genes = [];
            var goTerms = [];
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i].trim();
                if (line == "GENES") {
                    mode = "GENES";
                }
                else if (line == "GO TERMS") {
                    mode = "GO TERMS";
                }
                else if (line == "PBS") {
                    mode = "";
                    loHiArray.push(lines[i + 1]);
                    loHiArray.push(lines[i + 2]);
                }
                else if (mode == "GENES") {
                    genes.push(line);
                }
                else if (mode == "GO TERMS") {
                    goTerms.push(line);
                }
            }
            treeComponents.loadState(goTerms, genes, loHiArray);
        }
    }

    function loadCss(filename) {
        var fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", filename);
        if (typeof fileref != "undefined") {
            document.getElementsByTagName("head")[0].appendChild(fileref)
        }
    }

    function resolvePath(fileName) {
        var index = fileName.indexOf("//");
        if (index > -1) {
            s1 = fileName.substring(0, index);
            s2 = fileName.substring(index + 1);
            s1 = s1.substring(0, s1.lastIndexOf("/"));
            return s1 + s2;
        }
        else {
            return fileName;
        }
    }

    function genericLoad(fileName, eventName, failEventName) {
        console.log(fileName);
        //fileName = resolvePath(fileName);
        console.log(fileName);
        if (fileName) {
            $.ajax({
                type: "GET",
                url: fileName,
                success: function (data, textStatus, jqXHR) {
                    console.info("Success loading " + fileName);
                    var evt = document.createEvent("Event");
                    evt.initEvent(eventName, true, true);
                    evt.data = data;
                    document.dispatchEvent(evt);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.error("error in loading " + fileName +
                        " " + errorThrown
                    );
                    var evt = document.createEvent("Event");
                    evt.initEvent(failEventName, true, true);
                    document.dispatchEvent(evt);
                }
            });
        }
        else {
            console.error("No fileName for " + eventName);
        }
    }

    function saveSvg() {
        //console.dir(document.getElementById("PhyloViz"));
        var svgString = $("#chart").html();
        var index = svgString.indexOf("/svg\">") + 6;
        var start = svgString.substring(0, index);
        var end = svgString.substring(index);
        var cssString = "<defs><style type=\"text/css\"><![CDATA[.link {stroke: #ccc; stroke-width: 1;}]]></style></defs>";
        return start + cssString + end;
    }


    function saveState() {
        var s = "GENES\n";
        var len = dynamicData.selectedGenes().length;
        for (var i = 0; i < len; i++) {
            s += dynamicData.selectedGenes()[i] + "\n";
        }
        s += "GO TERMS\n";
        var len = dynamicData.selectedGOterms().length;
        for (var i = 0; i < len; i++) {
            s += dynamicData.selectedGOterms()[i] + "\n";
        }
        s += "PBS\n";
        s += dynamicData.getlowHighArray_AbsPBS()[0] + "\n";
        s += dynamicData.getlowHighArray_AbsPBS()[1];
        return s;
    }

    function loadState() {
        upLoadMode = "SAVED_STATE";
        fileUpLoader.startRead("upLoadState");
    }

    /**
     Triggers event 'configLoadComplete'
     */
    function loadConfig(configPath) {
        genericLoad(configPath, "configLoadCompleteInternal");
    }

    /**
     Triggers event 'newickLoadComplete'
     */
    function loadNewick(path) {
        genericLoad(path + config["newickFile"], "newickLoadComplete");
    }

    /**
     Triggers event 'pbsLoadComplete'
     */
    function loadPBS(path) {
        genericLoad(path + config["pbsFile"], "pbsLoadComplete");
    }

    /**
     Triggers event 'GeneToGOFileLoadComplete'
     */
    function loadGeneToGoFile(path) {
        var fileName = config["geneToGOFile"];
        console.info("loading " + path + fileName);
        $.getJSON(
            path + fileName,
            function (data) {
                console.info("Success loading " + path + fileName);
                var evt = document.createEvent("Event");
                evt.initEvent("GeneToGOFileLoadComplete", true, true);
                evt.data = data;
                document.dispatchEvent(evt);
            });

        //genericLoad(config["geneToGOFile"], "GeneToGOFileLoadComplete");
    }

    /**
     Triggers event 'GoTermDescriptionFileLoadComplete'
     */
    function loadGoTermDescriptionFile(path) {
        genericLoad(path + config["GOannotationFile"], "GoTermDescriptionFileLoadComplete");
    }

    function getConfig() {
        return config;
    }

    return {
        getConfig: getConfig,
        loadConfig: loadConfig,
        loadCss: loadCss,
        loadPBS: loadPBS,
        //loadGOtoGeneFile:loadGOtoGeneFile,
        genericLoad: genericLoad,
        uploadGenes: uploadGenes,
        uploadGoTerms: uploadGoTerms,
        loadGeneToGoFile: loadGeneToGoFile,
        loadGoTermDescriptionFile: loadGoTermDescriptionFile,
        loadNewick: loadNewick,
        saveSvg: saveSvg,
        saveState: saveState,
        loadState: loadState
    };

}());

genericLineParser = function () {
    var lines = [];

    function parseResults(data) {
        lines.length = 0;
        var tokens = data.split("\n");
        var length = tokens.length;
        console.log("genericLineParser: parsing " + length + " tokens.")
        for (var i = 0; i < length; i++) {
            lines.push(tokens[i]);
        }
    }

    return {
        parseResults: parseResults,
        getLines: function () {
            return lines;
        }
    }
}();