var controller = function () {

    var uidSelectMode = false;
    var currentUidData = {genes: [], goTerms: []};
    var termType = "Go Terms*";
    var measureType = "PBS*";
    var currentGeneObject;
    var codonAlignmentsFolder;
    var proteinAlignmentsFolder;

    addEventListener('addedNodeToUIDNodes', function (e) {
        if ($("#genesUIDcheck").attr('checked')) {
            console.log("use Genes")
            var labelName = " Genes";
            var typeData = e.data.genes;
        }
        else {
            console.log("use Terms")
            var labelName = " Go Terms";
            var typeData = e.data.goTerms;
        }
        currentUidData = e.data;
        setUIDData(labelName, typeData);
    }, false);

    addEventListener('nodeClicked', function (e) {
        console.log(d3.event)
        d = e.data.node;
        if (controller.getUIDSelectMode()) {
            if (!d.nodeType || !d.nodeType == "species") {
                if (staticData.addToUIDNodes(d.id)) {
                    d.uidSelected = true;
                }
                else {
                    d.uidSelected = false;
                }
            }
            bigPlantTree.updateRoot();
        }
        else {
            if (d3.event.shiftKey) {
                //tooltips.showPBSInfo(d, true, d3.event.pageX, d3.event.pageY);
                collapse.toggleCollapse(d);
                //tooltips.showClickOptions(d);
            } else if (d3.event.altKey) {
                var n1 = dynamicData.getPairwisePatristicNode1();
                if (!n1) {
                    dynamicData.setPairwisePatristicNode1(d.name);
                }
                else {
                    dynamicData.setPairwisePatristicNode2(d.name);
                    overlay.addAlert("distance b/n " + tooltips.getPairwisePatristicNode1()
                        + " + " + d.name + " = " + staticData.getPairwisePatristicDistance(tooltips.getPairwisePatristicNode1(), d.name))
                    dynamicData.setPairwisePatristicNode1(null);
                }
            }
            else {
                tooltips.showClickOptions(d)
            }
        }
    }, false);

    addEventListener('nodeMouseover', function (e) {
        d = e.data.node;
        if (!controller.getUIDSelectMode()) {
            var data = {name: d.name, keepUp: false, x: d3.event.pageX, y: d3.event.pageY};
            util.dispatchEvent("showPBSInfo", data);
        }
    }, false);

    function setUIDData(labelName, typeData) {
        $("#nodesToComparePanel").text("Nodes: " + staticData.getUIDNodes());
        $("#unionHeader").html("Union - " + typeData.union.length + labelName + "&nbsp;&nbsp;&nbsp;&nbsp;<span id='saveUnion'></span>&nbsp;&nbsp;<input class='clipboard' type='image' src='images/clippy.svg' width='18' height='18' data-clipboard-target='#unionComparePanel'>");
        $("#intersectHeader").html("Intersect - " + typeData.intersect.length + labelName + "&nbsp;&nbsp;&nbsp;&nbsp;<span id='saveIntersect'></span>&nbsp;&nbsp;<input class='clipboard' type='image' src='images/clippy.svg' width='18' height='18' data-clipboard-target='#intersectComparePanel'>");
        $("#differenceHeader").html("Difference - " + typeData.difference.length + labelName + "&nbsp;&nbsp;&nbsp;&nbsp;<span id='saveDifference'></span>&nbsp;&nbsp;<input class='clipboard' type='image' src='images/clippy.svg' width='18' height='18' data-clipboard-target='#differenceComparePanel'>");
        var unionText = ("" + typeData.union).replace(/,/g, ", ");
        $("#unionComparePanel").text(unionText);
        var intersectText = ("" + typeData.intersect).replace(/,/g, ", ");
        $("#intersectComparePanel").text(intersectText);
        var differenceText = ("" + typeData.difference).replace(/,/g, ", ");
        $("#differenceComparePanel").text(differenceText);

        Downloadify.create('saveUnion', {
            filename: function () {
                return "union.txt";
            },
            data: function () {
                return unionText;
            },
            onComplete: function () {
                overlay.addAlert('Saved!');
            },
            onCancel: function () {
            },
            onError: function () {
                overlay.addAlert('You must put something in the File Contents or there will be nothing to save!');
            },
            swf: 'lib/downloadify.swf',
            downloadImage: 'images/save_icon.png',
            width: 20,
            height: 20,
            transparent: true,
            append: false
        });
        Downloadify.create('saveDifference', {
            filename: function () {
                return "difference.txt";
            },
            data: function () {
                return differenceText
            },
            onComplete: function () {
                overlay.addAlert('Saved!');
            },
            onCancel: function () {
            },
            onError: function () {
                overlay.addAlert('You must put something in the File Contents or there will be nothing to save!');
            },
            swf: 'lib/downloadify.swf',
            downloadImage: 'images/save_icon.png',
            width: 20,
            height: 20,
            transparent: true,
            append: false
        });
        Downloadify.create('saveIntersect', {
            filename: function () {
                return "intersect.txt";
            },
            data: function () {
                return intersectText;
            },
            onComplete: function () {
                overlay.addAlert('Saved!');
            },
            onCancel: function () {
            },
            onError: function () {
                overlay.addAlert('You must put something in the File Contents or there will be nothing to save!');
            },
            swf: 'lib/downloadify.swf',
            downloadImage: 'images/save_icon.png',
            width: 20,
            height: 20,
            transparent: true,
            append: false
        });
    }

    function setUIDType(type) {
        if (type == "genes") {
            setUIDData(" genes", currentUidData.genes);
        }
        else {
            setUIDData(" " + termType + "s", currentUidData.goTerms);
        }
    }

    function changeTaxonomyLevel(level) {
        bigPlantTree.setTaxonomyLevel(level);
        bigPlantTree.updateRoot();
    }

    function toggleHideUnionIntersectDifferencePanel() {
        if ($("#toggleHideUnionIntersectDifference").text() == "Show Union/Intersect/Difference") {
            $("#uidComparePanel").show();
            $("#toggleHideUnionIntersectDifference").text("Hide Union/Intersect/Difference");
        }
        else {
            $("#uidComparePanel").hide();
            $("#toggleHideUnionIntersectDifference").text("Show Union/Intersect/Difference");
        }
    }

    function openHelpWindow() {
        window.open("Help.html", 'Help', 'width=600,height=600,left=200,titlebar=no,toolbar=no,location=0,scrollbars=1').focus();
    }

    function toggleHideBranchLengths() {
        if ($("#toggleHideBranchLengths").text() == "Show Branch Length") {
            bigPlantTree.setUseBranchLengthForNodeLabel(true);
            bigPlantTree.updateRoot();
            $("#toggleHideBranchLengths").text("Hide Branch Length");
        }
        else {
            bigPlantTree.setUseBranchLengthForNodeLabel(false);
            bigPlantTree.updateRoot();
            $("#toggleHideBranchLengths").text("Show Branch Length");
        }
    }

    function filterGoTerms(event) {
        console.log(event);
        // treeComponents.filterGoTerms();
    }

    function toggleFilters() {
        if ($("#filters").is(":visible")) {
            $("#filters").hide("blind", "fast");
            $("#toggleFilters").text("Show Filters");
        }
        else {
            $("#filters").show("blind", "fast");
            $("#toggleFilters").text("Hide Filters");
        }
    }

    function toggleLayout() {
        if ($("#toggleLayout").text() == "Phylogram") {
            bigPlantTree.setUsePhylogramLayout(true);
            $("#toggleHideBranchLengths").show();
            bigPlantTree.updateRoot();
            $("#toggleLayout").text("Cladogram");
        }
        else {
            bigPlantTree.setUsePhylogramLayout(false);
            $("#toggleHideBranchLengths").hide();
            bigPlantTree.updateRoot();
            $("#toggleLayout").text("Phylogram");
        }
    }

    function toggleUIDSelection() {
        if ($("#toggleUIDselectButton").text() == "Start Selecting") {
            uidSelectMode = true;
            $("#uidComparePanel").show();
            $("#toggleUIDselectButton").text("Stop Selecting");
        }
        else {
            uidSelectMode = false;
            $("#toggleUIDselectButton").text("Start Selecting");
        }
    }

    function clearSelectedNodes() {
        staticData.walkTree(function (node) {
            if (node.uidSelected) {
                node.uidSelected = false;
            }
        });
        bigPlantTree.updateRoot();
        staticData.clearUIDNodes();
        currentUidData = {genes: [], goTerms: []};
        $("#nodesToComparePanel").text("Nodes: ");
        $("#unionHeader").text("Union");
        $("#intersectHeader").text("Intersect");
        $("#differenceHeader").text("Difference");
        $("#unionComparePanel").text("");
        $("#intersectComparePanel").text("");
        $("#differenceComparePanel").text("");
    }

    addEventListener("showPBSInfo", function (e) {
        console.log("got showPBSInfo event");
        if (typeof e.data === 'string') {
            data = JSON.parse(e.data);
        }
        else {
            data = e.data;
        }
        tooltips.showPBSInfo(staticData.getNodeFromRootByName(data.name), data.keepUp, data.x, data.y);
    }, false);

    addEventListener("union", function (e) {
        console.log("got union event");
    }, false);


    addEventListener("intersect", function (e) {
        console.log("got intersect event");
    }, false);


    addEventListener("ppd", function (e) {
        console.log("got ppd event");
    }, false);

    // protein and codon alignment display
    function openGeneOverlay(gene) {
        if (!currentGeneObject) {
            currentGeneObject = {gene: gene};
            loader.genericLoad("data/" + app.getFolder() + "/" + codonAlignmentsFolder + "/" + gene + ".fna", "codonAlignmentsLoaded", "codonAlignmentsFailed");
        }
    }

    addEventListener("codonAlignmentsLoaded", function (e) {
        console.log("codonAlignmentsLoaded");
        currentGeneObject.codonAlignments = e.data;
        loader.genericLoad("data/" + app.getFolder() + "/" + proteinAlignmentsFolder + "/" + currentGeneObject.gene + ".aln", "proteinAlignmentsLoaded", "proteinAlignmentsFailed");
    }, false);

    addEventListener("codonAlignmentsFailed", function (e) {
        loader.genericLoad("data/" + app.getFolder() + "/" + proteinAlignmentsFolder + "/" + currentGeneObject.gene + ".aln", "proteinAlignmentsLoaded", "proteinAlignmentsFailed");
    }, false);

    addEventListener("proteinAlignmentsLoaded", function (e) {
        console.log("proteinAlignmentsLoaded");
        currentGeneObject.proteinAlignments = e.data;
        overlay.addGeneOverlay(currentGeneObject);
        currentGeneObject = null;
    }, false);

    addEventListener("proteinAlignmentsFailed", function (e) {
        overlay.addGeneOverlay(currentGeneObject);
        currentGeneObject = null;
    }, false);

    // uid display
    function getUIDSelectMode() {
        console.log("controller.getuidselectmode: " + uidSelectMode);
        return uidSelectMode;
    }

    function setUIDSelectMode(m) {
        uidSelectMode = m;
    }

    // sets display term, for exammple 'PBS' or 'KEGG'
    function setTermType(tt) {
        termType = tt;
    }

    function getTermType() {
        return termType;
    }

    // set display measurement
    function setMeasureType(tt) {
        measureType = tt;
    }

    function getMeasureType() {
        return measureType;
    }

    return {
        toggleFilters: toggleFilters,
        toggleLayout: toggleLayout,
        getUIDSelectMode: getUIDSelectMode,
        setUIDSelectMode: setUIDSelectMode,
        toggleUIDSelection: toggleUIDSelection,
        clearSelectedNodes: clearSelectedNodes,
        setTermType: setTermType,
        getTermType: getTermType,
        setMeasureType: setMeasureType,
        getMeasureType: getMeasureType,
        setCodonAlignmentsFolder: function (ca) {
            codonAlignmentsFolder = ca;
        },
        setProteinAlignmentsFolder: function (pa) {
            proteinAlignmentsFolder = pa;
        },
        setUIDType: setUIDType,
        changeTaxonomyLevel: changeTaxonomyLevel,
        toggleHideBranchLengths: toggleHideBranchLengths,
        openHelpWindow: openHelpWindow,
        openGeneOverlay: openGeneOverlay,
        toggleHideUnionIntersectDifferencePanel: toggleHideUnionIntersectDifferencePanel
    }
}();