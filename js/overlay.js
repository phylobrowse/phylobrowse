/**
 * Class to organize the Heatmap and Save overlays
 */
var overlay = {};

overlay.addAlert = function (msg) {
    var template = $(app.getTemplates()).filter('#createAlertContainer').html();
    var obj = {};
    obj.msg = msg;
    $("body").append(Mustache.to_html(template, obj));

    $('#alertContainer').css('width', 300 + 'px');
    $('#alertContainer').draggable();
    util.centerElement('#alertContainer');
};

overlay.addHelpOverlay = function () {
    overlay.removeHelpOverlay();
    overlay.removeGeneOverlay();
    var template = $(app.getTemplates()).filter('#createHelpContainer').html();
    $("body").append(Mustache.to_html(template, {}));
    $('#helpContainer').draggable();

    $('#helpContainer').css('width', '600px');
    $('#helpContainer').css('left', 0);

    util.centerElementHorizontally('#helpContainer');
};

overlay.removeHelpOverlay = function () {
    $('#helpContainer').remove();
};

overlay.addGeneOverlay = function (geneObject) { // geneObject has fields gene, codonAlignments, proteinAlignements
    overlay.removeHeatmapOverlay();
    overlay.removeHelpOverlay();
    overlay.removeGeneOverlay();
    var info = staticData.getGeneInfoForGene(geneObject.gene);
    if (!info && !geneObject.codonAlignments && !geneObject.proteinAlignments) {
        overlay.addAlert("No alignments for "+geneObject.gene);
    }
    else {
        geneObject.geneInfo = info;
        var template = $(app.getTemplates()).filter('#createGeneContainer').html();
        $("body").append(Mustache.to_html(template, geneObject));
        util.centerElementHorizontally('#geneContainer');
    }
};

overlay.removeGeneOverlay = function () {
    $('#geneContainer').remove();
};

overlay.addSaveOverlay = function () {
    overlay.removeSaveOverlay();
    var template = $(app.getTemplates()).filter('#createSaveButtonsContainer').html();
    $("body").append(template);
    util.centerElement("#saveButtonsContainer");

    Downloadify.create('saveState', {
        filename: function () {
            return "state.txt";
        },
        data: function () {
            return loader.saveState();
        },
        onComplete: function () {
            overlay.removeSaveOverlay();
            overlay.addAlert('Saved!');
        },
        onCancel: function () {
        },
        onError: function () {
            overlay.addAlert('You must put something in the File Contents or there will be nothing to save!');
        },
        swf: 'lib/downloadify.swf',
        downloadImage: 'images/save_state.jpg',
        width: 100,
        height: 30,
        transparent: true,
        append: false
    });

    Downloadify.create('saveSVG', {
        filename: function () {
            return "phyloBrowseGraph.svg";
        },
        data: function () {
            return loader.saveSvg();
        },
        onComplete: function () {
            overlay.removeSaveOverlay();
            overlay.addAlert('Saved!');
        },
        onCancel: function () {
        },
        onError: function () {
        },
        swf: 'lib/downloadify.swf',
        downloadImage: 'images/save_state.jpg',
        width: 100,
        height: 30,
        transparent: true,
        append: false
    });
}

overlay.removeSaveOverlay = function () {
    $('#saveButtonsContainer').remove();
};

overlay.addHeatMapOverlay = function () {
    overlay.removeHeatmapOverlay();
    var yLabels = getGoTermsForNodes();
    if (yLabels.length > 0 && dynamicData.getKeepExpandedList()) {
        //$('<div id="overlayContainer" class="ui-widget-content overlayContainer"></div>')
        // todo - remove html
        var template = $(app.getTemplates()).filter('#createHeatmapContainer').html();
        $("body").append(Mustache.to_html(template, {}));

        var hm = overlay.showHeatmap(yLabels);
        $('#overlayContainer').css('width', hm.getWidth() + 100 + 'px');
        $('#overlayContainer').draggable();
        $('#heatMapContainer').css('left', 0);

        util.centerElement('#overlayContainer');
    }
    else {
        overlay.addAlert("No overrepresented GO terms found for the highlighted nodes.")
    }

    /*
     for keepExpandedList nodes
     for each go term list per node
     add to matrix
     */
    function getGoTermsForNodes() {
        var yLabels = []; // the GO term names
        var length = dynamicData.getKeepExpandedList().length;
        for (var i = 0; i < length; i++) {
            var node = dynamicData.getKeepExpandedList()[i];
            var goTerms = staticData.getPBS()[node].ovrTerm;
            if (goTerms) {
                for (var j = 0; j < goTerms.length; j++) {
                    // get index of GO term name or, if not encountered before, add it to yLabels
                    var index = $.inArray(goTerms[j].desc, yLabels);
                    if (index < 0) {
                        index = yLabels.push(goTerms[j].desc);
                        index--;
                        //console.debug("pushing " + goTerms[j].desc + " to index " + index);
                    }
                }
            }
        }
        util.sortListAlphabetically(yLabels);
        return yLabels;
    }
};

overlay.showHeatmap = function (yLabels) {
    var data = [];
    if (dynamicData.getKeepExpandedList()) { // array of nodes, ie: ["N1", "N2"]
        util.sortListAlphabetically(dynamicData.getKeepExpandedList());
        // Note: i is the column, index is the row
        var listOfNodesToUse = [];
        var numNodesWithData = 0;

        _.each(dynamicData.getKeepExpandedList(), function (node) {
            var goTerms = staticData.getPBS()[node].ovrTerm;
            if (goTerms && goTerms.length > 0) {
                data[numNodesWithData] = [];
                _.each(goTerms, function (goTerm) {
                    // add to matrix rows
                    var index = $.inArray(goTerm.desc, yLabels);
                    data[numNodesWithData][index] = goTerm.pValue;
                    //console.debug("data [" + numNodesWithData + "][" + index + "] = " + data[numNodesWithData][index]);
                });

            }
            numNodesWithData++;
            listOfNodesToUse.push(node);
        });

        var hm = heatmap(data, listOfNodesToUse, yLabels);
        hm.setConvertOpacityFunction(function (val) {
            var v = Math.log(1 / val) / 5; // the 5 is a total hack
            console.debug(val + " >> " + v);
            return v;
        });
    }
    hm.drawChart();
    return hm;
};

overlay.showHeatmapFake = function () { // for testing
    var xLabels = ['January', 'February', "March", "April", "May", "July"];
    var yLabels = ['a', 'b', "ccc", "dddddd", "e"];
    var data = [
        [1, 0, 0, 1, 0],
        [0, 0, 0, 1, 1],
        [1, .3, 0, .5, 0],
        [0, 1, 1, 1, 1],
        [1, 1, 0, .2, 1],
        [1, .3, 0, .5, 0]
    ];

    var hm = heatmap(data, xLabels, yLabels);
    hm.drawChart();
    return hm;
};

overlay.removeHeatmapOverlay = function () {
    $('#heatMapContainer > svg').remove();
    $('#overlayContainer > heatMapContainer').remove();
    $('#overlayContainer').remove();
};
