/**
 * expects a matrix of values - [][]
 * an array of x labels
 * an array of y labels
 * @param data
 */
var heatmap = function (data, xLabels, yLabels) {
    var matrix = data;

    var cellSize = 14;
    var hPadding = 300;
    var vPadding = 100;
    var width = 0;
    var height = 0;


    var drawChart = function () {
        width = xLabels.length * cellSize + hPadding + 20;
        height = yLabels.length * cellSize + vPadding + 5;
        var canvas = Raphael("heatMapContainer", width, height);

        var length = matrix.length;
        for (var i = 0; i < length; i++) {
            console.debug("matrix at " + i);
            var array = matrix[i];
            if (array) {
                var length2 = array.length;
                for (var j = 0; j < length2; j++) {
                    var r = canvas.rect((i * cellSize) + hPadding, (j * cellSize) + vPadding, cellSize, cellSize);
                    if (array[j] == undefined) {
                        r.attr({opacity:0});
                    }
                    else {
                        var opacity = convertOpacityFunction(array[j]);
                        r.attr({opacity:opacity, title:yLabels[j] + "/" + xLabels[i]+" = "+array[j]});
                    }
                    //console.debug("opacity at " + i + ", " + j + " = " + array[j]);
                    r.node.setAttribute("class", "heatMapRect");
                }
            }
        }

        var hEnd = hPadding + ((xLabels.length) * cellSize);
        for (var i = 0; i <= yLabels.length; i++) {
            var yPos = (i * cellSize) + vPadding;
            var l = canvas.path("M" + hPadding + " " + yPos + "L" + hEnd + " " + yPos);
            l.node.setAttribute("class", "heatMapLine");
        }

        var vEnd = vPadding + ((yLabels.length) * cellSize);
        for (var i = 0; i <= xLabels.length; i++) {
            var xPos = (i * cellSize) + hPadding;
            var l = canvas.path("M" + xPos + " " + vPadding + "L" + xPos + " " + vEnd);
            l.node.setAttribute("class", "heatMapLine");
        }

        for (var i = 0; i < yLabels.length; i++) {
            var t = canvas.text(hPadding - 5, (i * cellSize) + vPadding + (cellSize / 2), yLabels[i]);
            t.attr({'text-anchor':'end', 'font-size':cellSize - (cellSize/6)});
        }

        for (var i = 0; i < xLabels.length; i++) {
            var t = canvas.text((i * cellSize) + hPadding + (cellSize / 1.5), vPadding - 5, xLabels[i]);
            t.rotate(-90);
            t.attr({'text-anchor':'start', 'font-size':cellSize - (cellSize/6)});
        }
    };


    var getWidth = function () {
        return width;
    };

    var getHeight = function () {
        return height;
    };

    var convertOpacityFunction = function(val) {
        return val;
    }

    return {
        setConvertOpacityFunction: function(f) {
            convertOpacityFunction = f;
        },
        drawChart:drawChart,
        getWidth:getWidth,
        getHeight:getHeight
    }
};