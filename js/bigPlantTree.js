//noinspection LossyEncoding
var bigPlantTree = (function () {
    var tree,
        vis,
        diagonal,
        usePhylogramLayout = false,
        useBranchLengthForNodeLabel = true;

    var branchLengthScale;

    var VERTICAL_NODE_SPACING = 20;

    var margins = [20, 120, 20, 120],
        w = 1600 - margins[1] - margins[3],
        h = 2000 - margins[0] - margins[2],
        idCounter = 0,
        duration = 500;

    var sizeScale = d3.scale.linear(); // for scaling size of nodes/circles

    var taxonomyLevel;

    function setTaxonomyLevel(l) {
        console.log("setting taxonomy level to " + l);
        taxonomyLevel = l;
        console.log(taxonomyLevel);
    }

    function getNumLeaves() {
        var nodes = tree.nodes(staticData.getRoot());
        var numLeaves = 0;
        nodes.forEach(function (d) {
            if (!d.children) {
                numLeaves++;
            }
        });
        return numLeaves;
    }

    function resize(width) {
        w = width - margins[1] - margins[3];
        updateRoot();
    }

    function createVis(width) {
        if (width) {
            w = width - margins[1] - margins[3];
        }
        console.debug("setting up node size range with " + staticData.getLowestAvgPBSValue() + " and " + staticData.getHighestAvgPBSValue());
        sizeScale.domain([staticData.getLowestAvgPBSValue(), staticData.getHighestAvgPBSValue()]);
        sizeScale.range([3, 8]);
        tree = d3.layout.cluster();

        // figure out tree height
        h = getNumLeaves() * VERTICAL_NODE_SPACING;
        //console.debug("w & h = " + w + " & " + h);

        tree.size([h, w]); // need custom "separation" for radial

        diagonal = d3.phylogram.rightAngleDiagonal();

        vis = d3.select("#chart").append("svg")
            .attr("id", "PhyloViz")
            .attr("width", w + margins[1] + margins[3])
            .attr("height", h + margins[0] + margins[2])
            .attr("xmlns", "http://www.w3.org/2000/svg")
            .append("g")
            .attr("transform", "translate(" + margins[3] + "," + margins[0] + ")");
    }

    function createTree(width) {
        createVis(width);
        update(staticData.getRoot());
    }

    function getUsePhylogramLayout() {
        return usePhylogramLayout;
    }

    function setUsePhylogramLayout(_usePhylogramLayout) {
        usePhylogramLayout = _usePhylogramLayout;
    }

    function setUseBranchLengthForNodeLabel(_usePhylogramLayout) {
        useBranchLengthForNodeLabel = _usePhylogramLayout;
    }

    function updateRoot() {
        d3.select("#Phylo")
        update(staticData.getRoot());
    }

    function update(source) {
        var nodes = updateNodes(source);
        updateLinks(source, nodes);
    }

    function updateNodes(source) {
        // Compute the new tree layout.
        h = getNumLeaves() * VERTICAL_NODE_SPACING;
        //console.debug("w & h = " + w + " & " + h);

        var treeWidth = w - 200; // space on the right for species labels
        tree.size([h, treeWidth]);

        var nodes = tree.nodes(staticData.getRoot())//.reverse();
        if (usePhylogramLayout) {
            branchLengthScale = scaleBranchLengths(nodes, treeWidth);
            // copied from d3.phylogram.js, where is it not accessible
            function scaleBranchLengths(nodes, w) {
                // Visit all nodes and adjust y pos width distance metric
                var visitPreOrder = function (root, callback) {
                    callback(root);
                    if (root.children) {
                        for (var i = root.children.length - 1; i >= 0; i--) {
                            visitPreOrder(root.children[i], callback)
                        }
                    }
                };
                visitPreOrder(nodes[0], function (node) {
                    node.rootDist = (node.parent ? node.parent.rootDist : 0) + (node.length || 0);
                });
                var rootDists = nodes.map(function (n) {
                    return n.rootDist;
                });
                var yscale = d3.scale.linear()
                    .domain([0, d3.max(rootDists)])
                    .range([0, w]);
                visitPreOrder(nodes[0], function (node) {
                    node.y = yscale(node.rootDist)
                });
                return yscale;
            }
        }

        // Update the nodes
        var node = vis.selectAll("g.node")
            .data(nodes, function (d) {
                return d.id;
            });

        // Enter any new nodes at the parent's previous position.
        // g groups together the circle and the text
        var nodeEnter = node.enter().append("g")
            .attr("class", "node")
            .attr("transform", function (d) {
                return "translate(" + d.y + "," + d.x + ")";
            })
            .style("opacity", 0)
            .on("click", function (d) {
                util.dispatchEvent("nodeClicked", {node:d});
            });

        nodeEnter.append("circle")
            .attr("r", getCircleSize)
            .attr("id", function (d) {
                return d.id;
            })
            .on("mouseover", function (d) {
                util.dispatchEvent("nodeMouseover", {node:d});
            })
            .on("mouseout", tooltips.removePBSInfo)
            .style("fill", colors.getNodeColor);

        nodeEnter.append("text")
            .attr("x", function (d) {
                return d.children || d._children ? -10 : 10;
            })
            .attr("text-anchor", function (d) {
                return d.children || d._children ? "end" : "start";
            })
            .attr("class", "nodeLabel")
            .text(function (d) {
                return d.name;
            })
            .style("font", "10px sans-serif")
            .style("fill-opacity", 1);

        // add branch length labels
        nodeEnter.append("text")
            .attr("class", "nodeAnnotationLabel")
            .attr("x", function (d) {
                return -70;
            })
            .attr("y", function (d) {
                return -1;
            })
            .attr("text-anchor", "start")
            .style("font", "10px sans-serif")
            .attr("fill", "#0000b4");

        // add species labels on the right
        nodeEnter
            .filter(function (d) {
                if (d.species != undefined) {
                    d.nodeType = "species";
                }
                return d.species != undefined;
            })
            .append("text")
            .attr("x", function (d) {
                return w - d.y;
            })
            .attr("y", function (d) {
                return 0;
            })
            .attr("class", "speciesLabel")
            .attr("id", function (d) {
                return d.id + "speciesLabel";
            })
            .style("font", "10px sans-serif");

        d3.selectAll(".speciesLabel")
            .text(function (d) {
                if (d.species != undefined) {
                    return d.species[taxonomyLevel];
                }
                else return "";
            })
            .attr("fill", getSpeciesColor);

        // update branch length labels
        d3.selectAll(".nodeAnnotationLabel")
            .text(function (d) {
                if (usePhylogramLayout && useBranchLengthForNodeLabel && !(d.length === undefined) && !d.length == 0) {
                    return util.twoDecimalPlaces(d.length);
                }
                else if (!usePhylogramLayout) {
                    var value = staticData.getPBS()[d.name];
                    if (value && value.nodeSupport) {
                        return value.nodeSupport;
                    }
                }
            });

        // Transition nodes to their new position.

        var nodeUpdate = node.transition()
            .duration(duration)
            .attr("transform", function (d) {
                d3.select("#" + d.id + "speciesLabel").attr("x", w - d.y); // move the species Label while moving node
                return "translate(" + d.y + "," + d.x + ")";
            })
            .transition().duration(duration * 2)
            .style("opacity", 1);

        nodeUpdate.select("circle")
            .attr("r", getCircleSize)
            .style("fill", colors.getNodeColor)
            .style("stroke", colors.circleLine);

        nodeUpdate.select("text")
            .style("fill-opacity", 1);

        var nodeExit = node.exit();
        // at this point, nodeExit is a hash of g.node objects. Each one contains a circle and text object
        nodeExit = nodeExit.transition()
            .duration(duration / 2)
            .style("opacity", 0)
            .remove();

        return nodes;
    }

    function getSpeciesColor(d) {
        if (d.species && d.species[taxonomyLevel]) {
            var v = colors.stringHexNumber(d.species[taxonomyLevel]);
            c = ('000000' + v).slice(-6);
            c = colors.checkLuminosity(c);
            console.log(d.species[taxonomyLevel] + ": " + c);
            return "#" + c;
        }
        return '#666666';
    }

    function getCircleSize(d) {
        if (d.children || d._children) {
            if (staticData.getPBS()[d.name]) {
                return sizeScale(staticData.getPBS()[d.name].avgPBSValue);
            }
            else return 2;
        }
        else {
            return 0;
        }
    }

    function updateLinks(source, nodes) {
        // Update the links�
        var link = vis.selectAll("path.link")
            .data(tree.links(nodes), function (d) {
                return d.target.id;
            });

        // Enter any new links at the parent's previous position.
        var linkEnter = link.enter();

        linkEnter.insert("path", "g")
            .attr("class", "link")
            .attr("d", diagonal)
            .style("opacity", 0);

        // Transition links to their new position.
        link.transition()
            .duration(duration)
            .attr("d", diagonal)
            .transition().duration(duration * 2)
            .style("opacity", 1);


        var links = vis.selectAll("path.link")
            .style("fill", "none")
            .style("stroke-width", "1.5px");

        // Transition exiting nodes to the parent's new position.
        link.exit().remove();

        // Stash the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

// debugging functions
    function showNodeStructure(node, hidden) {
        console.debug(node.name + (hidden ? "(hidden)" : "") + ", expanded = " + node.expanded);
        if (node.children) {
            for (var i = 0; i < node.children.length; i++) {
                showNodeStructure(node.children[i], false);
            }
        }
        if (node._children) {
            for (var i = 0; i < node._children.length; i++) {
                showNodeStructure(node._children[i], true);
            }
        }
    }

    return {
        createTree: createTree,
        resize: resize,
        updateRoot: updateRoot,
        getUsePhylogramLayout: getUsePhylogramLayout,
        setUsePhylogramLayout: setUsePhylogramLayout,
        setUseBranchLengthForNodeLabel: setUseBranchLengthForNodeLabel,
        setTaxonomyLevel: setTaxonomyLevel,
    };

}());