/**
 * This class handles collapsing of nodes, both manually and programatically.
 *
 * There are two parallel strategies - the first marks nodes as expanded or collapsed. This is
 * used for knowing which nodes to collapse. The second marks nodes with a "filterMode" value.
 * If this is "highlighted" then the color of the node will be different than the manually collapsed node's.
 * toggleManualCollapse is the only function for manual collapsing and is relatively straightforward.
 *
 * The other functions handle path highlighting and collapsing extraneous nodes.
 */
var collapse = (function () {
    var numExpanded = 0;

    /*
     Expanded is all nodes that are visible.
     Highlighted is just those nodes that are in the keep expanded list.

     in drawing the tree, lines use expanded, that's why they're ALL colored orange.
     circle color comes from "highlighted" - only a few are in orange.
     */

    /**
     * Function for manually collapsing a node, as opposed to collapsing by filter.
     * This won't affect any filtering/highlighting already in place.
     * @param node
     */
    function toggleManualCollapse(node) {
        try {
            if (node.children) {
                node._children = node.children;
                node.manuallyCollapsed = true;
                //  node.manuallyExpanded = false;
                node.children = null;
            } else {
                node.children = node._children;
                node.manuallyCollapsed = false;
                node._children = null;
            }
            expandProgramaticallyCollapsedNodes(staticData.getRoot());
            collapseTree();
        }
        catch (err) {
            if (node.manuallyCollapsed) {
                node.manuallyCollapsed = false;
            }
        }
    }

    function onFiltering() {
        removeHighlightingBehaviorPrivate();  // does not work if you remove this -- why?
        dynamicData.createKeepExpandedList();
        collapseTree();
    }

    function collapseTree() {
        // todo - check whether tree collapses down to one node
        removeAllMarks(staticData.getRoot());
        setMarks(staticData.getRoot());
        removeChildrenFromUnexpandedNodes(staticData.getRoot());

        if (dynamicData.getKeepExpandedList().length == 0) {
            console.debug("keepExpandedList is empty, expanding programatically collapsed nodes");
            expandProgramaticallyCollapsedNodes(staticData.getRoot());
        }
        if (!staticData.getRoot().children) {
            overlay.addAlert("No nodes match the current filters (the tree would collapse into a single node). Please try something else.");
            throw new Error("NoNodesMatch");
        }
        bigPlantTree.updateRoot();
    }

    function expandProgramaticallyCollapsedNodes(d) {
        if ((d._children && d.manuallyCollapsed != true)) {
            d.children = d._children;
            d._children = null;
        }
        if (d.children) {
            d.children.forEach(expandProgramaticallyCollapsedNodes);
        }
    }

    /**
     * Removes both marks and highlighting from nodes
     */
    function removeHighlightingBehaviorPrivate() {
        removeAllMarks(staticData.getRoot());
        expandProgramaticallyCollapsedNodes(staticData.getRoot());
    }

    function removeHighlightingBehavior() {
        removeHighlightingBehaviorPrivate();
        bigPlantTree.updateRoot();
    }

    /**
     * Recursive procedure to remove mark (usually from node and all its
     * children. Also sets expanded to false.
     *
     * @param node
     */
    function removeAllMarks(node) {
        node.expanded = false;
        node.filterMode = "none";
        if (node.children) {
            node.children.forEach(removeAllMarks);
        }
    }

    /** Sets expanded=true for node and its parents
     *
     * @param node
     */
    function setParentHierarchyToExpanded(node) {
        node.expanded = true;
        var parent = node.parent;
        while (parent) {
            parent.expanded = true;
            parent = parent.parent;
        }
    }

    /**
     * Recursive procedure to add mark "highlighted" to node and all its children
     *
     * @param node
     * @param filterMode
     */
    function setMarks(node) {
        if (dynamicData.inKeepExpandedList(node.name)) {
            if (dynamicData.doesNodeHaveOverrepresentation(node.name)) {
                node.filterMode = "overrepresented";
            }
            else {
                node.filterMode = "highlighted";
            }
            setParentHierarchyToExpanded(node);
        }
        // recursion below
        if (node.children) {
            var length = node.children.length;
            for (var i = 0; i < length; i++) {
                setMarks(node.children[i]);
            }
        }
    }

    /**
     * Recursive procedure that removes children from a node if that node is not in an
     * expanded state.
     *
     * @param node
     */
    function removeChildrenFromUnexpandedNodes(node) {
        //console.debug("removeChildrenFromUnexpandedNodes " + node.name);
        if (node.children) {
            if (node.expanded) {
                node.children.forEach(removeChildrenFromUnexpandedNodes);
            }
            else {
                //console.debug("collapsing " + node.name);
                node._children = node.children;
                node.children = null;
            }
        }
    }

    return {
        toggleCollapse:toggleManualCollapse,
        onFiltering:onFiltering,
        removeHighlightingBehavior:removeHighlightingBehavior, // this and collapseTree are public for pbs slider action
        collapseTree:collapseTree
    };

}());