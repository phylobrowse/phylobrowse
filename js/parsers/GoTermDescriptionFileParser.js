/**
 * This parser reverses the order, creating a "GO description to GO id" map out of a "GO id to GO description" map
 */

goTermDescriptionFileParser = function () {

    var goNameToIdMap = {};
    var goIdToNameMap = {};


    function parseFile(data) {
        var tokens = data.split("\n");
        var length = tokens.length;
        console.log("goTermDescriptionFileParser: parsing " + length + " terms.");
        for (var i = 0; i < length; i++) {
            var token = tokens[i];
            parseToken(token);
        }
    }

    function parseToken(token) {
        var tokens = token.split(" | ");
        if (tokens[1]) {
            goNameToIdMap[tokens[1]] = tokens[0];
            goIdToNameMap[tokens[0]] = tokens[1];
        }
    }

    return {
        goIdToNameMap:goIdToNameMap,
        goNameToIdMap:goNameToIdMap,
        parseFile:parseFile
    };
}();