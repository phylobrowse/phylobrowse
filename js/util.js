var util = function () {

    var urlVars = null;

    function getUrlVars() {
        if (urlVars == null) {
            urlVars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                urlVars[key] = value;
            });
        }
        return urlVars;
    }

    function sortListAlphabetically(list) {
        list.sort(alphabeticalSort);
    }

    function alphabeticalSort(a, b) {
        if (a.toLowerCase() > b.toLowerCase()) {
            return 1;
        }
        else {
            return -1;
        }
    }

    function centerElement(elementId) {
        var windowHeight = $(window).height();
        var windowWidth = $(window).width();
        var boxHeight = $(elementId).height();
        var boxWidth = $(elementId).width();
        $(elementId).css({'left': ((windowWidth - boxWidth) / 2), 'top': ((windowHeight - boxHeight) / 2)});
    }

    function centerElementHorizontally(elementId) {
        var windowWidth = $(window).width();
        var boxWidth = $(elementId).width();
        $(elementId).css({'left': ((windowWidth - boxWidth) / 2)});
    }

    function twoDecimalPlaces(num) {
        num = num * 100;
        num = Math.round(num);
        return num / 100;
    }

    /* for debugging */
    function describeDataStructure(j, object) {
        if (!j) {
            j = {};
        }
        j.fields = Object.getOwnPropertyNames(object);
        _.each(j.fields, function (field) {
            if (!_.isFunction(object[field])) {
                console.log(field + ":");
                console.log(object[field]);
            }
        });
        return j;
    }

    function dispatchEvent(event, data) {
        var evt = new Event(event);
        evt.initEvent(event, true, true);
        evt.data = data;
        document.dispatchEvent(evt);
    }

    return {
        centerElement: centerElement,
        getUrlVars: getUrlVars,
        sortListAlphabetically: sortListAlphabetically,
        alphabeticalSort: alphabeticalSort,
        twoDecimalPlaces: twoDecimalPlaces,
        describeDataStructure: describeDataStructure,
        dispatchEvent: dispatchEvent,
        centerElementHorizontally:centerElementHorizontally
    }

}();