/* various color-related functions */

var colors = function () {
    var collapsedCircle = "#b0c4de";
    var filterMode = "#ff8456";
    var hasOvrTermData = "#ceff59";
    var circleLine = "#ccc";
    var manuallyCollapsed = "#000000";
    var uidSelected = "#80d9ff";
    var overrepresented = "#d70cde";

    /* function for optionally setting colors */
    function setColors(obj) {
        if (obj.collapsedCircle){
            collapsedCircle = obj.collapsedCircle;
        }
        if (obj.filterMode){
            filterMode = obj.filterMode;
        }
        if (obj.hasOvrTermData){
            hasOvrTermData = obj.hasOvrTermData;
        }
        if (obj.circleLine){
            circleLine = obj.circleLine;
        }
        if (obj.manuallyCollapsed){
            manuallyCollapsed = obj.manuallyCollapsed;
        }
        if (obj.uidSelected){
            uidSelected = obj.uidSelected;
        }
        if (obj.overrepresented){
            overrepresented = obj.overrepresented;
        }
    }


    /* Converts a string to a hex number for color.
    from https://stackoverflow.com/questions/17845584/converting-a-random-string-into-a-hex-colour
    */
    function stringHexNumber(str) {
        var v = parseInt(str, 36).toExponential().slice(2, -5)
        v = parseInt(v, 10) & 0xFFFFFF
        return v.toString(16).toUpperCase();
    }

    /* removes the '#' sign in front of string if it exists */
    function removePound(h) {
        return (h.charAt(0) == "#") ? h.substring(1, 7) : h
    }

    /* gets red component of a hex string */
    function hexToR(h) {
        return parseInt((removePound(h)).substring(0, 2), 16)
    }

    /* gets green component of a hex string */
    function hexToG(h) {
        return parseInt((removePound(h)).substring(2, 4), 16)
    }

    /* gets blue component of a hex string */
    function hexToB(h) {
        return parseInt((removePound(h)).substring(4, 6), 16)
    }

    /* gets luminosity of a hex string, for example #f2d132 */
    function getLuminosity(color) {
        R = hexToR(color);
        G = hexToG(color);
        B = hexToB(color);
        return (0.299 * R + 0.587 * G + 0.114 * B)
    }

    /* Checks luminosity of a hex string, for example #f2d132 */
    function checkLuminosity(color) {
        if (getLuminosity(color) > 180) {
            c = parseInt(color, 16);
            return (c & 0xcccccc).toString(16).toUpperCase();
        }
        else return color;
    }

    function getNodeColor(d) {
        if (d.uidSelected) {
            return uidSelected;
        }
        if (d.manuallyCollapsed) {
            return manuallyCollapsed;
        }
        else if (d._children) {
            return collapsedCircle;
        }
        if (d.filterMode == "highlighted") {
            return filterMode;
        }
        if (d.filterMode == "overrepresented") {
            return overrepresented;
        }
        else {
            return hasOvrTermData;
        }
    }

    return {
        hasOvrTermData:hasOvrTermData,
        filterMode:filterMode,
        hasOvrTermData:hasOvrTermData,
        circleLine:circleLine,
        stringHexNumber:stringHexNumber,
        hexToR:hexToR,
        hexToG:hexToG,
        hexToB:hexToR,
        checkLuminosity:checkLuminosity,
        getNodeColor:getNodeColor
    }
}();