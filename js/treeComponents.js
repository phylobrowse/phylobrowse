/**
 * Handles all non-viz GUI (filters, sliders, etc)
 * @type {{createAllComponents, goTermClicked, getSelectedGenes, getSelectedGoTerms,
 * setSelectedGenes, setSelectedGoTerms, geneClicked, updateInfoPanel, clearGeneFilter,
 * clearGOtermFilter, loadState, setPValueSliderLowValue, setPValueSliderHighValue,
 * setAbsPBSSliderLowValue, setAbsPBSSliderHighValue}}
 */

var treeComponents = (function () {

    var loPValue = 0;
    var hiPValue = 0;
    var goTermFilterString = "";
    var geneListFilterString = "";

    function createAllComponents(width) {
        loPValue = staticData.getLowestPValue();
        hiPValue = staticData.getHighestPValue();
        createGoTermList();
        bigPlantTree.createTree(width);
        createGeneList();
        addAbsPBSSlider();
        addPValueSlider();
        fillTaxonomySelect();
        setFilterByRangeTitle();
    }

    /**
     * obsolete
     * This checks the GO term list for a node and gets the -log(PVAL) average for all the terms.
     * @param nodeName

     function getPValue(nodeName) {
        var overTerms = staticData.getPBS()[nodeName].ovrTerm;
        if (overTerms) {
            var totalPValues = 0;
            var counter = 0;
            for (var i = 0; i < overTerms.length; i++) { // for each ov
                if (-1 < $.inArray(overTerms[i].id, dynamicData.selectedGOterms()   )) {
                    totalPValues += Math.log(1 / overTerms[i].pValue);
                    counter++;
                }
            }
            return totalPValues / counter;
        }
        else {
            return 0;
        }
    }*/

    function setFilterByRangeTitle() {
        var template = $(app.getTemplates()).filter('#createFilterByRangeTitle').html();
        var html = Mustache.to_html(template, {measureType:controller.getMeasureType()});
        $("#filterByRangeTitle").html(html);
    }

    function createGoTermList() {
        var template = $(app.getTemplates()).filter('#createGoTermList').html();
        var goTerms = [];
        _.each(staticData.getGoTermNamesList(), function (desc) {
            var obj = {};
            obj.goTermName = desc;
            obj.goTermId = staticData.getGOtermIdByName(obj.goTermName);
            obj.avgPValue = staticData.getAvgPValue(obj.goTermId);
            avgPValue = parseFloat(obj.avgPValue);
            //if (avgPValue >= loPValue && avgPValue <= hiPValue) {
            //if (checkAvgPValue(obj.avgPValue)) {
            obj.goTermNameShort = obj.goTermName;
            if (obj.goTermName.length > 67) {
                obj.goTermNameShort = obj.goTermName.substring(0, 67) + "...";
            }
            if (obj.avgPValue) { //todo -change to use a class
                obj.backgroundColor = "#f0ffd5";// color from colors?
            }
            else {
                obj.backgroundColor = "#ffffff";
            }
            goTerms.push(obj);
            //}
        });
        console.debug("creating GO terms list with " + goTerms.length + " terms.");
        goTermsObj = {};
        goTermsObj.goTerms = goTerms;
        goTermsObj.termType = controller.getTermType();
        var html = Mustache.to_html(template, goTermsObj);
        $("#controls").prepend(html);
    }

    function setGoTermFilterString(searchTerm) {
        goTermFilterString = searchTerm;
        filterGoTerms();
    }

    function setGeneListFilterString(searchTerm) {
        geneListFilterString = searchTerm;
        filterGeneList();
    }

    function filterGoTerms() {
        $("option.goTermSelectListOption").show();
        if (goTermFilterString) {
            $('option.goTermSelectListOption').not("option.goTermSelectListOption[title*='" + goTermFilterString + "']").hide();
            //$("option.goTermSelectListOption[title*='" + goTermFilterString + "']").show();
        }
        // if pValue slider is in use...
        if (!(loPValue == staticData.getLowestPValue() && hiPValue == staticData.getHighestPValue())) {
            $("option.goTermSelectListOption").filter(function (i, e) {
                var pvalue = e.attributes.getNamedItem("avgpvalue").value;
                if (isNaN(parseInt(pvalue))) {
                    return true;
                }
                else {
                    return !(pvalue >= loPValue && pvalue <= hiPValue);
                }
            }).hide();
        }
    }

    function filterGeneList() {
        if (geneListFilterString) {
            $('option.geneSelectListOption').not("option.geneSelectListOption[gene*='" + geneListFilterString + "']").hide();
            $("option.geneSelectListOption[gene*='" + geneListFilterString + "']").show();
        }
        else {
            $("option.geneSelectListOption").show();
        }
    }

    function fillTaxonomySelect() {
        var levels = staticData.getTaxonomyLevels();
        _.each(levels, function (item) {
            // TODO - REMOVE HTML
            $("#taxonomySelect").append("<option value='" + item + "'>" + item + "</option>");
        });
    }

    function createGeneList() {
        var genesArray = [];
        for (var i = 0; i < staticData.getGeneList().length; i++) {
            var obj = {};
            obj.key = staticData.getGeneList()[i];
            genesArray.push(obj)
        }
        var template = $(app.getTemplates()).filter('#createGeneList').html();
        var genesObj = {};
        genesObj.genes = genesArray;
        $("#geneSelectContainer").append(Mustache.to_html(template, genesObj));
    }

    function setSelectedGoTerms(arrayOfGoterms) {
        var unknownGoTerms = "";
        for (var i = 0; i < arrayOfGoterms.length; i++) {
            var goTerm = $(".goTermSelectListOption[goTerm=\"" + arrayOfGoterms[i] + "\"]");
            if (goTerm.length == 0) {
                unknownGoTerms += arrayOfGoterms[i] + "\n";
            }
            else {
                goTerm.attr("selected", true);
            }
        }
        dynamicData.addSelectedGoTerms(arrayOfGoterms);
        if (unknownGoTerms.length > 0) {
            overlay.addAlert("No matches for: \n" + unknownGoTerms);
        }
        goTermClicked();
    }

    /**
     * sets Genes uploaded from txt file in both gene list and tree
     * @param arrayOfGenes
     */
    function setSelectedGenes(arrayOfGenes) {
        var unknownGenes = "";
        for (var i = 0; i < arrayOfGenes.length; i++) {
            var gene = $(".geneSelectListOption[gene=\"" + arrayOfGenes[i] + "\"]");
            if (gene.length == 0) {
                unknownGenes += arrayOfGenes[i] + "\n";
            }
            else {
                gene.attr("selected", true);
            }
        }
        dynamicData.addSelectedGenes(arrayOfGenes);
        collapse.onFiltering();
        if (unknownGenes.length > 0) {
            overlay.addAlert("No matches for: \n" + unknownGenes);
        }
        geneClicked();
    }


    function setSliderLowValue(value, sliderName, functionToCall) {
        var loHiArray = $("#" + sliderName).slider("values");
        if (loHiArray[1] <= value) {
            overlay.addAlert("Low value cannot be higher than the current high value.");
        }
        else if (value < $("#" + sliderName).slider("option", "min")) {
            overlay.addAlert(value + " is less than the minimum allowed: " + $("#" + sliderName).slider("option", "min"));
        }
        else {
            //console.debug("min: " + $("#" + sliderName).slider("option", "min"));
            functionToCall(value, 0);
        }
    }

    function setSliderHighValue(value, sliderName, functionToCall) {
        var loHiArray = $("#" + sliderName).slider("values");
        if (loHiArray[0] >= value) {
            overlay.addAlert("High value cannot be lower than the current low value.");
        }
        else if (value > $("#" + sliderName).slider("option", "max")) {
            overlay.addAlert(value + " is greater than the maximum allowed: " + $("#" + sliderName).slider("option", "max"));
        }
        else {
            functionToCall(value, 1);
        }
    }

    {
        //////////// PVALUE SLIDER
        function setPValueSliderLowValue(value) {
            setSliderLowValue(value, "pValueSliderDiv", setPValueSliderValue);
        }

        function setPValueSliderHighValue(value) {
            setSliderHighValue(value, "pValueSliderDiv", setPValueSliderValue);
        }

        function setPValueSliderValue(value, index) {
            var jSlider = $("#pValueSliderDiv");
            jSlider.slider("values", index, value);
            var loHiArray = $("#pValueSliderDiv").slider("values");
            onPValueChanged(loHiArray);
        }

        function onPValueChanged(loHiArray) {
            loPValue = loHiArray[0];
            hiPValue = loHiArray[1];
            filterGoTerms();
        }

        function addPValueSlider() {
            var h = staticData.getHighestPValue();
            var l = staticData.getLowestPValue();
            console.debug("PValue Slider values: " + l + " and " + h);
            $("#loPvalue").val(l);
            $("#hiPvalue").val(h);

            $("#pValueSliderDiv").slider({
                min: l,
                max: h,
                animate: true,
                step: .0001,
                range: true,
                values: [l, h],
                slide: function (event, ui) {
                    var loHiArray = $("#pValueSliderDiv").slider("values");
                    $("#loPvalue").val(loHiArray[0]);
                    $("#hiPvalue").val(loHiArray[1]);
                },
                stop: function (event, ui) {
                    var loHiArray = $("#pValueSliderDiv").slider("values");
                    onPValueChanged(loHiArray);
                    $("#loPvalue").val(loHiArray[0]);
                    $("#hiPvalue").val(loHiArray[1]);
                }
            });
        }
    }

    {
        ////////// MINIMUM ABSOLUTE PBS SLIDER

        function setAbsPBSSliderValue(value, index) {
            var jSlider = $("#absPBSSliderDiv");
            jSlider.slider("values", index, value);
            var loHiArray = $("#absPBSSliderDiv").slider("values");
            onAbsPBSSliderChanged(loHiArray);
        }

        function onAbsPBSSliderChanged(loHiArray) {
            collapse.removeHighlightingBehavior();
            dynamicData.setlowHighArray_AbsPBS(loHiArray);
            dynamicData.createKeepExpandedList();
            collapse.collapseTree();
        }

        function setAbsPBSSliderLowValue(value) {
            setSliderLowValue(value, "absPBSSliderDiv", setAbsPBSSliderValue);
        }

        function setAbsPBSSliderHighValue(value) {
            setSliderHighValue(value, "absPBSSliderDiv", setAbsPBSSliderValue);
        }

        function addAbsPBSSlider() {
            var h = staticData.getHighestAbsPBSValue();
            var l = staticData.getLowestAbsPBSValue();
            console.debug("PBS Slider values: " + l + " and " + h);
            $("#loAbsPBSvalue").val(l);
            $("#hiAbsPBSvalue").val(h);

            dynamicData.setlowHighArray_AbsPBS([l, h]);

            $("#absPBSSliderDiv").slider({
                min: l,
                max: h,
                animate: true,
                step: 1,
                range: true,
                values: [l, h],
                slide: function (event, ui) {
                    var loHiArray = $("#absPBSSliderDiv").slider("values");
                    $("#loAbsPBSvalue").val(loHiArray[0]);
                    $("#hiAbsPBSvalue").val(loHiArray[1]);
                },
                stop: function (event, ui) {
                    var loHiArray = $("#absPBSSliderDiv").slider("values");
                    onAbsPBSSliderChanged(loHiArray);
                    $("#loAbsPBSvalue").val(loHiArray[0]);
                    $("#hiAbsPBSvalue").val(loHiArray[1]);
                }
            });
        }
    }

    function updateInfoPanel() {
        $("#infoPanelContainer").empty();
        var obj = {selectedGenes: dynamicData.selectedGenes()}

        obj.selectedGoTerms = _.map(dynamicData.selectedGOterms(), function (item) {
            return staticData.getGoTermName(item);
        });
        if (obj.selectedGenes.length > 0) {
            obj.hasGenes = true;
        }
        if (obj.selectedGoTerms.length > 0) {
            obj.hasGoTerms = true;
        }

        var template = $(app.getTemplates()).filter('#createInfoPanel').html();
        var html = Mustache.to_html(template, obj);
        $("#infoPanelContainer").append(html);
        $('.controlsDivInsert').width($('.controlsDivInsert').parent().parent().width()-30)
    }

    function loadState(goTerms, genes, pbs) {
        clearGeneFilter();
        clearGOtermFilter();
        //$("#pbsSliderDiv").slider("values") = pbs;
        setSelectedGenes(genes);
        setSelectedGoTerms(goTerms);
    }

    function clearGOtermFilter() {
        $("#goTermSelect").val([]);
        $("#heatMapButton").attr('disabled', 'disabled');
        goTermFilterString = "";
        $("#goTermsFilterInput").val("");
        $("#loPvalue").val(staticData.getLowestPValue());
        $("#hiPvalue").val(staticData.getHighestPValue());
        setPValueSliderLowValue(staticData.getLowestPValue());
        setPValueSliderHighValue(staticData.getHighestPValue());
        goTermClicked();
        $("option.goTermSelectListOption").show();
    }

    function goTermClicked() {
        //check heatmap button
        var keysList = $(".goTermSelectListOption:selected");
        if (keysList.length > 0) {
            $("#heatMapButton").removeAttr('disabled');
        }
        else {
            $("#heatMapButton").attr('disabled', 'disabled');
        }
        collapse.onFiltering();
        updateInfoPanel();
    }

    function geneClicked() {
        collapse.onFiltering();
        updateInfoPanel();
    }

    function clearGeneFilter() {
        $("#geneSelect").val([]);
        $("#geneListFilterInput").val("");
        geneListFilterString = "";
        filterGeneList();
        dynamicData.filterByPBSValue();
        geneClicked();
    }

    function getSelectedGenes(geneKeysList) {
        var geneFilter = $(".geneSelectListOption:selected");
        var selectedGenes = [];
        _.each(geneFilter, function (option) {
            selectedGenes.push(option.attributes.gene.value);
        });
        return selectedGenes;
    }

    function getSelectedGoTerms() {
        var goTermFilter = $(".goTermSelectListOption:selected");
        var selectedGOterms = [];
        _.each(goTermFilter, function (option) {
            selectedGOterms.push(option.attributes.goTerm.value);
        });
        return selectedGOterms;
    }

    return {
        createAllComponents: createAllComponents,
        goTermClicked: goTermClicked,
        getSelectedGenes: getSelectedGenes,
        getSelectedGoTerms: getSelectedGoTerms,
        setSelectedGenes: setSelectedGenes,
        setSelectedGoTerms: setSelectedGoTerms,
        geneClicked: geneClicked,
        updateInfoPanel: updateInfoPanel,
        clearGeneFilter: clearGeneFilter,
        clearGOtermFilter: clearGOtermFilter,
        loadState: loadState,
        setPValueSliderLowValue: setPValueSliderLowValue,
        setPValueSliderHighValue: setPValueSliderHighValue,
        setAbsPBSSliderLowValue: setAbsPBSSliderLowValue,
        setAbsPBSSliderHighValue: setAbsPBSSliderHighValue,
        setGoTermFilterString: setGoTermFilterString,
        setGeneListFilterString: setGeneListFilterString
    };

}());