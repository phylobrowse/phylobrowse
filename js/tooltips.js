var tooltips = function () {

    var keepUp = true;
    var lastX; // wierd hack because d3.event is null with latest version
    var lastY;

// function for debugging collapsing behavior
    function getNodeInfo(d) {
        var s = "<br/>&middot; manuallyCollapsed: " + d.manuallyCollapsed;
        s += "<br/>&middot; filterMode: " + d.filterMode;
        s += "<br/>&middot; expanded: " + d.expanded;
        return s;
    }

    function removePBSInfo() {
        if (!keepUp) {
            closePBSInfo();
        }
    }

    function closePBSInfo() {
        d3.select("div.pbsPane").remove();
    }

    /*
     keepInView == do not hide tooltip on mouseout
     */
    function showPBSInfo(d, keepInView) {
        keepUp = keepInView;
        if (!d.name) {
            // unnamed node - do not show tooltip
            return;
        }
        else if (hasInformation(d.name)) {
            //console.debug("ovrTerm: " + pbs[d.name].ovrTerm.length);
            addPBSInfo(d);
        }
        else {
            showPBSDialog("<br/>No data for " + d.name, d);
        }

        function addPBSInfo(d) {
            var obj = createObjectForTemplate(d.name);
            var template = $(app.getTemplates()).filter('#createNodeTooltip').html();
            var dialogText = Mustache.to_html(template, obj);
            showPBSDialog(dialogText, obj);
        }

        function showPBSDialog(dialogText, obj) {
            d3.select("div.pbsPane").remove();
            if (d3.event) {
                lastX = d3.event.pageX;
                lastY = d3.event.pageY;
            }
            d3.select("body")
                .append("div")
                .attr("class", "pbsPane")
                .attr("id", "pbsPane")
                .style("top", (lastY - 50) + "px")
                .style("left", (lastX + 10) + "px")
                .html(dialogText);
            if (keepUp) {
                addSaveButtonForTerms(obj);
                addSaveButtonForGenes(obj);
            }

            function addSaveButtonForGenes(obj) {
                createSaveButton("saveTooltipGenes",
                    function () {
                        return obj.name + "_Genes.txt";
                    }, function () {
                        var template = $(app.getTemplates()).filter('#createNodeTooltipSaveGenes').html();
                        return Mustache.to_html(template, obj);
                    });
            }

            function addSaveButtonForTerms(obj) {
                createSaveButton("saveTooltipTerms",
                    function () {
                        return obj.name + "_Terms.txt";
                    }, function () {
                        var template = $(app.getTemplates()).filter('#createNodeTooltipSaveTerms').html();
                        return Mustache.to_html(template, obj);
                    });
            }

            function createSaveButton(elementName, filenameFunction, dataFunction) {
                Downloadify.create(elementName, {
                    filename: filenameFunction,
                    data: dataFunction,
                    onComplete: function () {
                        overlay.addAlert('Saved!');
                    },
                    onCancel: function () {
                    },
                    onError: function () {
                        overlay.addAlert('You must put something in the File Contents or there will be nothing to save!');
                    },
                    swf: 'lib/downloadify.swf',
                    downloadImage: 'images/save_icon.png',
                    width: 20,
                    height: 20,
                    transparent: true,
                    append: false
                });
            }

            /*function addSaveButtonX(obj) {
                Downloadify.create('saveTooltipInfo', {
                    filename: function () {
                        return obj.name + ".txt";
                    },
                    data: function () {
                        var template = $(app.getTemplates()).filter('#createNodeTooltipSaveInfo').html();
                        return Mustache.to_html(template, obj);
                    },
                    onComplete: function () {
                        overlay.addAlert('Saved!');
                    },
                    onCancel: function () {
                    },
                    onError: function () {
                        overlay.addAlert('You must put something in the File Contents or there will be nothing to save!');
                    },
                    swf: 'lib/downloadify.swf',
                    downloadImage: 'images/save_icon.png',
                    width: 20,
                    height: 20,
                    transparent: true,
                    append: false
                });
            }*/
        }

        function createObjectForTemplate(nodeName) {
            var node = staticData.getPBS()[nodeName];

            var obj = {};
            obj.name = nodeName;
            obj.measureType = controller.getMeasureType();
            obj.avgPBSValue = node.avgPBSValue;
            obj.absPBSValue = node.maxAbsolutePBSValue;
            if (!node.genePartitionSorted) {
                node.genePartition.sort(function (a, b) {
                    a = parseInt(a.value);
                    b = parseInt(b.value);
                    if (a > b) {
                        return -1;
                    }
                    else {
                        return 1;
                    }
                })
                node.genePartitionSorted = true;
            }
            obj.goTermlength = node.ovrTerm ? node.ovrTerm.length : 0;
            obj.hasGoTerms = obj.goTermlength > 0 ? true : false;
            obj.keepUp = keepUp;
            obj.ovrTerm = node.ovrTerm;
            obj.geneLength = node.genePartition ? node.genePartition.length : 0;
            obj.genePartition = node.genePartition;
            return obj;
        }

        function hasInformation(name) {
            if (staticData.getPBS()[name]) {
                if (staticData.getPBS()[name].genePartition || staticData.getPBS()[name].ovrTerm) {
                    return true;
                }
            }
            return false;
        }
    }

    function showClickOptions(d) {
        //console.log("options x&y: " + d3.event.pageX + ", " + d3.event.pageY);
        var obj = {}
        obj.data = JSON.stringify({name: d.name, keepUp: true, x: d3.event.pageX, y: d3.event.pageY});
        obj.name = d.name
        var template = $(app.getTemplates()).filter('#createShowClickOptions').html();
        var html = Mustache.to_html(template, obj);
        d3.select("div.pbsPane").remove();
        d3.select("body")
            .append("div")
            .attr("class", "pbsPane")
            .attr("id", "pbsPane")
            .style("top", (d3.event.pageY - 50) + "px")
            .style("left", (d3.event.pageX + 10) + "px")
            .html(html);
        lastX = d3.event.pageX;
        lastY = d3.event.pageY;
        keepUp = true;
    }

    return {
        removePBSInfo: removePBSInfo,
        showPBSInfo: showPBSInfo,
        closePBSInfo: closePBSInfo,
        showClickOptions: showClickOptions
    };

}
();