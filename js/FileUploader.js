/**
 Loading works on:
 Google Chrome 6 and later
 Firefox 3.6 and later
 Safari 6 (Safari 6 will support the standard File API)
 Internet Explorer 10 (Preview 2+) � Microsoft added File API support!
 */

var fileUpLoader = function () {
    var fileString;

    function startRead(fileElementName) {
        // obtain input element through DOM

        var file = document.getElementById(fileElementName).files[0];
        if (file) {
            getAsText(file);
        }
    }

    function getAsText(readFile) {
        var reader;
        try {
            reader = new FileReader();
        } catch (e) {
            overlay.addAlert("Error: seems File API is not supported on your browser");
            return;
        }

        // Read file into memory as UTF-8
        reader.readAsText(readFile, "UTF-8");

        // Handle progress, success, and errors
        reader.onload = loaded;
        reader.onerror = errorHandler;
    }


    function loaded(event) {
        var evt = document.createEvent("Event");
        evt.initEvent('fileUploaded', true, true);
        evt.data = event.target.result;
        document.dispatchEvent(evt);
    }


    function errorHandler(evt) {
        if (evt.target.error.code == evt.target.error.NOT_READABLE_ERR) {
            // The file could not be read
            overlay.addAlert("Error reading file...");
        }
    }


    return {
        startRead:startRead
    };
}();