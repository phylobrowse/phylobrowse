/**
 * Stores and manages any data that may change during use
 */
var dynamicData = (function () {

    var selectedGenes = [];
    var selectedGOterms = [];
    var keepExpandedList = []; // list of node ids to expand
    var lowHighArray_AbsPBS;
    var pairwisePatristicNode1;
    var pairwisePatristicNode2;

    /** Populates keepExpandedList array with node ids from the gene and GO terms arrays
     */
    // Note: used in treeComponents, collapsing and here
    function createKeepExpandedList() {
        keepExpandedList = [];
        console.log("selectedGenes: " + selectedGenes);

        selectedGenes = treeComponents.getSelectedGenes();
        selectedGOterms = treeComponents.getSelectedGoTerms();
        console.log("selectedGenes.length: " + selectedGenes.length);
        console.log("selectedGOterms.length: " + selectedGOterms.length);
        if (selectedGenes.length + selectedGOterms.length == 0) {
            //console.log("filtering by pbs value");
            filterByPBSValue();
        }
        else {
            //console.log("not filtering from pbs value");
            addGeneNodesToKeepExpandedList();
            addGoTermNodesToKeepExpandedList();
        }
    }

    /** Creates array of nodes that relate to the selected genes */
    function addGeneNodesToKeepExpandedList() {
        //console.log("BEG: addGeneNodesToKeepExpandedList, len: " + keepExpandedList.length);
        _.each(selectedGenes, function (gene) {
            var nodesArray = staticData.getNodesForGene(gene);
            _.each(nodesArray, function (node) {
                if (isWithinPBSRange(node)) {
                    // console.log("gene " + gene + " pushing node " + node.key);
                    keepExpandedList.push(node);
                }
            })
        })
        console.log("END: addGeneNodesToKeepExpandedList, len: " + keepExpandedList.length);
    }

    /** Creates array of nodes that relate to the selected GO terms */
    function addGoTermNodesToKeepExpandedList() {
        //console.log("BEG: addGoTermNodesToKeepExpandedList, len: " + keepExpandedList.length);
        _.each(selectedGOterms, function (goTermId) {
            var nodesList = staticData.getNodesForGoTerm(goTermId);
            console.log("got nodes for goTerm " + goTermId + " " + nodesList);
            if (nodesList) {
                _.each(nodesList, function (node) {
                    var isInRange = isWithinPBSRange(node);
                    if (!(-1 != keepExpandedList.indexOf(node)) && isInRange) {
                        keepExpandedList.push(node);
                    }
                    else {
                        if (!isInRange) {
                            console.log(node + " not in PBS range");
                        }
                    }
                });
            }
        });
        console.log("END: addGoTermNodesToKeepExpandedList, len: " + keepExpandedList.length);
    }

    /**
     * Checks whether the average pbs value of the node with this name is within the bounds
     * of the low/high pbs values.
     * @param nodeName
     */
    function isWithinPBSRange_old(nodeName) {
        var node = staticData.getPBS()[nodeName];
        if (node.maxAbsolutePBSValue) {
            if (node.maxAbsolutePBSValue >= lowHighArray_AbsPBS[0] && node.maxAbsolutePBSValue <= lowHighArray_AbsPBS[1]) {
                return true;
            }
        }
        else {
            return true;
        }
        return false;
    }

    /**
     * Checks whether the gene values for each node with this name is within the bounds
     * of the low/high pbs values.
     * @param nodeName
     */
    function isWithinPBSRange(nodeName) {
        var node = staticData.getPBS()[nodeName];
        for (var i = 0; i < node.genePartition.length; i++) {
            gene = node.genePartition[i];
            var val = Math.abs(gene.value);
            if (val >= lowHighArray_AbsPBS[0] && val <= lowHighArray_AbsPBS[1]) {
                //console.log(nodeName + ": " + gene.value + " is in between " + lowHighArray_AbsPBS[0] + " and " + lowHighArray_AbsPBS[1]);
                return true;
            }
        }
        return false;
    }

    /**
     * checks if nodeID is in keepExpandedList
     * @param nodeID
     */
    function inKeepExpandedList(nodeID) {
        return -1 != keepExpandedList.indexOf(nodeID);
    }

    /** Adds genes in array to selectedGenes array
     * @param arrayOfGenes
     * */
    function addSelectedGenes(arrayOfGenes) {
        for (var i = 0; i < arrayOfGenes.length; i++) {
            selectedGenes.push(arrayOfGenes[i]);
        }
    }

    /** Adds GO terms in array to selectedGenes array */
    function addSelectedGoTerms(arrayOfGoTerms) {
        for (var i = 0; i < arrayOfGoTerms.length; i++) {
            selectedGOterms.push(arrayOfGoTerms[i]);
        }
    }

    /** replaces items in keepExpandedList with node ids that are within the PBS values specified */
    function filterByPBSValue() {
        keepExpandedList.length = 0;
        /*if ((lowHighArray_AvgPBS[0] == staticData.getLowestAvgPBSValue() && lowHighArray_AvgPBS[1] == staticData.getHighestAvgPBSValue())
         && (lowHighArray_AbsPBS[0] == staticData.getLowestAbsPBSValue() && lowHighArray_AbsPBS[1] == staticData.getHighestAbsPBSValue())) {
         return; // neither slider is affected
         }*/
        if (lowHighArray_AbsPBS[0] == staticData.getLowestAbsPBSValue() && lowHighArray_AbsPBS[1] == staticData.getHighestAbsPBSValue()) {
            return; // slider is unchanged
        }
        else {
            for (var nodeName in staticData.getPBS()) {
                if (isWithinPBSRange(nodeName)) {
                    keepExpandedList.push(nodeName);
                }
            }
        }
    }

    function debugCurrentGOTermRelationships() {
        var s = "";
        if (selectedGOterms.length > 0) {
            for (var i = 0; i < selectedGOterms.length; i++) {
                var term = selectedGOterms[i];
                s += ("<b>Selected term: " + term + "</b><br/>");
                var goObj = staticData.getNodesForGoTerm(term);
                for (var j = 0; j < goObj.length; j++) {
                    var nodeName = goObj[j];
                    //s += ("&nbsp;&nbsp;&nbsp;&nbsp;<i>" + nodeName + "</i><br/>");
                    var nodeData = staticData.getPBS()[goObj[j]];
                    if (nodeData.ovrTerm) {
                        for (var k = 0; k < nodeData.ovrTerm.length; k++) {
                            var ovrTerm = nodeData.ovrTerm[k];
                            if (ovrTerm.id == term) {
                                s += ("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ovrTerm.id + " " + ovrTerm.desc + " is overrepresented in " + nodeName + "<br/>");
                            }
                        }
                    }
                }
            }
        }
        else {
            s = "You must select some terms in order to debug them."
        }
        var w = window.open();
        w.document.write("<html>" + s + "</html>");
    }

    function doesNodeHaveOverrepresentation(nodeName) {
        var nodeData = staticData.getPBS()[nodeName];
        if (nodeData.ovrTerm) {
            for (var i = 0; i < nodeData.ovrTerm.length; i++) {
                var ovrTerm = nodeData.ovrTerm[i];
                if (-1 != selectedGOterms.indexOf(ovrTerm.id)) {
                    return true;
                }
            }
        }
        return false;
    }

    function getPairwisePatristicNode1() {
        return pairwisePatristicNode1;
    }

    function setPairwisePatristicNode1(nodeName) {
        pairwisePatristicNode1 = nodeName;
    }

    function getPairwisePatristicNode2() {
        return pairwisePatristicNode2;
    }

    function setPairwisePatristicNode2(nodeName) {
        pairwisePatristicNode2 = nodeName;
    }

    return {
        selectedGOterms: function () {
            return selectedGOterms;
        },
        selectedGenes: function () {
            return selectedGenes;
        },
        createKeepExpandedList: createKeepExpandedList,
        addSelectedGenes: addSelectedGenes,
        filterByPBSValue: filterByPBSValue,
        setlowHighArray_AbsPBS: function (lha) {
            lowHighArray_AbsPBS = lha;
        },
        getlowHighArray_AbsPBS: function () {
            return lowHighArray_AbsPBS;
        },
        addSelectedGoTerms: addSelectedGoTerms,
        getKeepExpandedList: function () {
            return keepExpandedList;
        },
        inKeepExpandedList: inKeepExpandedList,
        debugCurrentGOTermRelationships: debugCurrentGOTermRelationships,
        doesNodeHaveOverrepresentation: doesNodeHaveOverrepresentation,
        getPairwisePatristicNode1: getPairwisePatristicNode1,
        setPairwisePatristicNode1: setPairwisePatristicNode1,
        getPairwisePatristicNode2: getPairwisePatristicNode2,
        setPairwisePatristicNode2: setPairwisePatristicNode2,
    };

}());