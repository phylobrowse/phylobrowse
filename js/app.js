// to set up and start app - for continuing interaction methods, see controller.js

var app = function () {
    var pbs;
    var geneToGoTermMap;
    var templates;
    var folder;

    $(window).resize(function () {
        var ww = $(window).width();
        bigPlantTree.resize(ww);
    });

    function startBigPlantVis() {
        $("#infoPanel").hide();
        $("#content").hide();

        folder = util.getUrlVars()["folder"];
        if (!folder) {
            overlay.addAlert("Can't load data - must have a folder specified in url parameters")
        }
        else {
            var path = "data/" + folder + "/";

            addEventListener('configLoadComplete', function (e) {
                new ClipboardJS('.clipboard');
                loader.loadCss(util.getUrlVars()["css"]);
                var tt = loader.getConfig()["termType"];
                if (tt) {
                    controller.setTermType(tt);
                }
                var mt = loader.getConfig()["measureType"];
                if (mt) {
                    controller.setMeasureType(mt);
                }
                var pa = loader.getConfig()["proteinAlignmentsFolder"];
                if (mt) {
                    controller.setProteinAlignmentsFolder(pa);
                }
                var ca = loader.getConfig()["codonAlignmentsFolder"];
                if (mt) {
                    controller.setCodonAlignmentsFolder(ca);
                }
                $("#termLabelUID").html(tt + "s");
                $("#pvalueSliderTermType").html(tt + "s");
                $("#uploadTermTypeSpan").html(tt + "s");
                $("#heatmapTermTypeSpan").html(tt + "s");
                loader.loadNewick(path);
            }, false);

            addEventListener('newickLoadComplete', function (e) {
                staticData.setRoot(Newick.parse(e.data));
                if (!staticData.doAllNodesHaveBranchLengths()) {
                    $("#toggleLayout").remove();
                }
                loader.loadPBS(path);
            }, false);

            addEventListener('pbsLoadComplete', function (e) {
                pbs = e.data;
                loader.loadGeneToGoFile(path);
            }, false);

            addEventListener('GeneToGOFileLoadComplete', function (e) {
                geneToGoTermMap = e.data;
                loader.loadGoTermDescriptionFile(path);
            }, false);

            addEventListener('GoTermDescriptionFileLoadComplete', function (e) {
                goTermDescriptionFileParser.parseFile(e.data);

                loader.genericLoad(path + loader.getConfig()["speciesFile"], "SpeciesLoaded");
                //loader.genericLoad(path + "/taxonomy.json", "SpeciesLoaded");
            }, false);

            addEventListener('SpeciesLoaded', function (e) {
                staticData.setSpecies(e.data);
                if (loader.getConfig()["geneInfoFile"]) {
                    loader.genericLoad(path + loader.getConfig()["geneInfoFile"], "GeneInfoLoaded");
                }
                else {
                    finishSetUp();
                }
            }, false);

            addEventListener('GeneInfoLoaded', function (e) {
                staticData.setGeneInfo(e.data);
                finishSetUp();
            }, false);

            function finishSetUp() {
                staticData.setGoTermIdToGoNameMap(goTermDescriptionFileParser.goIdToNameMap);
                staticData.setGoTermNameToGoIdMap(goTermDescriptionFileParser.goNameToIdMap);
                goTermDescriptionFileParser = null;

                staticData.setPBS(pbs);
                staticData.processData(geneToGoTermMap);
                bigPlantTree.setTaxonomyLevel(staticData.getTaxonomyLevels()[0]);
                treeComponents.createAllComponents($(window).width());

                if (!staticData.doAllNodesHaveBranchLengths()) {
                    console.log("removing toggle for node names/branch lengths")
                    $("#toggleHideBranchLengths").remove();
                }
                else {
                    $("#toggleHideBranchLengths").hide();
                }
                $("#uidComparePanel").hide();
                $("#content").show();
            }

            loader.loadConfig(path + "config.json");
        }
    }

    $.get('templates.html', function (templateData) {
        templates = $(templateData);
    });

    function getTemplates() {
        return templates;
    }


    return {
        getTemplates: getTemplates,
        startBigPlantVis: startBigPlantVis,
        getFolder:function() {return folder; }
    }
}();
