var staticData = (function () {
        var root;
        var pbs;
        var goTermIdToGoNameMap;
        var goTermNameToGoIdMap;
        var goTermNames;

        var goTermToNodesMap = {};// go term to Node map, and average p-value, if it exists
        var geneToNodesMap = {};
        var geneList = [];// each gene in map contains two lists - of nodes (nodeList)


        var lowestPValue = 100;
        var highestPValue = -1;

        var lowestAvgPBSValue = 10000000;
        var highestAvgPBSValue = 0;

        var lowestAbsPBSValue = 10000000;
        var highestAbsPBSValue = 0;

        var geneInfo;

        var taxonomyLevels;
        var uidNodes = [];
        // for set up only

        function setGoTermIdToGoNameMap(map) {
            goTermIdToGoNameMap = map; // note: used for updating info panel
        }

        function setGeneInfo(info) {
            geneInfo = info;
        }

        function getGeneInfoForGene(gene) {
            if (geneInfo) {
                return geneInfo[gene];
            }
         }

        function getPairwisePatristicDistance(node1, node2) {
            return 6; // todo implement for real
        }

        function setSpecies(data) {
            taxonomyLevels = data.levels;
            if (!taxonomyLevels) {
                console.error("taxonomy file does not have an entry for levels");
            }
            if (!root) {
                console.error("root is missing");
            }
            else {
                addSpecies(root, data);
            }

            function addSpecies(node, speciesData) {
                if (node.children) {
                    for (var i = 0; i < node.children.length; i++) {
                        addSpecies(node.children[i], speciesData);
                    }
                }
                else {
                    node.species = speciesData[node.name];
                }
            }
        }

        /**
         * Nodes can get associated with genes from the pbs file.
         * Nodes can get associated with goTerms from the pbs file and the gene-to-go file
         * This merges the genes from the geneToGoTermMap with the genes from the pbs map */
        function processData(geneToGoTermMap) {
            setUpGeneOrderedHashmap(geneToGoTermMap);
            processGenesAndNodes(pbs);
            createGoTermList();
            createGoTermPValues();

            // Sets geneToGoTermMap's goTermList value and adds gene to geneList.
            // This will include genes that do not have p-values.
            function setUpGeneOrderedHashmap(geneToGoTermMap) {
                for (var gene in geneToGoTermMap) {
                    geneList.push(gene);
                }
                geneList.sort(util.alphabeticalSort);
            }

            // Note: Called after inital set up of the geneList from the geneTOGoTermMap.
            // This also calculates average PBS value
            function processGenesAndNodes() {
                for (var nodeName in pbs) {
                    var genePartition = pbs[nodeName].genePartition;
                    if (genePartition) {
                        var totalPBSValue = 0;
                        var maxAbsolutePBSValue = 0;
                        _.each(pbs[nodeName].genePartition, function (item) {
                            var gene = item.gene_id;
                            var pbsVal = parseInt(item.value)
                            totalPBSValue += pbsVal;
                            if (Math.abs(pbsVal) > maxAbsolutePBSValue) {
                                maxAbsolutePBSValue = Math.abs(pbsVal);
                            }
                            if (!geneToNodesMap[gene]) {
                                geneToNodesMap[gene] = [];
                            }
                            if (!geneToNodesMap[gene].nodeList) {
                                geneToNodesMap[gene].nodeList = [];
                            }
                            geneToNodesMap[gene].nodeList.push(nodeName);
                            addNodeNameToGoTermMap(gene, nodeName);
                        });

                        pbs[nodeName].maxAbsolutePBSValue = maxAbsolutePBSValue;
                        configurePBSValues(maxAbsolutePBSValue);

                        if (totalPBSValue == 0) {
                            var avgPBSValue = 0; // to avoid NaN
                        }
                        else {
                            var avgPBSValue = totalPBSValue / genePartition.length;
                        }
                        pbs[nodeName].avgPBSValue = avgPBSValue;
                        configureAvgPBSValues(avgPBSValue);
                    }
                }
                lowestAvgPBSValue = util.twoDecimalPlaces(lowestAvgPBSValue);
                console.log("highestAvgPBSValue: " + highestAvgPBSValue);
                console.log("lowestAvgPBSValue: " + lowestAvgPBSValue);
                console.log("highestAbsPBSValue: " + highestAbsPBSValue);
                console.log("lowestAbsPBSValue: " + lowestAbsPBSValue);

                /**
                 * Inverts gene-to-GO-term map to create GO-term-to-node-name map
                 * @param gene
                 * @param nodeName
                 */
                function addNodeNameToGoTermMap(gene, nodeName) {
                    var goTermList = geneToGoTermMap[gene];
                    _.each(goTermList, function (goTerm) {
                        if (!goTermToNodesMap[goTerm]) {
                            goTermToNodesMap[goTerm] = [];
                        }
                        if (-1 == goTermToNodesMap[goTerm].indexOf(nodeName)) {
                            goTermToNodesMap[goTerm].push(nodeName);
                        }
                    });
                }

            }

            /**
             * Creates array of all GO terms, for alphabetizing
             */
            function createGoTermList() {
                goTermNames = [];
                for (var goName in goTermNameToGoIdMap) {
                    goTermNames.push(goName);
                }
                console.log("goTermNames has " + goTermNames.length + " terms.");
                goTermNames.sort(util.alphabeticalSort);
            }

            // Note: needed for computing dot size
            function configureAvgPBSValues(pbs) {
                lowestAvgPBSValue = Math.min(lowestAvgPBSValue, pbs);
                highestAvgPBSValue = Math.max(highestAvgPBSValue, pbs);
            }

            function configurePBSValues(pbs) {
                lowestAbsPBSValue = Math.min(lowestAbsPBSValue, pbs);
                highestAbsPBSValue = Math.max(highestAbsPBSValue, pbs);
            }

            /**
             * Scans ovrTerms in pbs data structure and calculates average pValue for GO terms.
             * At the same time, configures highest and lowest pValues.
             * @param pbs
             */
            function createGoTermPValues() {
                for (var nodeName in pbs) {
                    if (pbs[nodeName].ovrTerm) {
                        //console.log(nodeName + ": " + pbs[nodeName].ovrTerm.length);
                        _.each(pbs[nodeName].ovrTerm, function (term) {
                                var goTerm = goTermToNodesMap[term.id];
                                if (!goTerm) {
                                    goTerm = [nodeName];
                                    goTermToNodesMap[term.id] = goTerm;
                                }
                                else {
                                    if (-1 == goTerm.indexOf(nodeName)) {
                                        goTerm.push(nodeName);
                                    }
                                }
                                //console.log(term.id + ": " + term.pValue);
                                if (!goTerm.numPValues || goTerm.numPValues == 0) { // if it doesn't exist
                                    goTerm.totalPValue = 0;
                                    goTerm.numPValues = 0;
                                }
                                goTerm.totalPValue += parseFloat(term.pValue);
                                //console.log("adding " + term.id + " to " + term.pValue + " to equal " + goTerm.totalPValue);
                                goTerm.numPValues++;

                                // console.log("no term in map for " + term.id);
                                // because (I think) the gene-to-GO-term file should, but doesn't, have this go-term mapped to it
                                // and the reason for that, is that the pbs file does not have the genes that map to this go term listed anywhere
                            }
                        );
                    }
                }

                for (var goTermId in goTermToNodesMap) {
                    if (goTermToNodesMap[goTermId].totalPValue) {
                        //console.log(goTermId + " total: " + goTermToNodesMap[goTermId].totalPValue + ", " + goTermToNodesMap[goTermId].numPValues);
                        goTermToNodesMap[goTermId].avgPValue = goTermToNodesMap[goTermId].totalPValue / goTermToNodesMap[goTermId].numPValues;
                        //console.log(goTermId + " avg: " + goTermToNodesMap[goTermId].avgPValue);
                        lowestPValue = Math.min(lowestPValue, goTermToNodesMap[goTermId].avgPValue);
                        highestPValue = Math.max(highestPValue, goTermToNodesMap[goTermId].avgPValue);
                    }
                }
            }
        }


// for use while running app

        function getGoTermName(goTermId) {
            return goTermIdToGoNameMap[goTermId];
        }

        function getAvgPValue(goTermId) {
            if (goTermToNodesMap[goTermId]) {
                return goTermToNodesMap[goTermId].avgPValue;
            }
            else {
                console.error("No GO term to node in map for " + goTermId);
                return undefined;
            }
        }

        function setGoTermNameToGoIdMap(map) {
            goTermNameToGoIdMap = map;
        }

// note: used to create GO term list
        function getGOtermIdByName(goTermName) {
            return goTermNameToGoIdMap[goTermName];
        }

        function getNodesForGene(gene) {
            var geneObj = geneToNodesMap[gene];
            if (!geneObj) {
                console.error("no mapping for " + gene);
            }
            return geneObj.nodeList;
        }

        function getNodesForGoTerm(goTermId) { // used in dynamicData
            return goTermToNodesMap[goTermId];
        }

        function walkTree(f) {
            _walkTree(f, root);
        }

        function _walkTree(f, node) {
            f(node);
            if (node.children) {
                _.each(node.children, function (childNode) {
                    _walkTree(f, childNode);
                });
            }
        }

        /** Returns object with two arrays specifying which nodes have branch lengths, and how many do not */
        function doAllNodesHaveBranchLengths() {
            // skip root - it's added on by code and does not have branch lengths.
            var doThey = true;
            _.each(root.children, function (node) {
                _walkTree(function (n) {
                    if (n.length === undefined) {
                        doThey = false;
                    }
                }, node);
            });
            return doThey;
        }

        function printRootDebugInfo() {
            var s = "";
            s = _printRootDebugInfo(root, s, false, 0);
            var w = window.open();
            w.document.write(s);
        }

        function getUnionIntersectDifference(nodeArray) {
            console.log(nodeArray);
        }

        function _printRootDebugInfo(node, s, hidden, numTabs) {
            for (var n = 0; n < numTabs; n++) {
                s += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            }
            if (hidden) {
                s += "<span style='color:#888888;'>"
            }
            s += node.name;
            s += hidden ? " (hidden) " : "";
            s += node.manuallyCollapsed ? " (manually collapsed) " : "";
            s += node.manuallyExpanded ? " (manually expanded) " : "";
            s += node.expanded ? " expanded " : "";
            s += node._children ? " (children hidden) " : "";
            s += node.filterMode == "highlighted" ? " highlighted " : "";
            if (hidden) {
                s += "</span>"
            }
            s += "<br/>";
            if (node.children) {
                for (var i = 0; i < node.children.length; i++) {
                    s = _printRootDebugInfo(node.children[i], s, hidden, numTabs + 1);
                }
            }
            if (node._children) {
                for (var i = 0; i < node._children.length; i++) {
                    s = _printRootDebugInfo(node._children[i], s, true, numTabs + 1);
                }
            }
            return s;
        }

        function setPBS(p) {
            pbs = p;
        }

        function getNodeFromRootByName(name) {
            var node_name = name;
            var node;
            var getNode = function (n) {
                //console.debug(n.name +" vs "+ node_name);
                if (n.name == node_name) {
                    node = n;
                }
            }
            walkTree(getNode)
            return node;
        }

        function setRoot(r) {
            root = r;
            root.x0 = 0;
            root.y0 = 0;
            root.id = "root";
            addParents(root);

            function addParents(node) {
                if (node.children) {
                    for (var i = 0; i < node.children.length; i++) {
                        node.children[i].parent = node;
                        if (node.children[i].name.length == 0) {
                            node.children[i].id = (node.id + "_" + i).replace(/ /g, "");
                        }
                        else {
                            node.children[i].id = (node.children[i].name).replace(/ /g, "");
                        }
                        addParents(node.children[i]);
                    }
                }
            }
        };

        function getUIDNodes(nodeId) {
            return uidNodes;
        }

        function clearUIDNodes() {
            uidNodes = [];
        }

        function addToUIDNodes(nodeId) {
            var added = true;
            if (uidNodes.indexOf(nodeId) == -1) {
                uidNodes.push(nodeId);
                console.log("adding " + nodeId);

            }
            else {
                console.log("removing " + nodeId);
                _.pull(uidNodes, nodeId);
                added = false;
            }
            var data = {goTerms: processUIDGoTerms(uidNodes), genes: processUIDGenes(uidNodes)};
            console.log(data);
            util.dispatchEvent("addedNodeToUIDNodes", data);
            return added;
        }

        function processUIDGoTerms(nodes) {
            if (nodes.length > 1) {
                // set up first array for comparison
                var g0 = _.map(pbs[nodes[0]].ovrTerm, function (gp) {
                    return gp.desc;
                });
                var union = g0;
                var intersect = g0;
                var difference = g0;
                // compare with all subsquent arrays
                for (var i = 1; i < nodes.length; i++) {
                    console.log(nodes[i]);
                    if (pbs[nodes[i]]) { // sometimes a node is in the .tre file but not in pbs
                        var g = _.map(pbs[nodes[i]].ovrTerm, function (gp) {
                            return gp.desc;
                        });
                    }
                    else {
                        g = [];
                    }
                    union = _.union(union, g); // Note: may be more efficient to gather all and do union & intersect once.
                    intersect = _.intersection(intersect, g);
                    //console.log("intersection of " + intersect + " + " + g + " is " + intersect);
                    difference = _.difference(union, intersect);


                }
                return {"union": union, "intersect": intersect, "difference": difference}
            }
            else {
                return {"union": [], "intersect": [], "difference": []}
            }
        }

        function processUIDGenes(nodes) {
            if (dynamicData.getKeepExpandedList().length > 0) {
                n = _.filter(nodes, function (node) {
                    return dynamicData.inKeepExpandedList(node);
                });
                nodes = n;
            }
            if (nodes.length > 1) {
                // set up first array for comparison
                var g0 = _.map(pbs[nodes[0]].genePartition, function (gp) {
                    return gp.gene_id;
                });
                var union = g0;
                var intersect = g0;
                var difference = g0;
                // compare with all subsquent arrays
                for (var i = 1; i < nodes.length; i++) {
                    if (pbs[nodes[i]]) { // sometimes a node is in the .tre file but not in pbs
                        var g = _.map(pbs[nodes[i]].genePartition, function (gp) {
                            return gp.gene_id;
                        });
                    }
                    else {
                        g = [];
                    }
                    union = _.union(union, g); // Note: may be more efficient to gather all and do union & intersect once.
                    intersect = _.intersection(intersect, g);
                    difference = _.difference(union, intersect);
                }
                return {"union": union, "intersect": intersect, "difference": difference}
            }
            else {
                return {"union": [], "intersect": [], "difference": []}
            }
        }

        return {
            addToUIDNodes: addToUIDNodes,
            clearUIDNodes: clearUIDNodes,
            getUIDNodes: getUIDNodes,
            getNodeFromRootByName: getNodeFromRootByName,
            doAllNodesHaveBranchLengths: doAllNodesHaveBranchLengths,
            getNodesForGoTerm: getNodesForGoTerm,
            getGeneList: function () {
                return geneList;
            },
            getGoTermNamesList: function () {
                return goTermNames;
            },
            getPBS: function () {
                return pbs;
            },
            getRoot: function () {
                return root;
            },
            getAvgPValue: getAvgPValue,
            getGOtermIdByName: getGOtermIdByName,
            getGoTermName: getGoTermName,
            getNodesForGene: getNodesForGene,
            getLowestPValue: function () {
                return lowestPValue;
            },
            getHighestPValue: function () {
                return highestPValue;
            },
            getLowestAvgPBSValue: function () {
                return lowestAvgPBSValue;
            },
            getHighestAvgPBSValue: function () {
                return highestAvgPBSValue;
            },
            getLowestAbsPBSValue: function () {
                return lowestAbsPBSValue;
            },
            getHighestAbsPBSValue: function () {
                return highestAbsPBSValue;
            },
            getPairwisePatristicDistance: getPairwisePatristicDistance,
            getTaxonomyLevels: function () {
                return taxonomyLevels;
            },
            processData: processData,
            printRootDebugInfo: printRootDebugInfo,
            setGeneInfo:setGeneInfo,
            getGeneInfo: function() {
                return geneInfo;
            },
            getGeneInfoForGene:getGeneInfoForGene,
            setGoTermIdToGoNameMap: setGoTermIdToGoNameMap,
            setGoTermNameToGoIdMap: setGoTermNameToGoIdMap,
            setRoot: setRoot,
            setSpecies: setSpecies,
            setPBS: setPBS,
            walkTree: walkTree
        }
            ;

    }()
);