var fs = require("fs");
var _ = require("lodash");
var mocha = require('mocha');
var chai = require('chai');

eval(fs.readFileSync('lib/newick.js') + '');
var Newick = this;

eval(fs.readFileSync('lib/newick.js') + '');
eval(fs.readFileSync('js/parsers/GeneToGOParser.js') + '');
eval(fs.readFileSync('js/OrderedHashmap.js') + '');
eval(fs.readFileSync('js/staticData.js') + '');
eval(fs.readFileSync('js/util.js') + '');

function setUpStaticData(folderName) {
    console.log("reading config file");
    var config = JSON.parse(fs.readFileSync(folderName+"config.json"));

    //var pbs = fs.readFileSync("data/" + config["pbsFile"]);


    var data = fs.readFileSync("data/" + config["newickFile"], "utf8");
    staticData.setRoot(Newick.parse(data));

    data = fs.readFileSync("data/" + config["speciesFile"], "utf8");
    data = JSON.parse(data);
    staticData.setSpecies(data);
    //console.log(staticData.getRoot());

    data = fs.readFileSync("data/" + config["GOannotationFile"], "utf8");
    goTermDescriptionFileParser.parseFile(data);
    staticData.setGoTermIdToGoNameMap(goTermDescriptionFileParser.goIdToNameMap);
    staticData.setGoTermNameToGoIdMap(goTermDescriptionFileParser.goNameToIdMap);

    data = fs.readFileSync("data/" + config["pbsFile"], "utf8");
    data = JSON.parse(data);
    staticData.setPBS(data);
    //console.log(staticData.getPBS().N58);

    data = fs.readFileSync(config["geneToGOFile"], "utf8");
    data = JSON.parse(data);
    staticData.processData(data);

}

function checkGoTerms() {
    var pbs = staticData.getPBS();
    for (var nodeId in pbs) {
        var goTerms = pbs[nodeId].ovrTerm;
        _.each(goTerms, function (term) {
            var term = staticData.getNodesForGoTerm(term.id);
            if (!term) {
                console.log("error: " + nodeId + " should have " + term.id + "  in GoTermIdToNodesMap data structure.")
            }
            else {
                //console.log(term);
            }
        })
    }

    var list = staticData.getGoTermNamesList();
    _.each(list, function (term) {
        var id = staticData.getGOtermIdByName(term);
        if (!id) {
            console.log("error: No GO term id for " + term);
        }
        var mapping = staticData.getNodesForGoTerm(id);
        if (mapping) {
            //if (mapping.avgPValue) {
            //  console.log(mapping.pValue);
            //}
        }
        else {
            console.log("error: No GO term to node in map for " + id);
        }
    })
}

function checkPBS() {
    staticData.walkTree(function (node) {
        var n = staticData.getPBS()[node.name];
        if (!n && node.children) {
            console.log("warning: No pbs info for " + node.name);
           // console.log(node);
        }
    });
}

function checkTree() {
    var lengths = staticData.doAllNodesHaveBranchLengths();
    console.log(lengths);

    if (!lengths){
        console.log("tree is missing some branch lengths - will be impossible to show Phylogram (viz will be Cladogram only)");
    }
}

function checkGeneList() {
    _.each(staticData.getGeneList(), function (gene) {
        try {
            staticData.getNodesForGene(gene);
        }
        catch (e) {
            try {
                if (staticData.getNodesForGene(gene.toUpperCase())) {
                    console.error("issue with different cases: pbs file has " + gene.toUpperCase() + " but G2GO file has " + gene);
                }
                else {
                    console.error(e.message);
                }
            }
            catch (e2) {

            }
        }
    });
}

function checkAll(config) {
    setUpStaticData(config);
    checkGoTerms();
    checkGeneList();
    checkPBS();
    checkTree();
}

//checkAll("data/gilData/config1.json");
checkAll("data/original");