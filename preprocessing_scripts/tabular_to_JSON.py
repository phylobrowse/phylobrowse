#!/usr/bin/env python

#########################################################################################
### The MIT License (MIT)
### 
### Copyright (c) 2018 Gil Eshel
### 
### Permission is hereby granted, free of charge, to any person obtaining a copy
### of this software and associated documentation files (the "Software"), to deal
### in the Software without restriction, including without limitation the rights
### to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
### copies of the Software, and to permit persons to whom the Software is
### furnished to do so, subject to the following conditions:
### 
### The above copyright notice and this permission notice shall be included in all
### copies or substantial portions of the Software.
###
### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
### IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
### FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
### AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
### LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
### OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
### SOFTWARE.
#########################################################################################
### DATE CREATED: Jun 4, 2018
### AUTHOR: Gil Eshel
### CONTACT1: ge30@nyu.edu
### CONTACT2: giltu1@gmail.com
###
### CITATION1: PhyloBrowse, https://bitbucket.org/PhyloBrowse/
### CITATION2: xxxxxx (2018) PhyloBrowse: A visual tool to explore phylogenomic data. 
###	Bioinformatics xx(xx):xx-xx, doi: 10.1093/bioinformatics/xxxxx.
#########################################################################################
### DESCRIPTION:
### This script can be used to parse tabular file into JSON format, with the list of columns as levels. In relation to PhyloBrowse visualization, you can use it to generate either the taxonomy.json or a gene_info.json file (information that will be displayed in a the opened tab, after clicking on the gene id in the node tooltip window).
### 
### Usage: tabular_to_JSON.py <input tab-delimited file> <output JSON file>
###
### Input arguments:
### (1) A tab-delimited file containing ids (gene ids, species labels, etc) in the first column, and one or more columns with their related information, such as gene symbols, gene description, GO annotations, etc., or species taxonomy classification levels
###	(2) A string indicating the output file name (Default: "info.json")
#########################################################################################

import sys, json, csv

### Input ###

json_format = {} # parse the input file into a dictionary to save as JSON format
try:
	with open(sys.argv[1], 'r') as tabFile:
		levels = next(tabFile).strip().split('\t') # list of column headers
		for line in tabFile:
			info_list = line.strip().split('\t')
			info_dict = {}
			for i in xrange(len(levels)):
				info_dict[levels[i]] = info_list[i]
			json_format[info_list[0]] = info_dict
except:
	sys.exit("Error - Please provide a tab-delimited file, where first column represent a unique id (gene_id, species_label, etc.), first line is used as header !\n Usage: tabular_to_JSON.py <input tab-delimited file> <output JSON file>")

json_format['levels']= levels # add the levels (for PhyloBrowse...)

### Save taxonomy.json file ###
try:
	with open(sys.argv[2], 'w') as fp: 
		json.dump(json_format, fp, indent=4) # if we don't want the indent output file, change to json.dump(json_format, fp), we can also sort the keys alphabetically by adding sort_keys=True
except:
	print("Output file name was not indicated. Results are saved in 'info.json'")
	with open('info.json', 'w') as fp:
		json.dump(json_format, fp, indent=4)