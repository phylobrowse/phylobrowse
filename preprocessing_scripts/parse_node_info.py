#!/usr/bin/env python

#########################################################################################
### The MIT License (MIT)
### 
### Copyright (c) 2018 Gil Eshel
### 
### Permission is hereby granted, free of charge, to any person obtaining a copy
### of this software and associated documentation files (the "Software"), to deal
### in the Software without restriction, including without limitation the rights
### to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
### copies of the Software, and to permit persons to whom the Software is
### furnished to do so, subject to the following conditions:
### 
### The above copyright notice and this permission notice shall be included in all
### copies or substantial portions of the Software.
###
### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
### IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
### FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
### AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
### LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
### OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
### SOFTWARE.
#########################################################################################
### DATE CREATED: May 10, 2018
### AUTHOR: Gil Eshel
### CONTACT1: ge30@nyu.edu
### CONTACT2: giltu1@gmail.com
###
### CITATION1: PhyloBrowse, https://bitbucket.org/PhyloBrowse/
### CITATION2: xxxxxx (2018) PhyloBrowse: A visual tool to explore phylogenomic data. 
###	Bioinformatics xx(xx):xx-xx, doi: 10.1093/bioinformatics/xxxxx.
#########################################################################################
### DESCRIPTION:
### This script can be used for preparing a node_info.json file for PhyloBrowse visualization.
### Please execute this script from within the directory where all the required input files are contained (You can change the code if you prefer to add the path to these files as an argument).
###
### Usage: parse_node_info.py <Gene-to-node file> <enrichment tables file name pattern: e.g. '_all_enriched_terms_table.txt'> <Node_support file> <annotation_type> <output.json>
###
### Input arguments:
### (1) Gene-to-node association file that includes all the gene-to-node association values, e.g. PBS or dN/dS, for all the nodes. It should be tab-separated and contain the following three columns (expecting a header line): (I) Node ID, (II) Gene ID, and (III) gene-to-node value
### (2) Name pattern of the node annotation enrichment files (GO terms, KEGG pathways, etc.), e.g. if the node enrichment files are named: "N1_enriched_terms_table.txt", "N2_enriched_terms_table.txt",... you should provide "_enriched_terms_table.txt" as the argument.
###		The node annotation enrichment files should be tab-separated and contain the following three columns (expecting a header line): (I) Annotation ID, (II) Annotation description, and (III) Enrichment significant value (e.g. p-value).
### (3) A file containing the node support values (e.g. Bootstrap or Jackknife values). It should be tab-separated and contain the following two columns (No header line): (I) Node ID and (II) Support value. If no support values are provided, please indicate "NA" or keep empty.
### (4) A string indicating the annotation type (e.g. "GO Term", "KEGG", etc.)
###	(5) A string indicating the output file name
#########################################################################################

import sys, fnmatch, os, json

### Check for correct number of arguments ###
if len(sys.argv) != 6:
	sys.exit("Error - Please provide all required arguments. Usage: parse_node_info.py gene_to_node_file pattern_name_for_enrichment_files node_support_file a_string_for_annotation_type output.json !")


### Define some variables ###
ovrTerm_list_of_list = [] # list that store all the ovrTerm of each node (each ovrTerm is a dictionary)
genePartition = [] # list to store gene_to_node and model_gene_id values(as dictionaries) for each node
genePartition_list_of_list = [] # list that store all the genePartition of each node
node_enrichemnt_table_names_list = [] # generate a list which contains the names of the enrichment tables (of each node)
node_ids_with_calculations = [] # store a list of node ids ("N1", "N2" ...) in the same order as the node_enrichemnt_table_names_list
node_json = {} # final dict to output

type_analysis = sys.argv[4] # added to the ovrTerm dictionary...


### Get node id and support values ###
node_ids = [] # store a list of all node ids ("N1", "N2" ...) from the newich tree
node_support = {} # dictionary to store the node support value of each node
try:
	with open(sys.argv[3], 'r') as sup:
		for line in sup:
			li=line.strip()
			if len(li.split("\t"))>1:
				node_ids.append(li.split("\t")[0])
				node_support[li.split("\t")[0]] = li.split("\t")[1]
			else:
				node_ids.append(li)
				node_support[li] = "NA"
except:
	print("Please provide a tab-delimited file with node ids (required) and their node support values (optional) !")		


### parse annotation enrichment tables:
for file in os.listdir('.'): # go through the files in the current working folder to find the enrichment tables
    if fnmatch.fnmatch(file, 'N*' + sys.argv[2]):
        node_enrichemnt_table_names_list.append(file) # list of enrichment table file names (only nodes with gene_to_node calculations...!)
        node_ids_with_calculations.append(file.split("_")[0]) # list of node ids with gene_to_node calculations corresponding the the enrichment table names list order ...

for f in node_enrichemnt_table_names_list:
	with open(f, 'r') as d:
		next(d)
		ovrTerm = list() # list to store enriched term results (as dictionaries) 
		for line in d:
			li=line.strip()
			if len(li.split("\t")) != 3:
				sys.exit("Error - Please provide a tab-delimited files for annotation enrichment information ! ")
			else:
				pValue=li.split("\t")[2]
				desc = li.split("\t")[1]
				AnnotationID = li.split("\t")[0]
				ovrTerm_dict = {"pValue":pValue, "desc":desc, "id":AnnotationID, "type":type_analysis}
				ovrTerm.append(ovrTerm_dict)
		ovrTerm_list_of_list.append(ovrTerm)

### parse the gene_to_node_filtered file:
node_id_from_gene_to_node_table =[] # record the node_id
gene_to_node = [] # record the gene_to_node value
mode_gene_id = [] # record the model_gene_id value


with open(sys.argv[1], 'r') as f:
	next(f) # skip the header line...
	for line in f:
		li=line.strip() 
		if len(li.split("\t")) != 3:
			sys.exit("Error - Please check the format of the gene_to_node file: Should be tab-delimited, and contain three columns: (1) Node ID, (2) Gene ID, and (3) gene_to_node value !")
		else:
			if li.split("\t")[1]!="NA": # store only partitions with model species identifier
				node_id_from_gene_to_node_table.append(li.split("\t")[0])
				gene_to_node.append(li.split("\t")[2])
				mode_gene_id.append(li.split("\t")[1])


for i in range(0, len(node_ids_with_calculations)): # Generate the list of partition dictionaries (genePartition_list_of_list) 
    for j in range(0, len(node_id_from_gene_to_node_table)):
        if node_ids_with_calculations[i]==node_id_from_gene_to_node_table[j]:
            value_gene_id = {"value":gene_to_node[j],"gene_id":mode_gene_id[j]}
            genePartition.append(value_gene_id)
            if i==len(node_ids_with_calculations)-1 and j==len(node_id_from_gene_to_node_table)-1:
                genePartition_list_of_list.append(genePartition)
        else:
            if len(genePartition) != 0:
            	genePartition_list_of_list.append(genePartition)
            	genePartition = []


### Add empty lists to the genePartition_list_of_list and ovrTerm_list_of_list for nodes without gene_to_node calculations (generate new lists in the length and order of the node_ids list):
genePartition_list_of_list_all_nodes = [] 
ovrTerm_list_of_list_all_nodes = [] 

for i in range(0, len(node_ids)):
	if node_ids[i] in node_ids_with_calculations: # check if the i item in node_ids exist in the node_ids_with_calculations list (the sub list...)
		j = node_ids_with_calculations.index(node_ids[i]) # if it does, get the index for its position on the node_ids_with_calculations list
		genePartition_list_of_list_all_nodes.append(genePartition_list_of_list[j]) # append the genePartition_list_of_list item for that node
		ovrTerm_list_of_list_all_nodes.append(ovrTerm_list_of_list[j]) # append the ovrTerm_list_of_list item for that node
	else:
		genePartition_list_of_list_all_nodes.append([])
		ovrTerm_list_of_list_all_nodes.append([])
		
### Create the json file		         
node_json = {}  # open a dictionary for the final json object
for i in range(0, len(node_ids)): # for each node generate a dictionary that contains the ovrTerm, the genePartition lists and the support value
	node = dict()
	node['ovrTerm'] = list()
	node['ovrTerm'] = ovrTerm_list_of_list_all_nodes[i]
	node['genePartition'] = list()
	node['genePartition'] = genePartition_list_of_list_all_nodes[i]
	node['nodeSupport'] = list()
	node['nodeSupport'] = node_support[node_ids[i]]
	json_object = json.dumps(node) 
	node_json[node_ids[i]] = json_object # add the json dump object for that node to the final node_json dictionary

json_string = str(node_json)
json_string = json_string.replace('\\',"") 
json_string = json_string.replace('""','"')
json_string = json_string.replace("'",'"')
json_string = json_string.replace('"{"','{"')
json_string = json_string.replace('}"','}')
json_string = json_string.replace(", ",",")
json_string = json_string.replace(": ",":")
json_string = json_string.replace('}]}"','}]}')

try:
	if '.' in sys.argv[5]:
		if sys.argv[5].split('.')[-1] == 'json':
			with open(sys.argv[5], 'w') as fp: # Write the final node_json dictionary to the node_info.json file
				fp.write(json_string)		
		else:
			with open(sys.argv[5], 'w') as fp: # Write the final node_json dictionary to the node_info.json file
				fp.write(json_string)			
			print("Please check your output file name - not ending with '.json' ! ")
	else:
		with open(sys.argv[5]+'.json', 'w') as fp: # Write the final node_json dictionary to the node_info.json file
			fp.write(json_string)
except:
	print("Please provide an output json file name ! For example node_info.json")		