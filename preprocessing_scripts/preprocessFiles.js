var fs = require("fs");
var _ = require("lodash");

var geneList = [];
var config;
var goToDescriptionMap = {};
var pbs;
var geneToGoMap;

function loadGeneToGoFiles() {
    console.log("reading config file");
    config = JSON.parse(fs.readFileSync(configName));

    console.log("reading pbs file from config: " + config["pbsFile"]);
    pbs = fs.readFileSync("data/" + config["pbsFile"]);
    pbs = JSON.parse(pbs);
    populateGeneList(pbs);

    console.log("reading gene-to-go file from config: " + config["geneToGOFile"]);
    var g2g = fs.readFileSync(config["geneToGOFile"], 'utf-8');

    geneToGoMap = JSON.parse(g2g);

    function populateGeneList(pbs) {
        for (var nodeName in pbs) {
            var genePartition = pbs[nodeName].genePartition
            if (genePartition) {
                _.each(genePartition, function (item) {
                    var gene = item.atOrtholog;
                    //gene = gene.replace("T", "t");
                    //gene = gene.replace("G", "g");
                    if (geneList.indexOf(gene) == -1) {
                        geneList.push(gene);
                    }
                });
            }
        }
    }
}

var createGeneToGOFileJSON = function () {
    console.log("saving filtered gene-to-go file: " + saveGeneToGoFileName);
    var filteredMap = {};
    var counter = 0;

    _.each(geneList, function (item) {
        filteredMap[item] = geneToGoMap[item];
        counter++;
    });
    console.log(counter + " items in filtered gene-to-go data");
    console.log(saveGeneToGoFileName);
    fs.writeFileSync(saveGeneToGoFileName, JSON.stringify(filteredMap));
    return filteredMap;
}

var saveGoAnnotations = function (s) {
    console.log("saving GO description file: " + saveGoDescFileName);
    fs.writeFileSync(saveGoDescFileName, s);
}

var getGoAnnotations = function () {
    console.log("loading GO Annotations from config: " + config["GOannotationFile"]);
    var data = fs.readFileSync(config["GOannotationFile"], 'utf-8');
    parseFile(data);

    return getFilteredGoDescriptions();

    function parseFile(data) {
        var tokens = data.split("\n");
        var length = tokens.length;
        for (var i = 0; i < length; i++) {
            var token = tokens[i];
            parseToken(token);
        }
        console.log(length + " unfiltered GO terms");
    }

    function parseToken(token) {
        var tokens = token.split(" | ");
        if (tokens[1]) {
            goToDescriptionMap[tokens[0]] = tokens[1];
        }
    }

    function getFilteredGoDescriptions(filteredMap) {
        var allGoTermsList = [];
        /*for (var gene in geneToGoMap) {
         var goTerms = geneToGoMap[gene];
         allGoTermsList = _.concat(allGoTermsList, goTerms);
         allGoTermsList = _.uniq(allGoTermsList);
         }*/
        for (var gene in filteredMap) {
            var goTerms = filteredMap[gene];
            allGoTermsList = _.concat(allGoTermsList, goTerms);
            allGoTermsList = _.uniq(allGoTermsList);
        }
        console.log(_.uniq(allGoTermsList).length + " filtered GO terms");
        for (var key in pbs) {
            if (pbs[key].ovrTerm) {
                var flat = _.flatMap(pbs[key].ovrTerm, function (item) {
                    if (!item.id.startsWith("GO:")) {
                        throw new Error("invalid GO term, check PBS file, " + key + ": " + item.id);
                    }
                    return item.id;
                });
                allGoTermsList = _.concat(allGoTermsList, flat);
                allGoTermsList = _.uniq(allGoTermsList);
            }
        }
        console.log(_.uniq(allGoTermsList).length + " filtered GO terms");
        var s = "";
        _.each(allGoTermsList, function (goTerm) {
            s += goTerm + " | " + goToDescriptionMap[goTerm] + "\n";
        })
        return s;
    }

    function getFilteredGoDescriptionsOld() {
        var uniqueGoTermsList = [];
        var s = "";
        for (var gene in geneToGoMap) {
            var goTerms = geneToGoMap[gene];
            _.each(goTerms, function (goTerm) {
                if (-1 == uniqueGoTermsList.indexOf(goTerm)) {
                    uniqueGoTermsList.push(goTerm);
                    s += goTerm + " | " + goToDescriptionMap[goTerm] + "\n";
                }
            });
        }
        console.log(uniqueGoTermsList.length + " filtered GO terms");
        return s;
    }
}

console.log("reading arguments from cmd...");
var configName = process.argv[2];
console.log("\tusing config: " + configName);
var saveGeneToGoFileName = process.argv[3];
console.log("\twill save gene-to-go to: " + saveGeneToGoFileName);
var saveGoDescFileName = process.argv[4];
console.log("\twill to save Go descriptions to: " + saveGoDescFileName);
console.log();

if (!configName || !saveGeneToGoFileName || !saveGoDescFileName) {
    console.log("usage: node preprocessGeneToGo.js [config file in /data dir] [save gene-to-go file name] [save go descriptions file name]");
}

loadGeneToGoFiles();
console.log("----------------------------------------");
console.log("Filtering Gene-to-GO...")
var filteredMap = createGeneToGOFileJSON();
console.log();
console.log("----------------------------------------");
console.log("Filtering GO Annotations...")
saveGoAnnotations(getGoAnnotations(filteredMap));

// Example: node preprocessFiles.js data/configPreprocess.json data/processedG2GO.json data/processedGODescriptions.txt