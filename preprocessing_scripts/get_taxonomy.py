#!/usr/bin/env python

#########################################################################################
### The MIT License (MIT)
### 
### Copyright (c) 2018 Gil Eshel
### 
### Permission is hereby granted, free of charge, to any person obtaining a copy
### of this software and associated documentation files (the "Software"), to deal
### in the Software without restriction, including without limitation the rights
### to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
### copies of the Software, and to permit persons to whom the Software is
### furnished to do so, subject to the following conditions:
### 
### The above copyright notice and this permission notice shall be included in all
### copies or substantial portions of the Software.
###
### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
### IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
### FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
### AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
### LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
### OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
### SOFTWARE.
#########################################################################################
### DATE CREATED: May 10, 2018
### AUTHOR: Gil Eshel
### CONTACT1: ge30@nyu.edu
### CONTACT2: giltu1@gmail.com
###
### CITATION1: PhyloBrowse, https://bitbucket.org/PhyloBrowse/
### CITATION2: xxxxxx (2018) PhyloBrowse: A visual tool to explore phylogenomic data. 
###	Bioinformatics xx(xx):xx-xx, doi: 10.1093/bioinformatics/xxxxx.
#########################################################################################
### DESCRIPTION:
### This script can be used to retrieve species taxonomic classification and output it in a json format for PhyloBrowse visualization.
### It retrieves the NCBI information from the EBI website (see http://www.ebi.ac.uk/ena/browse/taxonomy-service and http://ena-docs.readthedocs.io/en/latest/tax.html). Therefore, Internat connection is required during program execution.
###
### Usage: get_taxonomy.py <species_names.txt file with one species name per line> <taxonomy.json output file name>
###
### Input arguments:
### (1) A file containing a list of species or genus names, one per line.
###	(2) A string indicating the output file name (Default: "taxonomy.json")
#########################################################################################

import sys, json, csv, os, ssl

# Also need the urllib2 module (different between Python 2 and 3):
try:
    # For Python 3.0 and later
    from urllib.request import urlopen
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen

### This modification is to prevent a possible "urllib2.URLError: <urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:661)>" error ###
# This was suggested in: https://stackoverflow.com/questions/19268548/python-ignore-certificate-validation-urllib2 - they claim that it is not recommended
#ctx = ssl.create_default_context()
#ctx.check_hostname = False
#ctx.verify_mode = ssl.CERT_NONE

# This workaround is more recommended according to https://docs.python.org/2/library/ssl.html#verifying-certificates , however they indicate that "The SSL context created will only allow TLSv1 and later (if supported by your system) connections."
ctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
ctx.options |= ssl.OP_NO_SSLv2
ctx.options |= ssl.OP_NO_SSLv3

### functions ###
def getTaxNBCI(species,tag): # input species full name (e.g. Arabidopsis thaliana), and information tag needed (options are: 'taxId', 'scientificName', 'commonName', 'formalName', 'rank', 'division', 'lineage', 'geneticCode', 'mitochondrialGeneticCode', 'submittable')
	if " " in species:
		species_name = species.split()
		url = "http://www.ebi.ac.uk/ena/data/taxonomy/v1/taxon/scientific-name/" + species_name[0] + '%20' + species_name[1]
	elif "_" in species:
		species_name = species.split("_")
		url = "http://www.ebi.ac.uk/ena/data/taxonomy/v1/taxon/scientific-name/" + species_name[0] + '%20' + species_name[1]	
	else:
		url = "http://www.ebi.ac.uk/ena/data/taxonomy/v1/taxon/scientific-name/" + species
	try: # ignore if a species can't be found - You can resubmit with the genus name, however there is a small chance to hit the same genus name in different organism domain (some examples of redundant genus names between plants, bacteria, fungi, and invertebrates exist...)
		response = urlopen(url, context=ctx) # added the "context=ctx" to avoid the "urllib2.URLError: <urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:661)>" potential error
		data = response.read()
		values = json.loads(data)
		return str(values[0][tag])
	except:
		print("can't find taxonomy for " + species +", please try to submit only genus name")


def getFullTaxNBCI(species): # input species full name (e.g. Arabidopsis thaliana), and get back a all the taxonomy lineage levels (uses the getTaxNBCI function)
	taxonomy = {}
	if " " in species:
		species_name = species.split()
		url = "http://www.ebi.ac.uk/ena/data/taxonomy/v1/taxon/scientific-name/" + species_name[0] + '%20' + species_name[1]
	else:
		url = "http://www.ebi.ac.uk/ena/data/taxonomy/v1/taxon/scientific-name/" + species
	try: # ignore if a species can't be found - You can resubmit with the genus name, however there is a small chance to hit the same genus name in different organism domain (some examples of redundant genus names between plants, bacteria, fungi, and invertebrates exist...) 
		response = urlopen(url, context=ctx) # added the "context=ctx" to avoid the "urllib2.URLError: <urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:661)>" potential error
		data = response.read()
		values = json.loads(data)
		taxid = str(values[0]['taxId'])
		scientificName = str(values[0]['scientificName'])
		lineage = str(values[0]['lineage']).strip().replace('; ', ';')[:-1].split(';') # list of all the levels, genus and above
		lineage_levels = [] # Get a list of all the levels
		for i in lineage:
			lineage_levels.append(getTaxNBCI(i,'rank'))
		taxonomy['scientificName'] = scientificName
		taxonomy.update(zip(lineage_levels, lineage))
		taxonomy['taxId'] = taxid
		return taxonomy
	except:
		print("can't find taxonomy for " + species +", please try to submit only genus name")

### Input ###

speciesList = [] # get species list - full names from input file

try:
	with open(sys.argv[1], 'r') as spFile:
		for line in spFile:
			speciesList.append(line.strip())
except:
	sys.exit("Error - Please provide a file with species or genera names, one line per species/genera !")

### Get taxonomy ###

taxo = {}
taxonomy_levels = [] # list of taxonomy levels to be included in the taxonomy.json - collect all the levels for all the species, and make a unique list
for np in speciesList:
	taxo[np] = getFullTaxNBCI(np)
	try:
		taxonomy_levels.append(taxo[np].keys()) # add the taxonomy levels to the taxonomy_levels list
	except:
		taxo[np] = {'scientificName':np, 'genus':np.split()[0]} # include the species without the taxonomy match - only species name and genus

taxonomy_levels = list(set([j for i in taxonomy_levels for j in i])) # get unique set of taxonomy levels

taxo['levels']= taxonomy_levels


### Save taxonomy.json file ###
try:
	with open(sys.argv[2], 'w') as fp: 
		json.dump(taxo, fp, indent=4) # if you don't want the indent output file, change to json.dump(taxo, fp), you can also sort the keys alphabetically by adding sort_keys=True
except:
	print("Output file name was not indicated. Results are saved in 'taxonomy.json'")
	with open('taxonomy.json', 'w') as fp:
		json.dump(taxo, fp, indent=4)